<div class="layout-2-block mainbanner">
    <div class="container">
        <div class="row">
        	
            <div class="span6">
            	<img src="<?php echo FRONTEND_ASSETS;?>images/layerslider/women-bg.jpg">
            </div>
            <div class="span6 bannerheads">
                <!--  START -->
                <p class="bannerhead"><?php echo $home['banner_head'];?></p>
                <p class="bannersubhead"><?php echo $home['banner_subhead'];?></p>
                <input type="text" maxlength="10" placeholder="Enter your postcode" id="postcode" name="post_code" autocomplete="off">
                <input id="check" type="button" value="Book" class="btn">
                <br clear="all">
                <p id="bsorry" class="sorrymsg dnone">Sorry! We are not available in your area</p>
               
              	<?php echo ($home['banner_p1']!="") ? '<p class="greencheck"><i class="icon-ok"></i> '.trim($home['banner_p1']).'</p>' : ''?>
               <?php echo ($home['banner_p2']!="") ? '<p class="greencheck"><i class="icon-ok"></i> '.trim($home['banner_p2']).'</p>' : ''?>
               <?php echo ($home['banner_p3']!="") ? '<p class="greencheck"><i class="icon-ok"></i> '.trim($home['banner_p3']).'</p>' : ''?>
               <?php echo ($home['banner_p4']!="") ? '<p class="greencheck"><i class="icon-ok"></i> '.trim($home['banner_p4']).'</p>' : ''?>
               <?php echo ($home['banner_p5']!="") ? '<p class="greencheck"><i class="icon-ok"></i> '.trim($home['banner_p5']).'</p>' : ''?>
               <?php echo ($home['banner_p6']!="") ? '<p class="greencheck"><i class="icon-ok"></i> '.trim($home['banner_p6']).'</p>' : ''?>
               <?php echo ($home['banner_p7']!="") ? '<p class="greencheck"><i class="icon-ok"></i> '.trim($home['banner_p7']).'</p>' : ''?>
				 <br clear="all"> <br clear="all">	
                  <!-- <a class="button-color mybigbt" href="#">Book Appointment <i class="icon-caret-right" style="margin-left:20px;"></i></a>-->
                <br clear="all"> <br clear="all">
                <!-- END -->
            </div>
		</div>
	</div>
	</div>
    
<div class="purchase">
	<section class="how-it-works-section light-blue">
            <div class="container">
                <div class="section-header">
                   <h2 class="text-center"><?php echo $home['green_top_head'];?></h2>
                   <div class="divider"></div>
                </div>
                <div class="row">
                   <div class="span4 text-center">
                       <div class="img-responsive img-circle" alt="book-cleaner">
                           <strong>1</strong>
                       </div>
                       <h4 class="text-center"><?php echo $home['green_sub_head1'];?></h4>
                       <p class="text-center"><?php echo $home['green_sub_text1'];?></p>
                   </div>
                   <div class="span4 text-center">
                       <div class="img-responsive img-circle" alt="book-cleaner">
                           <strong>2</strong>
                       </div>
                       <h4 class="text-center"><?php echo $home['green_sub_head2'];?></h4>
                       <p class="text-center"><?php echo $home['green_sub_text2'];?></p>
                   </div>
                   <div class="span4 text-center">
                       <div class="img-responsive img-circle" alt="book-cleaner">
                           <strong>3</strong>
                       </div>
                       <h4 class="text-center"><?php echo $home['green_sub_head3'];?></h4>
                       <p class="text-center"><?php echo $home['green_sub_text3'];?></p>
                   </div>
                </div>
            </div>
        </section>
        
        <section class="satisfaction-section">
            <div class="container">
              <div class="row">
                  <div class="span10 offset1">
                      <div class="row-fluid">
                          <div class="span3 text-center pull-right" style="margin-bottom:10px">
                            <img class="img-responsive" alt="" src="<?php echo FRONTEND_ASSETS;?>images/tidyness_gurantee.png"></div>
                          <div class="span9 pull-left">
                              <h3 class="text-center"><?php echo $home['tidy_head'];?></h3>
                              <p class="text-center guranteedesc" ><?php echo $home['tidy_sub_head'];?></p>
                          </div>
                          
                      </div>
                  </div>
              </div>
            </div>
        </section>
        
        <section class="trust-us-section dark-blue">
            <div class="container">
                <div class="row">
                <div class="span3 text-center">
                        <img class="img-responsive" alt="" src="<?php echo FRONTEND_ASSETS;?>images/trust.png">
                    </div>
                    <div class="span9">
                        <h2><?php echo $home['com_head'];?></h2>
                        <p><?php echo $home['com_text'];?></p>
                        <div class="main-content-block-entry">
                    	<ul class="list-style-2">
                      	<?php echo ($home['com_p1']!="") ? '<li><i class="icon-ok"></i> '.$home['com_p1'].'</li>' : '';?>
                       	<?php echo ($home['com_p2']!="") ? '<li><i class="icon-ok"></i> '.$home['com_p2'].'</li>' : '';?>
                        <?php echo ($home['com_p3']!="") ? '<li><i class="icon-ok"></i> '.$home['com_p3'].'</li>' : '';?>
                        <?php echo ($home['com_p4']!="") ? '<li><i class="icon-ok"></i> '.$home['com_p4'].'</li>' : '';?>
                        <?php echo ($home['com_p5']!="") ? '<li><i class="icon-ok"></i> '.$home['com_p5'].'</li>' : '';?>
                        <?php echo ($home['com_p6']!="") ? '<li><i class="icon-ok"></i> '.$home['com_p6'].'</li>' : '';?>
                       	<?php echo ($home['com_p7']!="") ? '<li><i class="icon-ok"></i> '.$home['com_p7'].'</li>' : '';?>
                      </ul>
                    </div>
                    </div>
                    
                </div>
            </div>
        </section>
</div>

