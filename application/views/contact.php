<div class="margtop"></div>
<?php 
if($websettings['lat_long']!="")
{?>
<div id="map-canvas"></div>
<?php }?>
<div class="main-content ">
	<div class="container">
    	<div class="row contactrow">
        
        	<div class="span9">
				<div class="main-content-block cont">
                <h2>Contact Us</h2>
					<p><?php echo $websettings['contact_text'];?></p>
				</div>
				<div class="main-content-block cont">
                	<div class="main-content-block-entry">
                    	<form class="add-comment-form" id="contact_form" name="contact_form" action="<?php echo base_url('home/sendemail');?>" method="post" >
	                        <p><textarea class="required" style="resize:none" placeholder="Message" id="message" name="message"></textarea></p>
                            <p><input type="text" placeholder="Name" name="name" id="name" class="required"> <input type="email" placeholder="Email Address" id="email" name="email" class="required"> <button type="submit"><i class="icon-ok-sign"></i> Submit</button></p>
                        </form>
                        <?php if($this->session->flashdata('status')=="success")
						{?>
                        <p><strong style="color:#37C878">Thank you! We have received your message.<a name="bottom" style="visibility:hidden">Bottom</a></strong></p>
                        <?php } ?>
                    </div>
				</div>
            </div>
            
            <div class="span3">
				<div class="main-content-block cont">
                	<h2>contact info</h2>
					<div class="main-content-block-entry contact-info">
                    	<?php if($websettings['address']!=""){;?>
                        <p><i class="icon-location-arrow"></i> <?php echo nl2br($websettings['address']);?></p>
                    	<?php } ?>
                        <?php if($websettings['phone']!=""){;?>
                        <p><i class="icon-phone"></i> <?php echo $websettings['phone'];?></p>
                    	<?php } ?>
                        <?php if($websettings['email']!=""){;?>
                        <p><i class="icon-envelope"></i><a href="mailto:<?php echo $websettings['email'];?>"> <?php echo $websettings['email'];?></a></p>
                    	<?php } ?>
                        
                    	 <?php if($websettings['website_url']!=""){;?>
                        <p><i class="icon-globe"></i><a href="mailto:<?php echo $websettings['email'];?>"> <?php echo $websettings['website_url'];?></a></p>
                    	<?php } ?>
                        
					</div>
				</div>
				<div class="main-content-block cont">
                	<h2>working hours</h2>
					<div class="main-content-block-entry contact-info">
                    	<p><i class="icon-time"></i> <?php echo $websettings['b_phone'];?></p>
                    	<?php if($websettings['b_email']!=""){;?><p><i class="icon-time"></i> <?php echo $websettings['b_email'];?></p><?php } ?>
                    	<?php if($websettings['m_phone']!=""){;?><p><i class="icon-remove-sign"></i> <?php echo $websettings['m_phone'];?></p><?php } ?>
					</div>
				</div>
            </div>
        
        </div>
    </div>
</div>
<?php 
if($websettings['lat_long']!="")
{?>
 <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&hl=en&sensor=false"></script>
    <script>
var map;
function initialize() {
	var latlng = new google.maps.LatLng(<?php echo $websettings['lat_long'];?>);
  var mapOptions = {
    zoom: 16,
	 scrollwheel: true,
	draggable: true,
    center: latlng,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
	
	
  };
  
  map = new google.maps.Map(document.getElementById('map-canvas'),
      mapOptions);
	new google.maps.Marker({
                        position: latlng,
                        map: map,
                        /* icon: map_pin*/
                    });
}

google.maps.event.addDomListener(window, 'load', initialize);

    </script>
<?php 
}?>