<div class="margtop"></div>
<div class="main-content">
	<div class="container">
    	<div class="row" style="padding-top:20px;">
        
        	<!-- blog entries start //-->
            <div class="span9">
				<div class="main-content-block" >
                	<h2><?php echo $b['blog_name'];?></h2>
					<div class="main-content-block-entry">
                    	<?php
						
							$chk = ($b['blog_image']!=""&&file_exists('./assets/frontend/images/blog/'.$b['blog_image'])) ? "1" : "0";
							?>
                        <div class="post-item" style="margin-top:10px;">
                        	<div class="date"><?php echo date('d',strtotime($b['blog_added']));?> <span><?php echo date('M',strtotime($b['blog_added']));?></span></div>
                            <i class="icon-pencil"></i>
                            
                            <div class="post-item-body" <?php echo ($chk=="0") ? ' style="padding-left:10px"' : '';?> >
                            	<?php
								if($chk=="1"){?>
                                
                                <a href="<?php echo base_url('blog/'.$b['blog_uri'].'.html');?>"><figure><img src="<?php echo FRONTEND_ASSETS;?>images/blog/<?php echo $b['blog_image'];?>" alt="<?php echo $b['blog_name'];?>"></figure></a>
                                <?php } ?>
                                
                               
                                <p> <?php echo str_replace('<img','<img class="img-responsive" ',$b['blog_text']);?></p>
                                 
                                <p>Posted in: <?php 
								$cnt = count($b['cats']);
								$cn = 1;
								foreach($b['cats'] as $c)
								{
									echo '<a href="'.base_url('blog/index/?cat='.$c['cat_id']).'">'.trim($c['cat_name']).'</a>';
									echo ($cnt!=$cn) ? ', ' : '';
									$cn++;
								}?></p>
                                
                                
                                <?php
								$cnt = count($b['tags']);
								if($cnt>0)
								{?> 
                                 <p>Tags: <?php $cn = 1;
								foreach($b['tags'] as $c)
								{
									echo '<a href="'.base_url('blog/index/?tag='.$c['tag_id']).'">'.$c['tag_name'].'</a>';
									echo ($cnt!=$cn) ? ', ' : '';
									$cn++;
								}?></p>
                                
								<?php } ?> 
                                  <p>Posted on: <?php echo date('F d, Y',strtotime($b['blog_added']));?></a></p> 
								  <?php if($b['blog_author'] ) { ?>
								  <p>Author: <?php echo $b['blog_author'];?></p> 
								  <?php } ?>

           </p>
                            </div>
                        </div>
                       
                    
                   </div>
                </div>
            </div>
            
        	<!-- right sidebar start //-->
            <div class="span3">
            	<!-- sidebar search //-->
            	<?php if(!empty($cats)){?>
                <!-- sidebar categories //-->
            	
                <div class="main-content-block cont">
                	<h2><br>categories</h2>
                    <div class="main-content-block-entry sidebar-categories">
                    	<ul>
                        	<?php
							foreach($cats as $c)
							{
                            echo '<li><a href="'.base_url('blog/index/?cat='.$c['cat_id']).'">'.trim($c['cat_name']).'</a> ('.$c['cat_total'].')</li>';
							}
							?>
							</ul>
                    </div>
                </div>
                <!-- sidebar text widget //-->
            	<?php 
				}?>
                <!-- sidebar posts //-->
            	
                <?php if(!empty($tags)){?>
                <!-- content tags //-->
            	<div class="main-content-block cont">
                	<h2><br>blog tags</h2>
                    <div class="main-content-block-entry widget-content-tags">
                    	<?php
							foreach($tags as $t)
							{
                            echo '<li><a href="'.base_url('blog/index/?tag='.$t['tag_id']).'">'.$t['tag_name'].'</a></li>';
							}
							?>
                    </div>
				</div>
                
            </div>
        	<?php }?>
        <?php if($b['blog_comment_status'] == "yes"){ ?>
		<div class="span9">
				<div class="main-content-block cont">
                <h2>Comments</h2>
				</div>
				<div style="margin-top:10px;" class="main-content-block cont">                	
                     <div class="main-content-block-entry widget-content-tags">					 
                    	<?php
						$i =0;
							foreach($comments as $comment)
							{
							$i++;
                            echo '<p><b>Comment '.$i.' :</b> '.$comment['comment'].' <br/><b>By: '.$comment['name'].' on '.date('M, d Y h:i A',strtotime($comment['dt'])).'</b></p>';
							}
							?>
							
                    </div>
				</div>
				<div class="main-content-block cont">
                	<div class="main-content-block-entry">
                    	<form class="add-comment-form" id="contact_form" name="contact_form" action="<?php echo base_url('blog/addcomment/'.$b['blog_id']);?>" method="post" >
	                        <p><textarea class="required" style="resize:none" placeholder="Message" id="message" name="message"></textarea></p>
                            <p><input type="text" placeholder="Name" name="name" id="name" class="required"> <input type="email" placeholder="Email Address" id="email" name="email" class="required"> <button type="submit"><i class="icon-ok-sign"></i> Submit</button></p>
                        </form>
                        <?php if($this->session->flashdata('status')=="success")
						{?>
                        <p><strong style="color:#37C878">Thank you! Your Comment has been added.<a name="bottom" style="visibility:hidden">Bottom</a></strong></p>
                        <?php } ?>
                    </div>
				</div>
            </div>
		<?php } ?>
        </div>
    </div>
</div>