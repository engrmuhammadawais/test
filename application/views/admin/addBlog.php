<?php 
$crumb2 = "";

 if($alert=="error"||$alert=="edit" || $alert=="image_error"){
 //print_r($tbl_data);
	$blog_name = $tbl_data['blog_name'];
	$blog_author = $tbl_data['blog_author'];
	$blog_uri = $tbl_data['blog_uri'];
	$blog_text = stripslashes($tbl_data['blog_text']);
	$blog_status = $tbl_data['blog_status']; 
	$blog_commnet_status = $tbl_data['blog_comment_status']; 
	$blog_meta_key = stripslashes($tbl_data['blog_meta_key']);
	$blog_meta_desc = stripslashes($tbl_data['blog_meta_desc']);
	$blog_extra_tags = stripslashes($tbl_data['blog_extra_tags']);
 	
 	$blog_featured = $tbl_data['blog_featured'];
	$blog_tags = explode(",",$tbl_data['blog_tags']);
	$blog_cats = explode(",",$tbl_data['blog_cats']);
	$blog_image = $tbl_data['blog_image'];
	$blog_thumb = $tbl_data['blog_thumb'];
	
	
 }
 else{
	 	$blog_slider = "";
	 $unpub_date = "";
	 $unpub_time = "";
	 $blog_author = "";
	$blog_thumb = "";
	$blog_image = "";
	$blog_tags="";
	$blog_cats="";
	$blog_uri = "";
	$blog_name="";
	$blog_text="";
	$blog_status = "Published";
	$blog_commnet_status = "No";
	$blog_meta_key = "";
	$blog_meta_desc = "";
	$blog_extra_tags = "";
	$blog_pdate = "";
	$pub_date = "";
	$pub_time ="";
	$blog_featured = "No";
	$blog_video = "";
 }
 
 if($alert=="edit")
 {
	$crumb = "Edit";
	$action = "editRecord/".$tbl_data['blog_id']; 
	
 }
 else{
	$crumb = "Add";
	$action = "addRecord"; 
 }
 ?>

<ol class="breadcrumb bc-3">
<li><a href="<?php echo ADMIN_URL;?>"><i class="entypo-home"></i>Home</a></li>
<li><a href="<?php echo ADMIN_URL;?>blogs"><i></i>Articles</a></li>
<li class="active"><strong><?php echo $crumb;?> Article</strong></li>
</ol>
     
     
     
<?php if($alert=="success") { ?>
<div class="row alertrow">
	<div class="col-md-12">
    <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-success"><strong>Success!</strong> Settings saved sucessfully.</div>
	</div>
</div>
<?php } if($alert=="error"||$editerror=="editerror") { ?>
<div class="row alertrow">
	<div class="col-md-12">
     <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-danger"><strong>Error!!</strong> The user name or email you specified is already exist, please use different user name or email.</div>
	</div>
</div>
<?php } else if($alert=="image_error"||$editerror=="image_error"){ ?>
<div class="row alertrow">
	<div class="col-md-12">
     <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-danger"><strong>Error!!</strong> Unable to upload the image, please check the file format and size.</div>
	</div>
</div>
  
<?php } if($alert=="perror") { ?>
<div class="row alertrow">
	<div class="col-md-12">
     <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-danger"><strong>Error!!</strong> Unable to change the password, please provide the correct current password.</div>
	</div>
</div>

<?php } ?>     
     
     

<h2><?php echo $crumb;?> Article</h2>
<br />


<div class="panel panel-primary">

	
	
	<div class="panel-body">
		 <form  id="blog_form" name="blog_form" method="post" action="<?php echo base_url();?>manage/blogs/<?php echo $action;?>" enctype="multipart/form-data" class="validate">
			<div class="form-group">
				<label class="control-label">Article Title :</label>
                
                  <input type="text" name="blog_name" id="blog_name" value="<?php echo $blog_name;?>" class="form-control " placeholder="My First Article" data-validate="required,maxlength[250]"/>
			</div>
            
          
            
            <div class="form-group">
					
						
						<div class="col-sm-8" style="padding-left:0px;">
								<label class="control-label">Slug :</label><br clear="all" />
							<div class="pull-left">
								<input type="text" style="width:200px" name="blog_uri" id="blog_uri" value="<?php echo $blog_uri;?>" class="form-control aplhasmall" placeholder="my-first-blog" data-validate="required,maxlength[250]"/>
							</div>
                            <div class="pull-left" style="margin-top:7px">.html</div>
						</div>
					</div> 
                    
                    
                     <br clear="all" />    <br clear="all" />
                     <div class="form-group" >
				<label class="control-label">Article Author :</label>
                
                  <input type="text" name="blog_author" id="blog_author" value="<?php echo $blog_author;?>" class="form-control aplha" placeholder="John Doe" data-validate="maxlength[100]"/>
			</div>  
                    <div class="form-group">
				 <label class="control-label">Article Image :</label>
               
                  <input type="file"  name="uploadfile" id="uploadfile" accept="image/*"/>
                 <br/> <span>Image Type:</strong> JPEG / PNG</span><br/>
                  <?php if($blog_thumb!=""){?><br/><img src="<?php echo $this->imagethumb->image('./assets/frontend/images/blog/'.$blog_thumb,180,0);?>" /><?php } ?>
			</div>
                   
            <br clear="all" />
                    <div class="form-group">
				<label class="control-label">Category : <button id="addNewCat" class="btn btn-default btn-xs" type="button">Add New</button></label>
                
                <select autocomplete="off" data-validate="required" name="blog_category[]" id="blog_category"  multiple>
				<?php
                    if(!empty($cats))
                    {
                        foreach($cats as $c)
                        {
                            echo '<option value="'.$c['cat_id'].'" ';
							if(!empty($blog_cats)&&in_array($c['cat_id'],$blog_cats)){
							echo ' selected="selected"' ;	
							}
							echo ' >'.$c['cat_name'].'</option>';	
                        }
                    }
                
                ?>
							</select>
			</div>
              <div class="form-group">
				<label class="control-label">Tags : <button id="addNewTag" class="btn btn-default btn-xs" type="button">Add New</button></label></label>
                <select autocomplete="off" name="blog_tags[]" id="blog_tags"  multiple>
				<?php
                    if(!empty($tags))
                    {
                        foreach($tags as $t)
                        {
                            echo '<option value="'.$t['tag_id'].'" ';
							if(!empty($blog_tags)&&in_array($t['tag_id'],$blog_tags)){
							echo ' selected="selected"' ;	
							}
							echo '>'.$t['tag_name'].'</option>';	
                        }
                    }
                
                ?>

							</select>
			</div>      
                    
           <br clear="all" />
            <div class="form-group">
				<?php echo $this->ckeditor->editor("blog_text",$blog_text);?>
			</div>
           
            
            <div class="form-group">
				 <label class="control-label">Status :</label>
                
                  <select class="form-control"  name="blog_status" id="blog_status">
                    <option value="Published" <?php if($blog_status=="Published"){ echo ' selected="selected"';} ?>>Published</option>
                    <option value="Un-Published" <?php if($blog_status=="Un-Published"){ echo ' selected="selected"';} ?>>Un-Published</option>
                  </select>
			</div>
             
            <div class="form-group">
				 <label class="control-label">Comment :</label>
                
                  <select class="form-control"  name="blog_commnet_status" id="blog_commnet_status">
                    <option value="yes" <?php if($blog_commnet_status=="yes"){ echo ' selected="selected"';} ?>>Yes</option>
                    <option value="no" <?php if($blog_commnet_status=="no"){ echo ' selected="selected"';} ?>>No</option>
                  </select>
			</div>
             
            <div class="form-group">
				<label class="control-label">Meta Keywords :</label>
                
                  <textarea style="resize:none" name="blog_meta_key" id="blog_meta_key"  class="form-control" placeholder="" /><?php echo $blog_meta_key;?></textarea>
			</div>
            <div class="form-group">
				<label class="control-label">Meta Description :</label>
                
                  <textarea style="resize:none" name="blog_meta_desc" id="blog_meta_desc"  class="form-control" placeholder="" /><?php echo $blog_meta_desc;?></textarea>
			</div>
            <div class="form-group">
				<label class="control-label">Extra SEO Tags :</label>
                
                  <textarea style="resize:none" name="blog_extra_tags" id="blog_extra_tags"  class="form-control" placeholder="" /><?php echo $blog_extra_tags;?></textarea>
			</div>
            
           
			
			
			<div class="form-group">
				
				<button type="button" class="btn btn-danger" onclick="window.location='<?php echo ADMIN_URL;?>blogs'">Cancel</button>
                <button type="submit"  class="btn btn-success">Submit</button>
			</div>
		
		</form>
	
  
	</div>
    </div>


