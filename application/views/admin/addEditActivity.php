<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="<?php echo base_url('manage');?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> | <a href="<?php echo base_url('manage/'.$this->controller);?>" class="tip-bottom"><?php echo $this->moduleName;?></a> | <a href="javascript:void(0)" class="current"><?php echo $type." ".$this->moduleName;?></a> </div>
    
  </div>

  <div class="container-fluid">
    <div class="row-fluid">
      <div class="span9">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
            <h5><?php echo $type." ".$this->moduleName;?></h5>
          </div>
          <div class="widget-content nopadding">

            <table class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>Field</th>
                  <th>Value</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Full Name</td>
                  <td><?php echo $tbl_data['full_name']; ?></td>
                </tr>
                <tr>
                  <td>Last Login Time</td>
                  <td><?php echo date('j, M Y - h:i:s A', strtotime($tbl_data['datetime'])); ?></td>
                </tr>
                <tr>
                  <td>Main Activity</td>
                  <td><?php echo $tbl_data['log_key']; ?></td>
                </tr>
                <?php if ( $tbl_data['log_value'] != '' ) {?>
                <tr>
                  <td>Last Login Time</td>
                  <td><?php echo $tbl_data['log_value']; ?></td>
                </tr>
                <?php } ?>
                <?php if ( $tbl_data['log_others'] != '' ) { ?>
                <?php foreach( json_decode($tbl_data['log_others'], TRUE) as $key => $value ) { ?>
                <tr>
                  <td><?php echo humanize($key); ?></td>
                  <td><?php echo $value; ?></td>
                </tr>
                <?php } ?>
                <?php } ?>
                <tr>
                  <td>UserName</td>
                  <td><?php echo $tbl_data['user_name']; ?></td>
                </tr>
                <tr>
                  <td>Email</td>
                  <td><?php echo $tbl_data['email']; ?></td>
                </tr>
                <tr>
                  <td>Last Login User Agent</td>
                  <td><?php echo $tbl_data['user_agent']; ?></td>
                </tr>
                <tr>
                  <td>Last Login IP Address</td>
                  <td><?php echo $tbl_data['ip']; ?></td>
                </tr>

              </tbody>
            </table>

          </div>
        </div>
      </div>
      
    </div>
    
    <hr>
    
  </div>
</div>
