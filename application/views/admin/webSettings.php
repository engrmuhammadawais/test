

<ol class="breadcrumb bc-3">
<li><a href="<?php echo ADMIN_URL;?>"><i class="entypo-home"></i>Home</a></li>

<li class="active"><strong>Website Settings</strong></li>
</ol>
     
     
     
<?php if($alert=="success") { ?>
<div class="row alertrow">
	<div class="col-md-12">
    <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-success"><strong>Success!</strong> Website Settings saved sucessfully.</div>
	</div>
</div>
<?php } if($alert=="error") { ?>
<div class="row alertrow">
	<div class="col-md-12">
     <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-danger"><strong>Error!!</strong>  Error occurred while saving the record, please try again.</div>
	</div>
</div>
<?php }?>     
     
     

<h2>Website Settings</h2>
<br />


<div class="panel panel-primary">

	
	
	<div class="panel-body">
		 <form  id="slider_form" name="slider_form" method="post" action="<?php echo base_url();?>manage/home/savewebsettings" enctype="multipart/form-data" class="validate" >
			<div class="form-group">
				<label class="control-label">Website Title :</label>
               
                  <input type="text" data-validate="required" name="website_title" id="website_title" value="<?php echo $web['website_title'];?>" class="form-control" placeholder="My Website" />
			</div>
            <div class="form-group">
				<label class="control-label">Website URL :</label>
               
                  <input type="text" data-validate="url" name="website_url" id="website_url" value="<?php echo $web['website_url'];?>" class="form-control" placeholder="http://www.mywebsite.com" />
			</div>
            <div class="form-group">
				 <label class="control-label">Logo :</label>
               
                  <input type="file" name="uploadfile" id="uploadfile" />
                 <br/> <span>Image Type:</strong> JPEG / PNG</span><br/>
                  <br/>
                  <?php if(file_exists('./assets/frontend/images/logo/'.$web['logo'])){?>
                  <img style="background:#F4F4F4" src="<?php echo $this->imagethumb->image('./assets/frontend/images/logo/'.$web['logo'],180,0);?>" /><?php }?>
			</div>
            
            
            
			
			
			
			
			
			<div class="form-group">
				 <label class="control-label">Website Email :</label>
                
                  <input type="text" data-validate="email" name="email" id="email" value="<?php echo $web['email'];?>" class="form-control" placeholder="Email Address" />
			</div>
			<div class="form-group">
				 <label class="control-label">Contact Form Email :</label>
                
                  <input type="text" data-validate="required,email" name="contact_form_email" id="contact_form_email" value="<?php echo $web['contact_form_email'];?>" class="form-control" placeholder="Email Address" />
			</div>
            <div class="form-group">
				 <label class="control-label">Contact Form Text :</label>
                
                  <?php echo $this->ckeditor->editor("contact_text",$web['contact_text']);?>
			</div>
			<div class="form-group">
				 <label class="control-label">Map Latitude, Longitude :</label>
               
                  <input type="text"  name="lat_long" id="lat_long" value="<?php echo $web['lat_long'];?>" class="form-control" placeholder="24.855376,67.02861" /><br clear="all" /> Click <a href="http://itouchmap.com/latlong.html" target="_blank">here</a> to get the latitude and longitude 
			</div>
			
			<div class="form-group">
				  <label class="control-label">Phone :</label>
               	<input type="text"  name="phone" id="phone" value="<?php echo $web['phone'];?>" class="form-control" placeholder="Phone" />
                 
			</div>
			<div class="form-group" style="display:none">
				<label class="control-label">Fax :</label>
               
                  <textarea style="resize:none" name="fax" id="fax" class="form-control" placeholder="Fax Number" ><?php echo $web['fax'];?></textarea>
			</div>
            
            <div class="form-group">
				<label class="control-label">Address :</label>
                
                  <textarea style="resize:none" name="address" id="address" class="form-control" placeholder="Address" ><?php echo $web['address'];?></textarea>
			</div>
            
            
            <div class="form-group">
				  <label class="control-label">Working Hours :</label>
               <input type="text"  name="b_phone" id="b_phone" value="<?php echo $web['b_phone'];?>" class="form-control required" placeholder="Booking Phone" />
                 
			</div>
            <div class="form-group">
				 <label class="control-label">Working Hours 2 (Optional) :</label>
                
                  <input type="text"  name="b_email" id="b_email" value="<?php echo $web['b_email'];?>" class="form-control" placeholder="Booking Email Address" />
			</div>
            
             
            <div class="form-group">
				  <label class="control-label">Off Day (optional):</label>
               <input type="text"  name="m_phone" id="m_phone" value="<?php echo $web['m_phone'];?>" class="form-control" placeholder="Manager Phone" />
                  
			</div>
            
            
             
            <div class="form-group">
				<label class="control-label">Twitter :</label>
                
                  <input type="text" name="twitter" id="twitter" class="form-control" placeholder="" value="<?php echo $web['twitter'];?>" data-validate="url"/>
			</div>
             <div class="form-group">
				<label class="control-label">Facebook :</label>
                
                  <input type="text" name="facebook" id="facebook" class="form-control" placeholder="" value="<?php echo $web['facebook'];?>" data-validate="required,url"/>
			</div>
             <div class="form-group">
				<label class="control-label">Google Plus :</label>
                
                  <input type="text" name="google" id="google" class="form-control" placeholder="" value="<?php echo $web['google'];?>" data-validate="url"/>
			</div>
             <div class="form-group">
				<label class="control-label">Linkedin :</label>
                
                  <input type="text" name="linkedin" id="linkedin" class="form-control" placeholder="" value="<?php echo $web['linkedin'];?>" data-validate="url"/>
			</div>
             <div class="form-group">
				<label class="control-label">Pinterest :</label>
                
                  <input type="text" name="pinterest" id="pinterest" class="form-control" placeholder="" value="<?php echo $web['pinterest'];?>" data-validate="url"/>
			</div>
             <div class="form-group">
				<label class="control-label">Flickr :</label>
                
                  <input type="text" name="flickr" id="flickr" class="form-control" placeholder="" value="<?php echo $web['flickr'];?>" data-validate="url"/>
			</div>
             <div class="form-group">
				<label class="control-label">Youtube :</label>
                
                  <input type="text" name="youtube" id="youtube" class="form-control" placeholder="" value="<?php echo $web['youtube'];?>" data-validate="url"/>
			</div>
              <div class="form-group" style="display:none">
				<label class="control-label">Vimeo :</label>
                
                  <input type="text" name="vimeo" id="vimeo" class="form-control" placeholder="" value="<?php echo $web['vimeo'];?>" data-validate="url"/>
			</div>
             <div class="form-group">
				<label class="control-label">Instagram :</label>
                
                  <input type="text" name="instagram" id="instagram" class="form-control" placeholder="" value="<?php echo $web['instagram'];?>" data-validate="url"/>
			</div>
            
            
            
             <div class="form-group">
				<label class="control-label">Google Analytics :</label>
                
                  <input type="text" name="analytics" id="analytics" class="form-control" placeholder="UA-XXXXX-X" value="<?php echo $web['analytics'];?>" />
			</div>
            
             <div class="form-group" style="display:none">
				<label class="control-label">Google Map :</label>
                
                  <textarea style="resize:none" name="map" id="map" class="form-control" placeholder="Google Map" rows="6"><?php echo $web['map'];?></textarea>
			</div>
            
		<div class="form-group">
				
				<button type="button" class="btn btn-danger" onclick="window.location='<?php echo ADMIN_URL;?>'">Cancel</button>
                <button type="submit" class="btn btn-success">Submit</button>
			</div>
		</form>
	
  </div>
  
  </div>

