


<div class="row">
	<div class="col-sm-3" onclick="javascript:window.location='<?php echo base_url('manage/users/');?>'">
	
		<div class="tile-stats tile-red">
			<div class="icon"><i class="entypo-users"></i></div>
			<div class="num" data-start="0" data-end="<?php echo $totalUsers;?>" data-postfix="" data-duration="1500" data-delay="0">0</div>
			
			<h3>Admin users</h3>
			
		</div>
		
	</div>
	
	<div class="col-sm-3">
	
		<div class="tile-stats tile-green"  onclick="javascript:window.location='<?php echo base_url('manage/faq/');?>'">
			<div class="icon"><i class="glyphicon glyphicon-question-sign"></i></div>
			<div class="num" data-start="0" data-end="<?php echo $totalFAQ;?>" data-postfix="" data-duration="1500" data-delay="400">0</div>
			
			<h3>FAQs</h3>
		</div>
		
	</div>
	
	
    
    <div class="col-sm-3 "  onclick="javascript:window.location='<?php echo base_url('manage/blogs/');?>'">
	
		<div class="tile-stats tile-purple">
			<div class="icon"><i class="entypo-layout"></i></div>
			<div class="num" data-start="0" data-end="<?php echo $totalBlogs;?>" data-postfix="" data-duration="1500" data-delay="1600">0</div>
			
			<h3>Articles</h3>
			
		</div>
		
	</div>
    
    <div class="col-sm-3"  onclick="javascript:window.location='<?php echo base_url('manage/pages/');?>'">
	
		<div class="tile-stats tile-cyan">
			<div class="icon"><i class="entypo-doc-text-inv"></i></div>
			<div class="num" data-start="0" data-end="<?php echo $totalPages;?>" data-postfix="" data-duration="1500" data-delay="2000">0</div>
			
			<h3>Web Pages</h3>
			
		</div>
		
	</div>
    <div class="col-sm-3"  onclick="javascript:window.location='<?php echo base_url('manage/home/websettings/');?>'">
	
		<div class="tile-stats tile-plum">
			<div class="icon"><i class="entypo-cog"></i></div>
			<div class="num" style="visibility:hidden" data-start="0" data-end="52" data-postfix="" data-duration="1500" data-delay="2800">0</div>
			
			<h3>Website Settings</h3>
			
		</div>
		
	</div>
    <div class="col-sm-3"  onclick="javascript:window.location='<?php echo base_url('manage/home/logout');?>'">
	
		<div class="tile-stats tile-orange">
			<div class="icon"><i class="entypo-logout"></i></div>
			<div style="visibility:hidden" class="num" data-start="0" data-end="<?php echo $totalNews;?>" data-postfix="" data-duration="1500" data-delay="2400">0</div>
			
			<h3>Logout</h3>
			
		</div>
		
	</div>
    
</div>

<br />
<br />
<br />
<br />
<br />
<br />


