
<ol class="breadcrumb bc-3">
<li><a href="<?php echo ADMIN_URL;?>"><i class="entypo-home"></i>Home</a></li>
<li class="active"><strong>Edit Home Page</strong></li>
</ol>
     
     
     
<?php if($alert=="success") { ?>
<div class="row alertrow">
	<div class="col-md-12">
    <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-success"><strong>Success!</strong> Home page contents updated sucessfully.</div>
	</div>
</div>
<?php } if($alert=="error") { ?>
<div class="row alertrow">
	<div class="col-md-12">
     <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-danger"><strong>Error!!</strong> The user name or email you specified is already exist, please use different user name or email.</div>
	</div>
</div>
<?php } ?>     
     
     

<h2>Edit Home Page</h2>
<br />


<div class="panel panel-primary">

	
	
	<div class="panel-body">
		 <form  id="home_form" name="home_form" class="validate" method="post" action="<?php echo base_url('manage/home/savehome');?>" enctype="multipart/form-data" >
         	<div class="form-group">
				<label class="control-label"><strong>Banner Contents</strong></label>
        	</div>
			<div class="form-group">
				<label class="control-label">Heading :</label>
                
                  <input type="text" maxlength="250" name="banner_head" id="banner_head" value="<?php echo $home['banner_head'];?>" class="form-control required" placeholder="Get Your Place Cleaned" />
			</div>
            <div class="form-group">
				<label class="control-label">Sub Heading :</label>
                
                  <input type="text" maxlength="250" name="banner_subhead" id="banner_subhead" value="<?php echo $home['banner_subhead'];?>" class="form-control required" placeholder="It's simple, affordable, and convenient" />
			</div>
            <div class="form-group">
				<label class="control-label">Feature 1 :</label>
                
                  <input type="text" maxlength="150" name="banner_p1" id="banner_p1" value="<?php echo $home['banner_p1'];?>" class="form-control" placeholder="Easy online scheduling" />
			</div>
            <div class="form-group">
				<label class="control-label">Feature 2 :</label>
                
                  <input type="text" maxlength="150" name="banner_p2" id="banner_p2" value="<?php echo $home['banner_p2'];?>" class="form-control" placeholder="100% Satisfaction Guarantee" />
			</div>
            <div class="form-group">
				<label class="control-label">Feature 3 :</label>
                
                  <input type="text" maxlength="150" name="banner_p3" id="banner_p3" value="<?php echo $home['banner_p3'];?>" class="form-control" placeholder=" Cleanings are bonded and insured" />
			</div>
            <div class="form-group">
				<label class="control-label">Feature 4 :</label>
                
                  <input type="text" maxlength="150" name="banner_p4" id="banner_p4" value="<?php echo $home['banner_p4'];?>" class="form-control" placeholder="Cleaners bring all supplies and equipment" />
			</div>
            <div class="form-group">
				<label class="control-label">Feature 5 :</label>
                
                  <input type="text" maxlength="150" name="banner_p5" id="banner_p5" value="<?php echo $home['banner_p5'];?>" class="form-control" placeholder="Cleaners are background checked and certified" />
			</div>
            <div class="form-group">
				<label class="control-label">Feature 6 :</label>
                
                  <input type="text" maxlength="150" name="banner_p6" id="banner_p6" value="<?php echo $home['banner_p6'];?>" class="form-control" placeholder="Exta Field" />
			</div>
            <div class="form-group">
				<label class="control-label">Feature 7 :</label>
                
                  <input type="text" maxlength="150" name="banner_p7" id="banner_p7" value="<?php echo $home['banner_p7'];?>" class="form-control" placeholder="Exta Field" />
			</div>
            
            <div class="form-group">
				<label class="control-label"><strong>Steps Section</strong></label>
        	</div>
             <div class="form-group">
				<label class="control-label">Heading :</label>
                
                  <input type="text" maxlength="150" name="green_top_head" id="green_top_head" value="<?php echo $home['green_top_head'];?>" class="form-control required" placeholder="It's as Easy as 1-2-3" />
			</div>
            <div class="form-group">
				<label class="control-label">Step 1 Heading :</label>
                
                  <input type="text" maxlength="150" name="green_sub_head1" id="green_sub_head1" value="<?php echo $home['green_sub_head1'];?>" class="form-control required" placeholder="BOOK" />
			</div>
            <div class="form-group">
				<label class="control-label">Step 1 Text :</label>
                
                  <input type="text" maxlength="150" name="green_sub_text1" id="green_sub_text1" value="<?php echo $home['green_sub_text1'];?>" class="form-control" placeholder="Book in under 60 seconds" />
			</div>
            
             <div class="form-group">
				<label class="control-label">Step 2 Heading :</label>
                
                  <input type="text" maxlength="150" name="green_sub_head2" id="green_sub_head2" value="<?php echo $home['green_sub_head2'];?>" class="form-control required" placeholder="CLEAN" />
			</div>
            <div class="form-group">
				<label class="control-label">Step 2 Text :</label>
                
                  <input type="text" maxlength="150" name="green_sub_text2" id="green_sub_text2" value="<?php echo $home['green_sub_text2'];?>" class="form-control" placeholder="A cleaner comes over" />
			</div>
            
             <div class="form-group">
				<label class="control-label">Step 3 Heading :</label>
                
                  <input type="text" maxlength="150" name="green_sub_head3" id="green_sub_head3" value="<?php echo $home['green_sub_head3'];?>" class="form-control required" placeholder="TIDY" />
			</div>
            <div class="form-group">
				<label class="control-label">Step 3 Text :</label>
                
                  <input type="text" maxlength="150" name="green_sub_text3" id="green_sub_text3" value="<?php echo $home['green_sub_text3'];?>" class="form-control" placeholder="Your house is Tidy!" />
			</div>
			
			 
            <div class="form-group">
				<label class="control-label"><strong>Gurantee Section</strong></label>
        	</div>
             <div class="form-group">
				<label class="control-label">Heading :</label>
                
                  <input type="text" maxlength="150" name="tidy_head" id="tidy_head" value="<?php echo $home['tidy_head'];?>" class="form-control required" placeholder="100% tidyness guaranteed." />
			</div>
            <div class="form-group">
				<label class="control-label">Sub Heading :</label>
                
                  <input type="text" maxlength="150" name="tidy_sub_head" id="tidy_sub_head " value="<?php echo $home['tidy_sub_head'];?>" class="form-control required" placeholder="If the customer is unhappy we will redo the job for free." />
			</div>
            
            <div class="form-group">
				<label class="control-label"><strong>Commitment Section</strong></label>
        	</div>
             <div class="form-group">
				<label class="control-label">Heading :</label>
                
                  <input type="text" maxlength="150" name="com_head" id="com_head" value="<?php echo $home['com_head'];?>" class="form-control required" placeholder="Our Commitment to Trust and Safety" />
			</div>
            <div class="form-group">
				<label class="control-label">Text :</label>
                
                  <input type="text" maxlength="250" name="com_text" id="com_text" value="<?php echo $home['com_text'];?>" class="form-control required" placeholder="Your home is your sanctuary. At TidyUpp we go above and beyond to create a more trusted and reliable experience." />
			</div>
            
            <div class="form-group">
				<label class="control-label">Feature 1 :</label>
                
                  <input type="text" maxlength="150" name="com_p1" id="com_p1" value="<?php echo $home['com_p1'];?>" class="form-control" placeholder=" Commitment to creating a safe and trusted environment" />
			</div>
            <div class="form-group">
				<label class="control-label">Feature 2 :</label>
                
                  <input type="text" maxlength="150" name="com_p2" id="com_p2" value="<?php echo $home['com_p2'];?>" class="form-control" placeholder="Your safety and peace of mind are our top priority" />
			</div>
            <div class="form-group">
				<label class="control-label">Feature 3 :</label>
                
                  <input type="text" maxlength="150" name="com_p3" id="com_p3" value="<?php echo $home['com_p3'];?>" class="form-control" placeholder="Background Checks" />
			</div>
            <div class="form-group">
				<label class="control-label">Feature 4 :</label>
                
                  <input type="text" maxlength="150" name="com_p4" id="com_p4" value="<?php echo $home['com_p4'];?>" class="form-control" placeholder="High standards for quality" />
			</div>
              <div class="form-group">
				<label class="control-label">Feature 5 :</label>
                
                  <input type="text" maxlength="150" name="com_p5" id="com_p5" value="<?php echo $home['com_p5'];?>" class="form-control" placeholder="A secure platform" />
			</div>
              <div class="form-group">
				<label class="control-label">Feature 6 :</label>
                
                  <input type="text" maxlength="150" name="com_p6" id="com_p6" value="<?php echo $home['com_p6'];?>" class="form-control" placeholder="Extra Field" />
			</div>
              <div class="form-group">
				<label class="control-label">Feature 7 :</label>
                
                  <input type="text" maxlength="150" name="com_p7" id="com_p7" value="<?php echo $home['com_p7'];?>" class="form-control" placeholder="Extra Field" />
			</div>
            
          
			<div class="form-group">
				
				<button type="button" class="btn btn-danger" onclick="window.location='<?php echo ADMIN_URL;?>'">Cancel</button>
                <button type="submit" value="Submit" name="homesubmit" id="homesubmit" class="btn btn-success">Submit</button>
			</div>
			
			
			
			
			
			
			
			
		
		</form>
	
  
</div>
  
  </div>

