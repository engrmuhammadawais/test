
<?php 
$crumb2 = "";



 if($alert=="error"||$alert=="edit"){
	$faq_question = $tbl_data['faq_question'];
	$faq_answer = $tbl_data['faq_answer'];
	$faq_status = $tbl_data['faq_status']; 
	 }
 else{
	$faq_question="";
	$faq_answer="";
	$faq_status = "Enable";
	}
 
 if($alert=="edit")
 {
	$crumb = "Edit";
	$action = "editRecord/".$tbl_data['faq_id']; 
	
 }
 else{
	$crumb = "Add";
	$action = "addRecord"; 
 }
 ?>

<ol class="breadcrumb bc-3">
<li><a href="<?php echo ADMIN_URL;?>"><i class="entypo-home"></i>Home</a></li>
<li><a href="<?php echo ADMIN_URL;?>faq"><i></i>FAQs</a></li>
<li class="active"><strong><?php echo $crumb;?> FAQ</strong></li>
</ol>
     
     
     


<h2><?php echo $crumb;?> FAQ</h2>
<br />


<div class="panel panel-primary">

	
	
	<div class="panel-body">
		 <form  id="faq_form" name="faq_form" class="validate" method="post" action="<?php echo base_url('manage/faq/'.$action);?>" enctype="multipart/form-data">
			<div class="form-group">
				<label class="control-label">Question :</label>
                
                  <textarea  name="faq_question" id="faq_question" class="form-control required rnone" placeholder="Question?" ><?php echo $faq_question;?></textarea>
			</div>
            <div class="form-group">
				<label class="control-label">Answer :</label>
                
                  <textarea name="faq_answer" id="faq_answer"  class="form-control required rnone" placeholder="Answer" ><?php echo $faq_answer;?></textarea>
			</div>
			
		<div class="form-group">
				 <label class="control-label">Status :</label>
                
                  <select class="form-control"  name="faq_status" id="faq_status">
                    <option value="Enable" <?php if($faq_status=="Enable"){ echo ' selected="selected"';} ?>>Enable</option>
                    <option value="Disable" <?php if($faq_status=="Disable"){ echo ' selected="selected"';} ?>>Disable</option>
                  </select>
			</div>
			<div class="form-group">
				
				<button type="button" class="btn btn-danger" onclick="window.location='<?php echo ADMIN_URL;?>faq'">Cancel</button>
                <button type="submit" class="btn btn-success">Submit</button>
			</div>
		
		</form>
	
  
</div>
  
  </div>

