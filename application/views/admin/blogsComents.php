<ol class="breadcrumb bc-3">
<li><a href="<?php echo ADMIN_URL;?>"><i class="entypo-home"></i>Home</a></li>
<li><a href="<?php echo ADMIN_URL;?>manage/blogs">Blogs</a></li>
<li class="active"><strong>Comments</strong></li>
</ol>

 <?php if($alert=="success") { ?>
 <div class="row alertrow">
	<div class="col-md-12">
    <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-success"><strong>Success!</strong> Comment added sucessfully.</div>
	</div>
</div>
   <?php } if($alert=="deletesuccess") { ?>
 <div class="row alertrow">
	<div class="col-md-12">
    <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-success"><strong>Success!</strong> Comment deleted sucessfully.</div>
	</div>
</div>
   <?php }if($alert=="postsuccess") { ?>
 <div class="row alertrow">
	<div class="col-md-12">
    <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-success"><strong>Success!</strong> Comment publish sucessfully.</div>
	</div>
</div>
   <?php }if($alert=="unpostsuccess") { ?>
 <div class="row alertrow">
	<div class="col-md-12">
    <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-success"><strong>Success!</strong> Comment unpublish sucessfully.</div>
	</div>
</div>
   <?php } if($alert=="deleteerror") { ?>
  <div class="row alertrow">
	<div class="col-md-12">
     <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-danger"><strong>Error!!</strong> Error occurred while deleting the record, please try again.</div>
	</div>
</div>
   <?php } if($alert=="editsuccess") { ?>
   <div class="row alertrow">
	<div class="col-md-12">
    <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-success"><strong>Success!</strong> Comment updated sucessfully.</div>
	</div>
</div>
   <?php } if($alert=="error") { ?>
  <div class="row alertrow">
	<div class="col-md-12">
     <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-danger"><strong>Error!!</strong> Error occurred while saving the record, please try again.</div>
	</div>
</div>
  <?php } ?>                    


<h2>Blog Comments</h2>
<hr />



<div class="row" style="min-height:400px;">
	<div class="col-md-12">
		<button  id="deleteAllRecords" class="btn btn-default btn-icon icon-left" type="button">
            Delete Selected
            <i class="entypo-trash"></i>
        </button> 
        
        
		<hr />
		<form action="<?php echo ADMIN_URL;?>blogs/deleteallomment/<?php echo $blog_id ?>" method="post" name="multiDel" id="multiDel">
		<table class="table table-hover table-striped">
			<thead>
				<tr>
					<th><input type="checkbox" id="all-checkbox" name="all-checkbox" autocomplete="off"></th>
                    <th>ID</th>
                    <th>Name</th>
                    <th class="hideCol">Email</th> 
                    <th class="hideCol">Comments</th>
                    <th>Date Posted</th>
                    <th class="hideCol">Actions</th>
				</tr>
			</thead>
			
			<tbody>
            <?php
			  if(count($listing)>0)
			  {
				foreach($listing as $c)
				{
					?>
              <tr>
                  <td><input name="records[]" autocomplete="off" class="cselect" value="<?php echo $c['blog_id'];?>" type="checkbox" /></td>
                 <td><?php echo $c['id'];?></td>
                  <td ><?php echo $c['name'] ?></td>
                  
                  <td class="hideCol"><?php echo $c['email'];?></td>
				  <td class="hideCol"><?php echo $c['comment']; ?></td>
				  <td><?php echo date('M, d Y h:i:a',strtotime($c['dt']));?></td>
                  
                  <td>
					<a class="icon-left font16 postcomment" title="<?php echo ($c['status']=='Published') ? 'Unpublished' : 'Published'; ?> Comment" 
					href="javascript:void(0);" data-controller="blogs" id="commentID<?php echo $c['id'];?>">
					<i class="entypo-eye"></i></a>
					<a class="icon-left font16 postcomment" title="Delete Comment" href="javascript:void(0);" data-controller="blogs" id="commentID<?php echo $c['id'];?>">
					<i class="entypo-cancel"></i></a>
                    
                  </td>
                </tr>      
                    
			 <?php		
				}
			  }
			  else{ ?>
				<tr><td colspan="7">Sorry! No Records.</td></tr>
              <?php  
			  }
			  ?>
				
			</tbody>
		</table>
        </form>
		
	</div>
	
	
</div>



