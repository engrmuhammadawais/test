<ol class="breadcrumb bc-3">
<li><a href="<?php echo ADMIN_URL;?>"><i class="entypo-home"></i>Home</a></li>
<li class="active"><strong>Articles</strong></li>
</ol>

 <?php if($alert=="success") { ?>
 <div class="row alertrow">
	<div class="col-md-12">
    <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-success"><strong>Success!</strong> Article added sucessfully.</div>
	</div>
</div>
   <?php } if($alert=="deletesuccess") { ?>
 <div class="row alertrow">
	<div class="col-md-12">
    <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-success"><strong>Success!</strong> Article deleted sucessfully.</div>
	</div>
</div>
   <?php } if($alert=="deleteerror") { ?>
  <div class="row alertrow">
	<div class="col-md-12">
     <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-danger"><strong>Error!!</strong> Error occurred while deleting the record, please try again.</div>
	</div>
</div>
   <?php } if($alert=="editsuccess") { ?>
   <div class="row alertrow">
	<div class="col-md-12">
    <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-success"><strong>Success!</strong> Article updated sucessfully.</div>
	</div>
</div>
   <?php } if($alert=="error") { ?>
  <div class="row alertrow">
	<div class="col-md-12">
     <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-danger"><strong>Error!!</strong> Error occurred while saving the record, please try again.</div>
	</div>
</div>
  <?php } ?>                    


<h2>Articles</h2>
<hr />



<div class="row" style="min-height:400px;">
	<div class="col-md-12">
		<button  id="deleteAllRecords" class="btn btn-default btn-icon icon-left" type="button">
            Delete Selected
            <i class="entypo-trash"></i>
        </button> 
        
        <button  class="btn btn-default btn-icon icon-left" type="button" onclick="javascript:window.location='<?php echo base_url();?>manage/blogs/control'">
            Add Article
            <i class="entypo-plus-circled"></i>
        </button>
		<br/>
		
        <select onchange="filterByCat(this.value)" style="margin-top:10px;" class="form-control" id="article_cat" name="article_cat" data-controller="blogs" >
		<option value ="" >Select Category</option>
		<?php if($blog_categories){
			foreach($blog_categories as $blog_category){ ?>
				<option value="<?php echo $blog_category['cat_id'] ?>" <? if($cat_id == $blog_category['cat_id']){ echo ' selected="selected"'; } ?> ><?php echo $blog_category['cat_name'] ?></option>
			<?php
			}
		}?>
		</select>
		<br/>
		<?php if($cat_id){ ?>
		<button  class="btn btn-default btn-icon" type="button" onclick="javascript:window.location='<?php echo base_url();?>manage/blogs/'">
            Clear Search
        </button><?php }?>
		<hr />
		<form action="<?php echo ADMIN_URL;?>blogs/deleteall" method="post" name="multiDel" id="multiDel">
		<table class="table table-hover table-striped">
			<thead>
				<tr>
					<th><input type="checkbox" id="all-checkbox" name="all-checkbox" autocomplete="off"></th>
                    <th><a href="<?php echo base_url();?>manage/blogs/index/page/blog_id/<?php echo $order;?>/<?php echo $blog_numb;?>">ID</a></th>
					<th class="hideCol">Image</th>
                    <th><a href="<?php echo base_url();?>manage/blogs/index/page/blog_name/<?php echo $order;?>/<?php echo $blog_numb;?>">Name</a></th>
                    <th><a href="<?php echo base_url();?>manage/blogs/index/page/blog_status/<?php echo $order;?>/<?php echo $blog_numb;?>">Status</a></th>
                    <th class="hideCol"><a href="<?php echo base_url();?>manage/blogs/index/page/blog_added/<?php echo $order;?>/<?php echo $blog_numb;?>">Added</a></th>
                    <th>Actions</th>
				</tr>
			</thead>
			
			<tbody>
            <?php
			  if(count($listing)>0)
			  {
				foreach($listing as $c)
				{
					?>
              <tr>
                  <td><input name="records[]" autocomplete="off" class="cselect" value="<?php echo $c['blog_id'];?>" type="checkbox" /></td>
                 <td><?php echo $c['blog_id'];?></td>
                  <td class="hideCol"><img width="100" src="<?php echo FRONTEND_ASSETS;?>images/blog/<?php echo ($c['blog_thumb']!="") ? $c['blog_thumb']: 'no_image.jpg';?>" /></td>
                  
                  <td><?php echo $c['blog_name'];?></td>
                  <td><?php echo $c['blog_status'];?></td>
                  <td class="hideCol"><?php echo date('M d, Y h:i a', strtotime($c['blog_added']));?></td>
                  <td>
                 	<a class="icon-left font16" href="<?php echo base_url();?>manage/blogs/control/edit/<?php echo $c['blog_id'];?>">
					<i class="entypo-pencil"></i></a>
					
					<a href="<?php echo base_url();?>manage/blogs/comments/<?php echo $c['blog_id']?>" title="View Comments" class="icon-left font16" >
					<i class="entypo-eye"></i></a>
				
					<a class="icon-left font16 delitem" href="javascript:void(0);" data-controller="blogs" id="recordID<?php echo $c['blog_id'];?>">
					<i class="entypo-cancel"></i></a>
                    
                  </td>
                </tr>      
                    
			 <?php		
				}
			  }
			  else{ ?>
				<tr><td colspan="7">Sorry! No Records.</td></tr>
              <?php  
			  }
			  ?>
				
			</tbody>
		</table>
        </form>
        <?php 
 $total_pages = ceil($total_rows/$per_page); 
 $current_page = ceil($blog_numb/$per_page)+1;
 if($total_pages=="1")
 {
	$showing_from = 1;
	$showing_to = $total_rows;
 }
 else if($total_pages==$current_page)
 {
	$showing_from = ($per_page*($current_page-1))+1;
	$showing_to = $total_rows; 
 }
 else{
	$showing_from = ($per_page*($current_page-1))+1;
	$showing_to = $per_page*$current_page;
 }
 if($total_rows>0)
 {
?>
Showing <?php echo $showing_from;?> to <?php echo $showing_to;?> of <?php echo $total_rows;?> (<?php echo $total_pages;?> Pages) <?php } echo $paginate;?> 
		
	</div>
	
	
</div>



