<?php 
$crumb2 = "";

 if($alert=="error"||$alert=="edit" || $alert=="image_error"){
	$page_name = $tbl_data['page_name'];
	$page_uri = $tbl_data['page_uri'];
	$page_text = stripslashes($tbl_data['page_text']);
	$page_status = $tbl_data['page_status']; 
	$page_meta_key = stripslashes($tbl_data['page_meta_key']);
	$page_meta_desc = stripslashes($tbl_data['page_meta_desc']);
	$page_extra_tags = stripslashes($tbl_data['page_extra_tags']);
 	$pub_date = ($tbl_data['page_pdate']!="") ? date('d F Y',strtotime($tbl_data['page_pdate'])) : '';
	$pub_time = ($tbl_data['page_pdate']!="") ? date('H:i:s A',strtotime($tbl_data['page_pdate'])) : '';
 	$page_featured = $tbl_data['page_featured'];
	$page_caption = $tbl_data['page_caption'];
	$page_head = $tbl_data['page_head'];
	$show_in_menu = $tbl_data['show_in_menu'];
 }
 else{
	 $page_head = "";
	$page_uri = "";
	$page_name="";
	$page_text="";
	$page_status = "Published";
	$page_meta_key = "";
	$page_meta_desc = "";
	$page_extra_tags = "";
	$page_pdate = "";
	$pub_date = date('d F Y');
	$pub_time = date('H:i:s A');
	$page_featured = "No";
	$page_caption= "";
	$show_in_menu = "No";
 }
 
 if($alert=="edit")
 {
	$crumb = "Edit";
	$action = "editRecord/".$tbl_data['page_id']; 
	
 }
 else{
	$crumb = "Add";
	$action = "addRecord"; 
 }
 ?>

<ol class="breadcrumb bc-3">
<li><a href="<?php echo ADMIN_URL;?>"><i class="entypo-home"></i>Home</a></li>
<li><a href="<?php echo ADMIN_URL;?>pages"><i></i>Web Pages</a></li>
<li class="active"><strong><?php echo $crumb;?> Web page</strong></li>
</ol>
     
     
     
<?php if($alert=="success") { ?>
<div class="row alertrow">
	<div class="col-md-12">
    <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-success"><strong>Success!</strong> Settings saved sucessfully.</div>
	</div>
</div>
<?php } if($alert=="error"||$editerror=="editerror") { ?>
<div class="row alertrow">
	<div class="col-md-12">
     <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-danger"><strong>Error!!</strong> The user name or email you specified is already exist, please use different user name or email.</div>
	</div>
</div>
<?php } else if($alert=="image_error"||$editerror=="image_error"){ ?>
<div class="row alertrow">
	<div class="col-md-12">
     <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-danger"><strong>Error!!</strong> Unable to upload the image, please check the file format and size.</div>
	</div>
</div>
  
<?php } if($alert=="perror") { ?>
<div class="row alertrow">
	<div class="col-md-12">
     <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-danger"><strong>Error!!</strong> Unable to change the password, please provide the correct current password.</div>
	</div>
</div>

<?php } ?>     
     
     

<h2><?php echo $crumb;?> Web Page</h2>
<br />


<div class="panel panel-primary">

	
	
	<div class="panel-body">
		 <form  id="page_form" name="page_form" method="post" action="<?php echo base_url();?>manage/pages/<?php echo $action;?>" enctype="multipart/form-data" class="validate">
			<div class="form-group" >
				<label class="control-label">Page Title :</label>
                
                  <input type="text" name="page_name" id="page_name" value="<?php echo $page_name;?>" class="form-control" placeholder="Page Name" data-validate="required,maxlength[250]" />
			</div>
            <div class="form-group" >
				<label class="control-label">Heading :</label>
                
                  <input type="text" name="page_head" id="page_head" value="<?php echo $page_head;?>" class="form-control" placeholder="Page Heading" data-validate="required,maxlength[250]" />
			</div>
            
            <div class="form-group" <?php echo (isset($tbl_data['page_id'])&&$tbl_data['page_id']=="1")? 'style="display:none"' :'';?>>
					
						
						<div class="col-sm-8" style="padding-left:0px;">
								<label class="control-label">Slug :</label><br clear="all" />
							<div class="pull-left">
								<input type="text" style="width:200px" name="page_uri" id="page_uri" value="<?php echo $page_uri;?>" class="form-control aplhasmall" placeholder="about-us" data-validate="required,maxlength[250]"/>
							</div>
                            <div class="pull-left" style="margin-top:7px">.html</div>
						</div>
					</div>
                    <br clear="all" /><br clear="all" />
                    <div class="form-group" style="display:none">
				<label class="control-label">Page Caption :</label>
                
                  <textarea name="page_caption" id="page_caption"  class="form-control" style="resize:none"/><?php echo $page_caption;?></textarea>
			</div>
            
                    <div class="form-group" style="display:none">
				 <label class="control-label">Background Image :</label>
               
                  <input type="file" name="uploadfile" id="uploadfile" accept="image/*"/>
                  <span style="display:none" id="ufile_error" for="uploadfile" generated="true" class="help-inline">Please select image.</span>
			</div>
			
			<div class="form-group" style="display:none">
				 <label <?php echo (isset($tbl_data['page_id'])) ? 'id="image'.$tbl_data['page_id'].'"' : '';?> class="control-label"><?php if($alert=="edit"&&$tbl_data['page_thumb_image']!=""&&file_exists('./assets/frontend/images/pages/'.$tbl_data['page_thumb_image']))
				{
					echo '<img src="'.FRONTEND_ASSETS.'images/pages/'.$tbl_data['page_thumb_image'].'">';	
					echo '<br/><br/><button class="btn btn-primary btn-xs removeImage" data-controller="pages" id="img'.$tbl_data['page_id'].'" type="button">Remove Image</button><br/>';
				}?></label><br/><strong>Max Size:</strong> 10 Mb. <strong>Image Type:</strong> JPEG / PNG<br/>
                 <strong>Width:</strong> 1920px 
			</div>
            
            
            <div class="form-group" <?php echo (isset($tbl_data['page_id'])&&$tbl_data['page_id']=="1")? 'style="display:none"' :'';?>>
				<?php echo $this->ckeditor->editor("page_text",$page_text);?>
			</div>
            <div class="form-group" style="display:none">
				 <label class="control-label">Featured :</label>
                
                  <select class="form-control"  name="page_featured" id="page_featured">
                    <option value="Yes" <?php if($page_featured=="Yes"){ echo ' selected="selected"';} ?>>Yes</option>
                    <option value="No" <?php if($page_featured=="No"){ echo ' selected="selected"';} ?>>No</option>
                  </select>
			</div>
            <div class="form-group">
				 <label class="control-label">Show In Menu :</label>
                
                  <select class="form-control"  name="show_in_menu" id="show_in_menu">
                    <option value="Yes" <?php if($show_in_menu=="Yes"){ echo ' selected="selected"';} ?>>Yes</option>
                    <option value="No" <?php if($show_in_menu=="No"){ echo ' selected="selected"';} ?>>No</option>
                  </select>
			</div>
            <div class="form-group" <?php echo (isset($tbl_data['page_id'])&&$tbl_data['page_id']<=2)? 'style="display:none"' :'';?>>
				 <label class="control-label">Status :</label>
                
                  <select class="form-control"  name="page_status" id="page_status">
                    <option value="Published" <?php if($page_status=="Published"){ echo ' selected="selected"';} ?>>Published</option>
                    <option value="Un-Published" <?php if($page_status=="Un-Published"){ echo ' selected="selected"';} ?>>Un-Published</option>
                  </select>
			</div>
            <div class="form-group" style="display:none">
					
						
						<div class="col-sm-4" style="padding-left:0px;">
								<label class="control-label">Pusblish/Un-Pusblish On :</label>
							<div class="date-and-time">
								<input  name="pub_date" id="pub_date" type="text" autocomplete="off" class="form-control datepicker" value="<?php echo $pub_date;?>" data-format="dd MM yyyy">
								<input id="pub_time" name="pub_time"  type="text" autocomplete="off" class="form-control timepicker" data-template="dropdown" data-show-seconds="true" data-default-time="<?php echo $pub_time;?>" data-show-meridian="true" data-minute-step="1" data-second-step="1" />
							</div>
						</div>
					</div>
            
           <!--  <br clear="all" /><br clear="all" />-->
            <div class="form-group">
				<label class="control-label">Meta Keywords :</label>
                
                  <textarea style="resize:none" name="page_meta_key" id="page_meta_key"  class="form-control" placeholder="" /><?php echo $page_meta_key;?></textarea>
			</div>
            <div class="form-group">
				<label class="control-label">Meta Description :</label>
                
                  <textarea style="resize:none" name="page_meta_desc" id="page_meta_desc"  class="form-control" placeholder="" /><?php echo $page_meta_desc;?></textarea>
			</div>
            <div class="form-group" style="display:none">
				<label class="control-label">Extra SEO Tags :</label>
                
                  <textarea style="resize:none" name="page_extra_tags" id="page_extra_tags"  class="form-control" placeholder="" /><?php echo $page_extra_tags;?></textarea>
			</div>
            
           
			
			
			<div class="form-group" >
				
				<button type="button" class="btn btn-danger" onclick="window.location='<?php echo ADMIN_URL;?>pages'">Cancel</button>
                <button type="submit"  class="btn btn-success">Submit</button>
			</div>
		
		</form>
	
  
	
</div>
  
  </div>


