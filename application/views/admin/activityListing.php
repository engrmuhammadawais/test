<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="<?php echo base_url('manage');?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> | <a href="<?php echo base_url('manage/'.$this->controller);?>" class="tip-bottom current"><?php echo $this->moduleName;?></a> </div>

    <h1><?php echo $this->moduleName;?></h1>
  </div>
  
  
  
  
  <form action="<?php echo base_url('manage/'.$this->controller.'/deleteall');?>" method="post" name="delRecords" id="delRecords">
  <div class="container-fluid">
    <div class="widget-box">
          
          <div class="widget-content nopadding">
            <table id="listing-table"  class="table table-bordered table-striped with-check">
              <thead>
                <tr>
                  <th ><a href="<?php echo base_url('manage/'.$this->controller.'/index/user_id/'.$order.'/'.$status.'/'.$keywords.'/'.$page_numb);?>">UserName</a></th>
                  <th ><a href="<?php echo base_url('manage/'.$this->controller.'/index/datetime/'.$order.'/'.$status.'/'.$keywords.'/'.$page_numb);?>">Datetime</a></th>
                  <th ><a href="<?php echo base_url('manage/'.$this->controller.'/index/log_key/'.$order.'/'.$status.'/'.$keywords.'/'.$page_numb);?>">Main Activity</a></th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
              <?php
              if(is_array($rows)&&count($rows)>0)
              {
                foreach($rows as $r)
                {
                    ?>
              <tr id="row-<?php echo $r[$this->pKey];?>">
                  <td><a href="<?php echo base_url('manage/'.$this->controller.'/control/'.$r[$this->pKey]);?>"><?php echo $r['user_name'];?></a></td>
                  
                  <td><a href="<?php echo base_url('manage/'.$this->controller.'/control/'.$r[$this->pKey]);?>"><?php echo date('M, d Y',strtotime($r['datetime']));?></a></td>
                  
                  <td><a href="<?php echo base_url('manage/'.$this->controller.'/control/'.$r[$this->pKey]);?>"><?php echo humanize($r['log_key']);?></a></td>


                  <td style="text-align:center"><a href="<?php echo base_url('manage/'.$this->controller.'/control/'.$r['id']);?>" title="View Record"><i class="entypo-eye"></i></a> 
                
                  </td>
                </tr>      
                    
             <?php      
                }
              }
              else{ ?>
                <tr><td colspan="6">Sorry! No records found.</td></tr>
              <?php  
              }
              ?>
              
              
                
              </tbody>
            </table>
 <?php 
 $total_pages = ((int) $per_page === 0 )  ? '1' : ceil($total_rows/$per_page); 
 $current_page = ((int) $per_page === 0 )  ? '1' : ceil($page_numb/$per_page)+1; 
 if($total_pages=="1")
 {
    $showing_from = 1;
    $showing_to = $total_rows;
 }
 else if($total_pages==$current_page)
 {
    $showing_from = ($per_page*($current_page-1))+1;
    $showing_to = $total_rows; 
 }
 else{
    $showing_from = ($per_page*($current_page-1))+1;
    $showing_to = $per_page*$current_page;
 }
 if($total_rows>0)
 {
?>
Showing <?php echo $showing_from;?> to <?php echo $showing_to;?> of <?php echo $total_rows;?> Record<?php echo ((int)$total_rows>1)? 's' : '';?> (Total <?php echo $total_pages;?> Page<?php echo ((int)$total_pages>1)? 's' : '';?>) <?php } echo (isset($paginate)&&$paginate!="") ? $paginate : '';?> 
          </div>
        
        </div>
   
  </div>
 </form>
</div>

<script type="text/javascript">
$("#search_status").on('change',function(){
    var search_keywords = $("#search_keywords").val();
    search_keywords = (search_keywords=="") ? "-" : encodeURI(search_keywords);
    window.location = "<?php echo base_url('manage/'.$this->controller.'/index/'.$sortby.'/'.(($order=="ASC") ? 'DESC' : 'ASC'));?>/"+$("#search_status").val()+"/"+search_keywords;
});

$("#search_keywords").keypress(function(event) {
    if (event.keyCode == 13) {
        var search_keywords = $("#search_keywords").val();
    search_keywords = (search_keywords=="") ? "-" : encodeURI(search_keywords);
        window.location = "<?php echo base_url('manage/'.$this->controller.'/index/'.$sortby.'/'.(($order=="ASC") ? 'DESC' : 'ASC'));?>/"+$("#search_status").val()+"/"+search_keywords;
        return false;
    }
}); 
</script>