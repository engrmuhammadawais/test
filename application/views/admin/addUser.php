 <?php
 if($alert=="error"||$alert=="edit"){
	$full_name = $tbl_data['full_name'];
	$user_name = $tbl_data['user_name'];
	$email = $tbl_data['email'];
	$pwd = $tbl_data['pwd'];
	$pwd2 = $tbl_data['pwd'];
	$status = $tbl_data['status']; 
	$user_role = $tbl_data['user_role']; 
	
 }
 else{
	$full_name="";
	$user_name = "";
	$email = "";
	$pwd = "";
	$pwd2 = "";
	$user_role = "Admin";
	$status = "Enable";
 }
if($alert=="error")
{
$pwd = "";
$pwd2 = "";	
}

 
 if($alert=="edit")
 {
	$crumb = "Edit";
	$action = "editRecord/".$tbl_data['id']; 
 }
 else{
	$crumb = "Add";
	$action = "addRecord"; 
 }
 ?>
<ol class="breadcrumb bc-3">
<li><a href="<?php echo ADMIN_URL;?>"><i class="entypo-home"></i>Home</a></li>
<li><a href="<?php echo ADMIN_URL;?>users"><i></i>Users</a></li>
<li class="active"><strong><?php echo $crumb;?> User</strong></li>
</ol>
     
     
     
<?php if($alert=="success") { ?>
<div class="row alertrow">
	<div class="col-md-12">
    <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-success"><strong>Success!</strong> Settings saved sucessfully.</div>
	</div>
</div>
<?php } if($alert=="error"||$editerror=="editerror") { ?>
<div class="row alertrow">
	<div class="col-md-12">
     <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-danger"><strong>Error!!</strong> The user name or email you specified is already exist, please use different user name or email.</div>
	</div>
</div>
<?php } if($alert=="perror") { ?>
<div class="row alertrow">
	<div class="col-md-12">
     <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-danger"><strong>Error!!</strong> Unable to change the password, please provide the correct current password.</div>
	</div>
</div>

<?php } ?>     
     
     

<h2><?php echo $crumb;?> User</h2>
<br />


<div class="panel panel-primary">

	
	
	<div class="panel-body">
	
		<form role="form"  method="post"  id="admin_form"   name="admin_form" method="post" action="<?php echo ADMIN_URL;?>users/<?php echo $action;?>" >
			
			<div class="form-group">
				<label class="control-label">Name :</label>
				
				<input type="text" class="form-control"   name="full_name" id="full_name" value="<?php echo $full_name;?>" placeholder="Example: John Smith" />
			</div>
            
            
			
			<div class="form-group">
				<label class="control-label">Email :</label>
                
                  <input type="text" class="form-control"   name="email" id="email" value="<?php echo $email;?>" placeholder="Example: john_smith@dummyemail.com" />
			</div>
			
			<div class="form-group">
				 <label class="control-label">User Name :</label>
                
                  <input type="text" class="form-control"   name="user_name" id="user_name" value="<?php echo $user_name;?>" placeholder="Example: john.smith" />
			</div>
			
			<div class="form-group">
				 <label class="control-label">Password :</label>
               
                  <input type="password" class="form-control"   name="pwd" id="pwd" value="<?php echo $pwd;?>" placeholder="Example: sM!thJ@hN" />
			</div>
			
			<div class="form-group">
				 <label class="control-label">Confirm Password :</label>
               
                  <input type="password" class="form-control"   name="pwd2" id="pwd2" value="<?php echo $pwd2;?>" placeholder="Example: sM!thJ@hN" />
			</div>
			<div class="form-group">
				 <label class="control-label">User Role :</label>
                
                  <select class="form-control"  name="user_role" id="user_role">
                    <option value="Admin" <?php if($user_role=="Admin"){ echo ' selected="selected"';} ?>>Admin</option>
                    <option value="Super Admin" <?php if($user_role=="Super Admin"){ echo ' selected="selected"';} ?>>Super Admin</option>
                  </select>
			</div>
			<div class="form-group">
				 <label class="control-label">Status :</label>
                
                  <select class="form-control"  name="status" id="status">
                    <option value="Enable" <?php if($status=="Enable"){ echo ' selected="selected"';} ?>>Enable</option>
                    <option value="Disable" <?php if($status=="Disable"){ echo ' selected="selected"';} ?>>Disable</option>
                  </select>
			</div>
			<div class="form-group">
				
				<button type="button" class="btn btn-danger" onclick="window.location='<?php echo ADMIN_URL;?>users'">Cancel</button>
                <button type="submit" class="btn btn-success">Submit</button>
			</div>
		
		</form>
	
 
</div>
  
  </div>


