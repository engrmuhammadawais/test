<ol class="breadcrumb bc-3">
<li><a href="<?php echo ADMIN_URL;?>"><i class="entypo-home"></i>Home</a></li>
<li class="active"><strong>Blog Tags</strong></li>
</ol>

 <?php if($alert=="success") { ?>
 <div class="row alertrow">
	<div class="col-md-12">
    <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-success"><strong>Success!</strong> Tag added sucessfully.</div>
	</div>
</div>
   <?php } if($alert=="deletesuccess") { ?>
 <div class="row alertrow">
	<div class="col-md-12">
    <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-success"><strong>Success!</strong> Tag deleted sucessfully.</div>
	</div>
</div>
   <?php } if($alert=="deleteerror") { ?>
  <div class="row alertrow">
	<div class="col-md-12">
     <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-danger"><strong>Error!!</strong> Error occurred while deleting the record, please try again.</div>
	</div>
</div>
   <?php } if($alert=="editsuccess") { ?>
   <div class="row alertrow">
	<div class="col-md-12">
    <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-success"><strong>Success!</strong> Tag updated sucessfully.</div>
	</div>
</div>
   <?php } if($alert=="error") { ?>
  <div class="row alertrow">
	<div class="col-md-12">
     <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-danger"><strong>Error!!</strong> Error occurred while saving the record, please try again.</div>
	</div>
</div>
  <?php } ?>                    


<h2>Blog Tags</h2>
<hr />



<div class="row" style="min-height:400px;">
	<div class="col-md-12">
		<button  id="deleteAllRecords" class="btn btn-default btn-icon icon-left" type="button">
            Delete Selected
            <i class="entypo-trash"></i>
        </button> 
        
        <button  class="btn btn-default btn-icon icon-left" type="button" onclick="javascript:window.location='<?php echo base_url();?>manage/blogtags/control'">
            Add Tag
            <i class="entypo-plus-circled"></i>
        </button>
		<hr />
		<form action="<?php echo ADMIN_URL;?>blogtags/deleteall" method="post" name="multiDel" id="multiDel">
		<table class="table table-hover table-striped">
			<thead>
				<tr>
					<th><input type="checkbox" id="all-checkbox" name="all-checkbox" autocomplete="off"></th>
                    <th><a href="<?php echo base_url();?>manage/blogtags/index/page/tag_id/<?php echo $order;?>/<?php echo $page_numb;?>">ID</a></th>
					
                    <th><a href="<?php echo base_url();?>manage/blogtags/index/page/tag_name/<?php echo $order;?>/<?php echo $page_numb;?>">Name</a></th>
                    <th><a href="<?php echo base_url();?>manage/blogtags/index/page/tag_status/<?php echo $order;?>/<?php echo $page_numb;?>">Status</a></th>
                    <th class="hideCol"><a href="<?php echo base_url();?>manage/blogtags/index/page/tag_added/<?php echo $order;?>/<?php echo $page_numb;?>">Added</a></th>
                    <th>Actions</th>
				</tr>
			</thead>
			
			<tbody>
            <?php
			  if(count($listing)>0)
			  {
				foreach($listing as $c)
				{
					?>
              <tr>
                  <td><input name="records[]" autocomplete="off" class="cselect" value="<?php echo $c['tag_id'];?>" type="checkbox" /></td>
                 <td><?php echo $c['tag_id'];?></td>
                  
                  
                  <td><?php echo $c['tag_name'];?></td>
                  <td><?php echo $c['tag_status'];?></td>
                  <td class="hideCol"><?php echo date('M d, Y h:i a', strtotime($c['tag_added']));?></td>
                  <td>
                 	<a class="icon-left font16" href="<?php echo base_url();?>manage/blogtags/control/edit/<?php echo $c['tag_id'];?>">
					<i class="entypo-pencil"></i></a>
				
					<a class="icon-left font16 delitem" href="javascript:void(0);" data-controller="blogtags" id="recordID<?php echo $c['tag_id'];?>">
					<i class="entypo-cancel"></i></a>
                    
                  </td>
                </tr>      
                    
			 <?php		
				}
			  }
			  else{ ?>
				<tr><td colspan="6">Sorry! No Records.</td></tr>
              <?php  
			  }
			  ?>
				
			</tbody>
		</table>
        </form>
        <?php 
 $total_pages = ceil($total_rows/$per_page); 
 $current_page = ceil($page_numb/$per_page)+1;
 if($total_pages=="1")
 {
	$showing_from = 1;
	$showing_to = $total_rows;
 }
 else if($total_pages==$current_page)
 {
	$showing_from = ($per_page*($current_page-1))+1;
	$showing_to = $total_rows; 
 }
 else{
	$showing_from = ($per_page*($current_page-1))+1;
	$showing_to = $per_page*$current_page;
 }
 if($total_rows>0)
 {
?>
Showing <?php echo $showing_from;?> to <?php echo $showing_to;?> of <?php echo $total_rows;?> (<?php echo $total_pages;?> Pages) <?php } echo $paginate;?> 
		
	</div>
	
	
</div>



