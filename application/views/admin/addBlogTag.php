
<?php 
$crumb2 = "";



 if($alert=="error"||$alert=="edit" || $alert=="image_error"){
	$tag_name = $tbl_data['tag_name'];
	
	$tag_status = $tbl_data['tag_status']; 
	
 }
 else{
	$tag_name="";

	$tag_status = "Enable";
	
 }
 
 if($alert=="edit")
 {
	$crumb = "Edit";
	$action = "editRecord/".$tbl_data['tag_id']; 
	
 }
 else{
	$crumb = "Add";
	$action = "addRecord"; 
 }
 ?>

<ol class="breadcrumb bc-3">
<li><a href="<?php echo ADMIN_URL;?>"><i class="entypo-home"></i>Home</a></li>
<li><a href="<?php echo ADMIN_URL;?>blogtags"><i></i>Blog Tags</a></li>
<li class="active"><strong><?php echo $crumb;?> Tag</strong></li>
</ol>
     
     
     
<?php if($alert=="success") { ?>
<div class="row alertrow">
	<div class="col-md-12">
    <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-success"><strong>Success!</strong> Settings saved sucessfully.</div>
	</div>
</div>
<?php } if($alert=="error"||$editerror=="editerror") { ?>
<div class="row alertrow">
	<div class="col-md-12">
     <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-danger"><strong>Error!!</strong> The tag name you specified is already exist, please use different name.</div>
	</div>
</div>
<?php } else if($alert=="image_error"||$editerror=="image_error"){ ?>
<div class="row alertrow">
	<div class="col-md-12">
     <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-danger"><strong>Error!!</strong> Unable to upload the image, please check the file format and size.</div>
	</div>
</div>
  
<?php } if($alert=="perror") { ?>
<div class="row alertrow">
	<div class="col-md-12">
     <button class="close alertBox" data-dismiss="alert">x</button>
		<div class="alert alert-danger"><strong>Error!!</strong> Unable to change the password, please provide the correct current password.</div>
	</div>
</div>

<?php } ?>     
     
     

<h2><?php echo $crumb;?> Tag</h2>
<br />


<div class="panel panel-primary">

	
	
	<div class="panel-body">
		 <form  id="tag_form" name="tag_form" method="post" action="<?php echo base_url();?>manage/blogtags/<?php echo $action;?>" enctype="multipart/form-data" class="validate" >
			<div class="form-group">
				<label class="control-label">Tag Name :</label>
                
                  <input type="text" name="tag_name" id="tag_name" value="<?php echo $tag_name;?>" class="form-control" placeholder="Movies" data-validate="required,maxlength[50]"/>
			</div>
            
    	
			<div class="form-group">
				 <label class="control-label">Status :</label>
                
                  <select class="form-control"  name="tag_status" id="tag_status">
                    <option value="Enable" <?php if($tag_status=="Enable"){ echo ' selected="selected"';} ?>>Enable</option>
                    <option value="Disable" <?php if($tag_status=="Disable"){ echo ' selected="selected"';} ?>>Disable</option>
                  </select>
			</div>
			<div class="form-group">
				
				<button type="button" class="btn btn-danger" onclick="window.location='<?php echo ADMIN_URL;?>blogtags'">Cancel</button>
                <button type="submit" name="tag_submit" id="tag_submit" class="btn btn-success">Submit</button>
			</div>
		
		</form>
	
  </div>
  
  </div>



