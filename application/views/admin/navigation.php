<body class="page-body ">

<div class="page-container "><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->	
	
	<div class="sidebar-menu hidden-print">
		
			
		<header class="logo-env">
			
			<!-- logo -->
			<div class="logo">
				<a href="<?php echo ADMIN_URL;?>">
               
               		<?php 
					$logo = './assets/frontend/images/logo/'.$this->SqlModel->getSingleField('logo','site_settings',array('id'=>1));
					if(file_exists($logo))
					{
					?>
					<img src="<?php echo $this->imagethumb->image($logo,150,0);?>" alt="Admin Logo" />
					
                    <?php } ?>
				</a>
			</div>
			
						<!-- logo collapse icon -->
						
			<div class="sidebar-collapse">
				<a href="#" class="sidebar-collapse-icon with-animation"><!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
					<i class="entypo-menu"></i>
				</a>
			</div>
			
									
			
			<!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
			<div class="sidebar-mobile-menu visible-xs">
				<a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
					<i class="entypo-menu"></i>
				</a>
			</div>
			
		</header>
				
		
				
		<ul id="main-menu" class="">
			
			<li <?php echo (isset($dashBoardActive))? 'class="active"' : '';?>>
				<a href="<?php echo ADMIN_URL;?>">
					<i class="entypo-gauge"></i>
					<span>Dashboard</span>
				</a>
				
			</li>
            
			<li <?php echo (isset($usersActive))? 'class="active"' : '';?>>
				<a href="<?php echo ADMIN_URL;?>users">
					<i class="entypo-users"></i>
					<span>Users</span>
				</a>
				
			</li>
           
			
			<li <?php echo (isset($homeActive))? 'class="active"' : '';?>>
				<a href="<?php echo ADMIN_URL;?>home/edit">
					<i class="glyphicon glyphicon-th"></i>
					<span>Home Page</span>
				</a>
				
			</li>
            <li <?php echo (isset($faqActive))? 'class="active"' : '';?>>
				<a href="<?php echo ADMIN_URL;?>faq">
					<i class="glyphicon glyphicon-question-sign"></i>
					<span>FAQs</span>
				</a>
				
			</li>
			<li <?php echo (isset($activityActive))? 'class="active"' : '';?>>
				<a href="<?php echo ADMIN_URL;?>activity">
					<i class="entypo-layout"></i>
					<span>Activity</span>
				</a>			
			</li>
			
            <li <?php echo (isset($blogsActive))? 'class="opened"' : '';?>>
				<a href="<?php echo ADMIN_URL;?>blogs">
					<i class="entypo-layout"></i>
					<span>Articles</span>
				</a>
				<ul>
                <li <?php echo (isset($blogsMainActive))? 'class="active"' : '';?>>
				<a href="<?php echo ADMIN_URL;?>blogs">
							<span>Articles</span>
						</a>
					</li>
					<li <?php echo (isset($blogsCatActive))? 'class="active"' : '';?>>
				<a href="<?php echo ADMIN_URL;?>blogcategories">
							<span>Categories</span>
						</a>
					</li>
					<li <?php echo (isset($blogsTagActive))? 'class="active"' : '';?>>
				<a href="<?php echo ADMIN_URL;?>blogtags">
							<span>Tags</span>
						</a>
					</li>
			</ul>
			</li>
             
            <li <?php echo (isset($pagesActive))? 'class="active"' : '';?>>
				<a href="<?php echo ADMIN_URL;?>pages">
					<i class="entypo-doc-text-inv"></i>
					<span>Web Pages</span>
				</a>
				
			</li>
              
          <li <?php echo (isset($wsettingActive))? 'class="active"' : '';?>>
				<a href="<?php echo ADMIN_URL;?>home/websettings">
					<i class="entypo-cog"></i>
					<span>Website Settings</span>
				</a>
				
			</li>
            <li>
				<a href="<?php echo ADMIN_URL;?>home/logout">
					<i class="entypo-logout"></i>
					<span>Logout</span>
				</a>
				
			</li>
            
			
			
			
			
			
			
			
		</ul>
				
	</div>
<?php ################# END of Navigation ##################### ?>
<div class="main-content">
<div class="row hidden-print">
	
	<!-- Profile Info and Notifications -->
	<div class="col-md-6 col-sm-8 clearfix">
		
		<ul class="user-info pull-left pull-none-xsm">
		
			<!-- Profile Info -->
			<li class="profile-info"><!-- add class "pull-right" if you want to place this from right -->
				
				
					
					<strong>Welcome!</strong> <?php echo $userdata['full_name'];?> <br/><strong>Last login:</strong> <?php echo date('M d, Y h:ia',strtotime($this->session->userdata('admin_last_login')));?> | <strong>IP Address:</strong> <?php echo $this->session->userdata('admin_last_ip');?>
				
				
				
			</li>
		
		</ul>
		
		
	
	</div>
	
	
	<!-- Raw Links -->
	<div class="col-md-6 col-sm-4 clearfix hidden-xs">
		
		<ul class="list-inline links-list pull-right">
			<li>
				<a href="<?php echo ADMIN_URL.'home/settings';?>">
					Account Settings <i class="entypo-tools right"></i>
				</a>
			</li>
			
			
			<li class="sep"></li>
			
			<li>
				<a href="<?php echo ADMIN_URL.'home/logout';?>">
					Log Out <i class="entypo-logout right"></i>
				</a>
			</li>
		</ul>
		
	</div>
	
</div>
<hr />
<?php ################## Dashboard Scripts ################### ?>


<script type="text/javascript">
<?php if(isset($dashBoard)){?>	
jQuery(document).ready(function($) 
{/*
	// Sample Toastr Notification
	setTimeout(function()
	{			
		var opts = {
			"closeButton": true,
			"debug": false,
			"positionClass": "toast-top-right",
			"toastClass": "black",
			"onclick": null,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "5000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
		};

		toastr.success("You have been awarded with 1 year free subscription. Enjoy it!", "Account Subcription Updated", opts);
	}, 3000);
	
	*/
	// Sparkline Charts
	$('.inlinebar').sparkline('html', {type: 'bar', barColor: '#ff6264'} );
	$('.inlinebar-2').sparkline('html', {type: 'bar', barColor: '#445982'} );
	$('.inlinebar-3').sparkline('html', {type: 'bar', barColor: '#00b19d'} );
	$('.bar').sparkline([ [1,4], [2, 3], [3, 2], [4, 1] ], { type: 'bar' });
	$('.pie').sparkline('html', {type: 'pie',borderWidth: 0, sliceColors: ['#3d4554', '#ee4749','#00b19d']});
	$('.linechart').sparkline();
	$('.pageviews').sparkline('html', {type: 'bar', height: '30px', barColor: '#ff6264'} );
	$('.uniquevisitors').sparkline('html', {type: 'bar', height: '30px', barColor: '#00b19d'} );
	
	
	$(".monthly-sales").sparkline([1,2,3,5,6,7,2,3,3,4,3,5,7,2,4,3,5,4,5,6,3,2], {
		type: 'bar',
		barColor: '#485671',
		height: '80px',
		barWidth: 10,
		barSpacing: 2
	});	
	
	
	// JVector Maps
	var map = $("#map");
	
	map.vectorMap({
		map: 'europe_merc_en',
		zoomMin: '3',
		backgroundColor: '#383f47',
		focusOn: { x: 0.5, y: 0.8, scale: 3 }
	});		
	
		
	
	// Line Charts
	var line_chart_demo = $("#line-chart-demo");
	
	var line_chart = Morris.Line({
		element: 'line-chart-demo',
		data: [
			{ y: '2006', a: 100, b: 90 },
			{ y: '2007', a: 75,  b: 65 },
			{ y: '2008', a: 50,  b: 40 },
			{ y: '2009', a: 75,  b: 65 },
			{ y: '2010', a: 50,  b: 40 },
			{ y: '2011', a: 75,  b: 65 },
			{ y: '2012', a: 100, b: 90 }
		],
		xkey: 'y',
		ykeys: ['a', 'b'],
		labels: ['October 2013', 'November 2013'],
		redraw: true
	});
	
	line_chart_demo.parent().attr('style', '');
	
	
	// Donut Chart
	var donut_chart_demo = $("#donut-chart-demo");
	
	donut_chart_demo.parent().show();
	
	var donut_chart = Morris.Donut({
		element: 'donut-chart-demo',
		data: [
			{label: "Download Sales", value: getRandomInt(10,50)},
			{label: "In-Store Sales", value: getRandomInt(10,50)},
			{label: "Mail-Order Sales", value: getRandomInt(10,50)}
		],
		colors: ['#707f9b', '#455064', '#242d3c']
	});
	
	donut_chart_demo.parent().attr('style', '');
	
	
	// Area Chart
	var area_chart_demo = $("#area-chart-demo");
	
	area_chart_demo.parent().show();
	
	var area_chart = Morris.Area({
		element: 'area-chart-demo',
		data: [
			{ y: '2006', a: 100, b: 90 },
			{ y: '2007', a: 75,  b: 65 },
			{ y: '2008', a: 50,  b: 40 },
			{ y: '2009', a: 75,  b: 65 },
			{ y: '2010', a: 50,  b: 40 },
			{ y: '2011', a: 75,  b: 65 },
			{ y: '2012', a: 100, b: 90 }
		],
		xkey: 'y',
		ykeys: ['a', 'b'],
		labels: ['Series A', 'Series B'],
		lineColors: ['#303641', '#576277']
	});
	
	area_chart_demo.parent().attr('style', '');
	
	
	
	
	// Rickshaw
	var seriesData = [ [], [] ];
	
	var random = new Rickshaw.Fixtures.RandomData(50);
	
	for (var i = 0; i < 50; i++) 
	{
		random.addData(seriesData);
	}
	
	var graph = new Rickshaw.Graph( {
		element: document.getElementById("rickshaw-chart-demo"),
		height: 193,
		renderer: 'area',
		stroke: false,
		preserve: true,
		series: [{
				color: '#73c8ff',
				data: seriesData[0],
				name: 'Upload'
			}, {
				color: '#e0f2ff',
				data: seriesData[1],
				name: 'Download'
			}
		]
	} );
	
	graph.render();
	
	var hoverDetail = new Rickshaw.Graph.HoverDetail( {
		graph: graph,
		xFormatter: function(x) {
			return new Date(x * 1000).toString();
		}
	} );
	
	var legend = new Rickshaw.Graph.Legend( {
		graph: graph,
		element: document.getElementById('rickshaw-legend')
	} );
	
	var highlighter = new Rickshaw.Graph.Behavior.Series.Highlight( {
		graph: graph,
		legend: legend
	} );
	
	setInterval( function() {
		random.removeData(seriesData);
		random.addData(seriesData);
		graph.update();
	
	}, 500 );
	
});

<?php } ?>
function getRandomInt(min, max) 
{
	return Math.floor(Math.random() * (max - min + 1)) + min;
}
</script>