<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
<title><?php if(isset($page_title)){ echo $page_title; } else { echo PROJECT_TITLE;} ?></title>
<meta name="description" content="<?php if(isset($page_meta_desc)){ echo $page_meta_desc; } else { echo PROJECT_TITLE;} ?>">
<meta name="keywords" content="<?php if(isset($page_meta_key)){ echo $page_meta_key; } else { echo PROJECT_TITLE;} ?>">
<meta charset="utf-8" />
<meta http-equiv="x-ua-compatible" content="ie=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href='http://fonts.googleapis.com/css?family=Ubuntu:400,400italic,700,700italic,300,300italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Euphoria+Script' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="<?php echo FRONTEND_ASSETS;?>css/font-awesome/css/font-awesome.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo FRONTEND_ASSETS;?>css/tango/skin.css" />
<link rel="stylesheet" href="<?php echo FRONTEND_ASSETS;?>css/colorbox.css" />
<link rel="stylesheet" href="<?php echo FRONTEND_ASSETS;?>css/layerslider/layerslider.css" />
<link rel="stylesheet" href="<?php echo FRONTEND_ASSETS;?>css/bootstrap.css" />
<link rel="stylesheet" href="<?php echo FRONTEND_ASSETS;?>css/style.css" />
<link rel="stylesheet" href="<?php echo FRONTEND_ASSETS;?>css/responsive.css" />
<link rel="stylesheet" href="<?php echo FRONTEND_ASSETS;?>css/color-01.css"/>
<link rel="stylesheet" href="<?php echo FRONTEND_ASSETS;?>css/datepicker.css"/>
<!--[if IE]>
<link rel="stylesheet" href="./css/ie-style.css" />
<![endif]-->
</head>

<body>
<div id="header">
<!-- header start //-->
<div class="row" style="height:30px;background:#37C878">
<div class="container">
<div class="span7" style="margin-left:10px">
<div class="header-social-buttons">

            	<?php if(isset($websettings['facebook'])&&$websettings['facebook']!=""){?><a target="_blank" href="<?php echo $websettings['facebook'];?>"><i class="icon-facebook"></i></a><?php } ?>
                <?php if(isset($websettings['twitter'])&&$websettings['twitter']!=""){?> <a target="_blank" href="<?php echo $websettings['twitter'];?>"><i class="icon-twitter"></i></a><?php } ?>
                <?php if(isset($websettings['pinterest'])&&$websettings['pinterest']!=""){?> <a target="_blank" href="<?php echo $websettings['pinterest'];?>"><i class="icon-pinterest"></i></a><?php } ?>
                 <?php if(isset($websettings['linkedin'])&&$websettings['linkedin']!=""){?><a target="_blank" href="<?php echo $websettings['linkedin'];?>"><i class="icon-linkedin"></i></a><?php } ?>
                 <?php if(isset($websettings['google'])&&$websettings['google']!=""){?><a target="_blank" href="<?php echo $websettings['google'];?>"><i class="icon-google-plus"></i></a><?php } ?>
                 <?php if(isset($websettings['flickr'])&&$websettings['flickr']!=""){?><a  target="_blank" href="<?php echo $websettings['flickr'];?>"><i class="icon-flickr"></i></a><?php } ?>
                 <?php if(isset($websettings['youtube'])&&$websettings['youtube']!=""){?><a href="<?php echo $websettings['youtube'];?>"><i class="icon-youtube"></i></a><?php } ?>
                 
                 <?php if(isset($websettings['instagram'])&&$websettings['instagram']!=""){?><a target="_blank" href="<?php echo $websettings['instagram'];?>"><i class="icon-instagram"></i></a><?php } ?>
            </div>
            </div>
</div>
</div>

<div class="container">

	<div class="row">
		<div class="span5">
        	<!-- site logo	//-->
        	<a href="<?php echo base_url();?>"><div class="site-logo"><img alt="<?php echo PROJECT_TITLE;?>" src="<?php echo $this->imagethumb->image('./assets/frontend/images/logo/'.$websettings['logo'],0,40);?>"></div></a>
            <!-- site description //-->
            
        </div>
        <div class="span7">
        	<!-- header social buttons //-->
        	
            <!-- header contacts //-->
            
            <!-- site desktop menu start //-->
            <nav class="site-desktop-menu">
            	<ul>
                	<li><a class="<?php echo (isset($active)&&$active=="1") ? 'active' : '';?>" href="<?php echo base_url();?>" ><?php echo $nav['home'];?></a></li>
                    <li><a class="<?php echo (isset($active)&&$active=="2") ? 'active' : '';?>" href="<?php echo base_url('faq.html');?>" >FAQs</a></li>
                    <li><a class="<?php echo (isset($active)&&$active=="3") ? 'active' : '';?>" href="<?php echo base_url('blog');?>" >Articles</a></li>
                     <li><a class="<?php echo (isset($active)&&$active=="page2") ? 'active' : '';?>" href="<?php echo $nav['about_slug'];?>"><?php echo $nav['about'];?></a></li>
                    <?php 
			 $wpages = $this->SqlModel->getRecords('page_id,page_uri,page_name','pages','page_id','ASC',array('page_status'=>'Published','show_in_menu'=>'Yes','page_id >'=>4));
			 if(!empty($wpages))
			 {
				 foreach($wpages as $p)
				 {?>
                 <li ><a class="<?php echo (isset($active)&&$active=="page".$p['page_id']) ? 'active' : '';?>" href="<?php echo base_url($p['page_uri'].'.html');?>"><?php echo $p['page_name'];?></a></li>
                 
				<?php	 
				 }
				 
			 }
			 ?>
             <li><a class="<?php echo (isset($active)&&$active=="4") ? 'active' : '';?>" href="<?php echo base_url('contact-us.html');?>" >Contact Us</a></li>
              </ul>
</nav>
            <!-- site desktop menu end //-->
            <!-- site desktop menu start //-->
            <nav class="site-mobile-menu">
            	<i class="icon-reorder"></i>
            	<ul>
                	<li><a href="<?php echo base_url();?>" ><?php echo $nav['home'];?></a></li>
                    <li><a href="<?php echo base_url('faq.html');?>" >FAQs</a></li>
                    <li><a href="<?php echo base_url('blog');?>" >Blog</a></li>
                     <li ><a href="<?php echo $nav['about_slug'];?>"><?php echo $nav['about'];?></a></li>
                	<?php 
			 $wpages = $this->SqlModel->getRecords('page_id,page_uri,page_name','pages','page_id','ASC',array('page_status'=>'Published','show_in_menu'=>'Yes','page_id >'=>4));
			 if(!empty($wpages))
			 {
				 foreach($wpages as $p)
				 {?>
                 <li><a href="<?php echo base_url($p['page_uri'].'.html');?>"><?php echo $p['page_name'];?></a></li>
                 
				<?php	 
				 }
				 
			 }
			 ?>
                	<li><a href="<?php echo base_url('contact-us.html');?>" >Contact Us</a></li>
                	
                </ul>
</nav>
            <!-- site desktop menu end //-->
        </div>
    </div>
</div>

<div class="gray-line"></div>
</div>
<!-- header end //-->
