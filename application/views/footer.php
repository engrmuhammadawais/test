
<div class="bottom-line">
	<div class="container">
    	<div class="row">
        	<div class="span6">
            	<p>© 2014 All rights reserved.  Powered by <?php
	   if(strpos($_SERVER['HTTP_HOST'],"demooze")===false)
		{?>
         <a style="color:#333" href="#" target="_blank">Muhammad Awais</a>
      
		<?php
        }
		else{
		?>
		<a style="color:#333" href="#" target="_blank">Muhammad Awais</a>
       <?php }?></p>
            </div>
        	<div class="span6">
            	<p class="bottom-menu"><a style="color:#333" href="<?php echo $nav['terms_slug'];?>"><?php echo $nav['terms'];?></a> | <a style="color:#333" href="<?php echo $nav['privacy_slug'];?>"><?php echo $nav['privacy'];?></a></p>
            </div>
        </div>
    </div>
</div>
<!-- to the top start //-->
<div id="to-the-top"><i class="icon-angle-up"></i></div>
<!-- to the top end //-->

<?php if(isset($active)&&$active=="10"){ ?>
<div class="hidden">
	<script type="text/javascript">
		<!--//--><![CDATA[//><!--
			var images = new Array()
			function preload() {
				for (i = 0; i < preload.arguments.length; i++) {
					images[i] = new Image()
					images[i].src = preload.arguments[i]
				}
			}
			preload(
				"<?php echo FRONTEND_ASSETS;?>images/opt1_active.png",
				"<?php echo FRONTEND_ASSETS;?>images/opt2_active.png",
				"<?php echo FRONTEND_ASSETS;?>images/opt3_active.png",
				"<?php echo FRONTEND_ASSETS;?>images/opt4_active.png",
				"<?php echo FRONTEND_ASSETS;?>images/opt5_active.png",
				"<?php echo FRONTEND_ASSETS;?>images/opt1_hover.png",
				"<?php echo FRONTEND_ASSETS;?>images/opt2_hover.png",
				"<?php echo FRONTEND_ASSETS;?>images/opt3_hover.png",
				"<?php echo FRONTEND_ASSETS;?>images/opt4_hover.png",
				"<?php echo FRONTEND_ASSETS;?>images/opt5_hover.png"
			)
		//--><!]]>
	</script>
</div>
<?php } ?>
</body>
<!-- scripts start //-->
<script>
var base_url = "<?php echo base_url();?>";
var front_assets = "<?php echo base_url()."assets/frontend/";?>";

</script>
<script src="<?php echo FRONTEND_ASSETS;?>js/jquery.js"></script>
<script src="<?php echo FRONTEND_ASSETS;?>js/bootstrap-datepicker.js"></script>
<script src="<?php echo FRONTEND_ASSETS;?>js/modernizr.js"></script>
<script src="<?php echo FRONTEND_ASSETS;?>js/layerslider.kreaturamedia.jquery.js"></script>
<script src="<?php echo FRONTEND_ASSETS;?>js/layerslider.transitions.js"></script>
<script src="<?php echo FRONTEND_ASSETS;?>js/jquery-easing-1.3.js" type="text/javascript"></script>
<script src="<?php echo FRONTEND_ASSETS;?>js/jquery-transit-modified.js" type="text/javascript"></script>
<script src="<?php echo FRONTEND_ASSETS;?>js/jquery.jcarousel.min.js"></script>

<script src="<?php echo FRONTEND_ASSETS;?>js/jquery.colorbox.js"></script>
<script src="<?php echo FRONTEND_ASSETS;?>js/jquery.validate.min.js"></script>
<script src="<?php echo FRONTEND_ASSETS;?>js/moment.min.js"></script>

<script src="<?php echo FRONTEND_ASSETS;?>js/custom.js"></script>
<script>
<?php if(isset($active)&&$active=="10"){ ?>
jQuery(document).ready(function() {
	/*
	var m = moment('2/12/2014').format('dddd, D MMMM YYYY');
	alert(m);
	
	*/
	var nowTemp = new Date('<?php echo date('F d, Y 00:00:00');?>');
	
var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
now.setDate(now.getDate()+2);

	$('#dp1').datepicker({orientation: 'auto',
  onRender: function(date) {
	 
    return date.valueOf() <now.valueOf() ? 'disabled' : '';
	
  }
}).on('show',function(ev){
	
	})
	.on('changeDate', function(ev){
		var d = $("#dp1").val();
		var m = moment(d).format('dddd, D MMMM YYYY');
		$("#showformatteddate,#show_date").html(m);
		$("#dpicker_container").addClass('typegreenbgdp');
		$("#dp1").datepicker('hide');
});
});
</script>
<?php
}
if(isset($websettings['analytics'])&&$websettings['analytics']!=""){?>
<script>
	var _gaq=[['_setAccount','<?php echo $websettings['analytics'];?>'],['_trackPageview']];
	(function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
	g.src='//www.google-analytics.com/ga.js';
	s.parentNode.insertBefore(g,s)}(document,'script'));
</script>
<?php } ?>
<!-- scripts end //-->

</html>
