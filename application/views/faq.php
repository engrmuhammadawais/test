<div class="margtop"></div>
<div class="main-content">
	<div class="container">
    	<div class="row">
        	<div class="span12"><br/><br/>
        <h2>Frequently Asked Questions</h2>
                <!-- main items start //-->
                <div class="main-content-block">
                	
                    <p> </p>
                    
                    <div id="sort-faq">
                    	
                      
                    
                        <!-- toggle start //-->
                        <div class="magnis-toggle">
                            
                            <?php
							if(!empty($faq))
							{
								foreach($faq as $f)
								{?>
                            <section>
                                <header><i class="icon-question-sign faqicon"></i> <?php echo nl2br($f['faq_question']);?></header>
                                <p><?php echo nl2br($f['faq_answer']);?></p>
                            </section>
                            <?php } 
							}?>
                             
                        </div>
                        <!-- toggle end //-->
                    
                    </div>

                </div>
                <!-- main items end //-->
        
        	</div>
            
        </div>
    </div>
</div>


