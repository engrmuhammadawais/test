<?php

class SqlModel extends CI_Model {

	private $table;

    public function __construct(){
	     // Call the Model constructor
	   parent::__construct();

    }


	/****************************** Start General Functions ***********************/
	
	//insert function parameter @tablename , @columns array('column name'=> column value)
	function insertRecord($table , $colums)
	{
		if($this->db->insert($table , $colums))
		return $this->db->insert_id();
		else
		return false;
	}
	
	//update function 
	//parameter @tablename , @columns array('column name'=> column value), @conidition array('column name'=> column value)
	function updateRecord($table , $colums , $condition)
	{
		if($this->db->update($table , $colums , $condition))
		return true;
		else
		return false;
	}
	
	// delete function 
	//parameter @tablename , @conidition array('column name'=> column value)
	function deleteRecord($table , $condition)
	{
		if($this->db->delete($table , $condition))
		{
		//echo $this->db->last_query();
		return true;
		}
		else
		{
		return false;
		}
	}
	
	//Get Data by Run Query
	//parameter @sql = query , @flag is use for known single row or mulitiple by default use for multiple
	function runQuery($sql, $flag='')
	{
		$this->result 	= $this->db->query($sql);
		if($flag)
		return  $this->result->row_array();
		else
		return $this->result->result_array();
	}
	
	//Get Single record
	//parameter @tablename , @conidition array('column name'=> column value)
	public function getSingleRecord($table, $where)
	{
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where($where);	
		$count = $this->db->count_all_results();
		if($count=="1")
		{
		$query = $this->db->get_where($table, $where);
		$data=$query->row_array();
		return $data;
		}
		else{
		return null;	
		}
	}
	
	//Get Single Field Data
	//parameter  @columns array('column name'=> column value), @tablename , @conidition array('column name'=> column value)
	public function getSingleField($col, $table, $where)
	{
		$this->db->select($col);
		$this->db->from($table);
		$this->db->where($where);	
		$query = $this->db->get();
		$data = $query->row_array();
		if(isset($data[$col]))
		{
		return $data[$col];
		}
		else{
		return null;	
		}
	}
	//Get Reocrds
	//parameter 
	//@field *, @tablename , @sort column name , @order by desc or asc,
	//@conidition array('column name'=> column value) , @limit  , @start limit 
	public function getRecords($fields, $table, $sortby="", $order="", $where="", $limit="0", $start="")
	{
		$this->db->select($fields);
		$this->db->from($table);
		if(!empty($where))
		{
		$this->db->where($where);	
		}
		if($sortby!="" && $order!="")
		{
		$this->db->order_by($sortby,$order);
		}
		
		if($limit!=0)
		{
			$this->db->limit($limit, $start);
			
		}
		$query = $this->db->get();
		$data=$query->result_array();
		return $data;
		
	}
	
	//count Records
	//parameter 
	//@tablename, 
	//@conidition array('column name'=> column value)
	public function countRecords($table, $where=array())
	{
	$this->db->select('*');
	$this->db->from($table);
	if(!empty($where))
	{
	$this->db->where($where);	
	}
	$records = $this->db->count_all_results();
	return $records;	
	}
	
	//checkCookie
	//parameter 
	//@tablename, 
	//@conidition array('column name'=> column value)
	public function checkCookie()
	{
		if(isset($_COOKIE['dstacklog'])&&$_COOKIE['dstacklog']!="")
		{
			$id = $this->encrypt->decode($_COOKIE['dstacklog']);
			$user_data = $this->getUserDataById($id);
			if(!empty($user_data))
			{	
			$this->session->set_userdata('user_id',$user_data['id']);
			$this->session->set_userdata('fb_uid',$user_data['fb_uid']);
			$this->session->set_userdata('email',$user_data['email']);
			$this->session->set_userdata('full_name',$user_data['full_name']);
			$this->session->set_userdata('role', $user_data['user_role']);
			$this->session->set_userdata('auth', '1');
			redirect(base_url().'home','location');	
			}
			else{
			return;	
			}
		}
		else
		{
		return;	
		}
	}
	
	public function truncate($table="")
	{
		if($table=="")
		{
		return;
		}
		if($this->db->truncate($table))
		{
			return true;	
		}
		else{
			return false;
		}
	}
	
	//Define Page Title Constant
	public function setTitle()
	{
		$webTitle = $this->getSingleField('website_title', 'site_settings', array('id'=>1));
		define('PROJECT_TITLE',$webTitle );		
	}
	
	//For getting slider images
	public function getSlider()
	{
		$query = "SELECT slider_title,CONCAT('".FRONTEND_ASSETS."images/slider/',slider_large) as image FROM slider s WHERE s.`slider_status`='Enable' ORDER BY RAND()";
		$d = $this->runQuery($query);
		return $d;	
	}
	
	//For getting navigation
	public function getNav()
	{
		$n = $this->getRecords('page_name,page_uri,page_id','pages','page_id','ASC',array('page_id <='=>4));
		$nav = array(
			'home' => $n[0]['page_name'],
			'home_slug' => base_url().$n[0]['page_uri'].'.html',
			'about' => $n[1]['page_name'],
			'about_slug' => base_url().$n[1]['page_uri'].'.html',
			'privacy' => $n[2]['page_name'],
			'privacy_slug' => base_url().$n[2]['page_uri'].'.html',
			'terms' => $n[3]['page_name'],
			'terms_slug' => base_url().$n[3]['page_uri'].'.html',
		);
		return $nav;
	}	
	

}

?>