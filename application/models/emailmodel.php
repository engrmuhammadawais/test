<?php

class EmailModel extends CI_Model {
public function __construct(){
        parent::__construct();
	   
	   $this->load->library('PHPMailer/phpmailer');
  	   $this->emailHost = EMAIL_HOST;
	   $this->sendFrom = EMAIL_SENDER_NAME;
   	   $this->sendFromEmail = EMAIL_ADDRESS;
    }
//Send Mail, 
//@parameter @name , $to , @subject , $body, $attachment
public function sendEmail($name, $to, $subject, $body, $attach=""){
 		$mail             = new PHPMailer();
   		$mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
												   // 1 = errors and messages
												   // 2 = messages only
		$mail->Host       = $this->emailHost;      // sets GMAIL as the SMTP server
   		$mail->SetFrom($this->sendFromEmail, $this->sendFrom);
		$mail->AddReplyTo($this->sendFromEmail, $this->sendFrom);
		$mail->Subject    = $subject;
		$mail->MsgHTML($body);
		$mail->AddAddress($to, $name);

		if($attach!="")
		{
		$mail->AddAttachment($attach);      // attachment
		}

		if(!$mail->Send()) {
		  return "Error";
		} else {
		  return "Sent";
		}
}





}

?>