<?php

class ActivityModel extends CI_Model {
    private $table = 'activities';
    private $track = array();
    private $allowed = array( 'user_agent', 'ip', 'referer' );
    
    public function __construct() {
        parent::__construct();
    }

    /**
     * Public track function to set what miscellanous data to track
     * Warning! It will override track value!
     */
    public function track( $array ) {
        $this->addTrackData( array_intersect((array) $array, $this->allowed) );
        return $this;
    }
	//Add Activity
	//Parameter @User Id, $log Key, @log values = login, @extra null
    public function addActivity($user_id, $key, $value='', $extra=array()) {
        $user_id = (int) $user_id;

        if ( count($this->track) > 0 ) {
            $extra = array_merge( $this->track, $extra );
        }

        if ( count($extra) > 0 )
            $extra = json_encode( $extra );
        else
            $extra = '';

        $query = $this->db->query("INSERT INTO ".$this->table." (user_id, log_key, log_value, log_others, datetime) VALUES(?, ?, ?, ?, NOW())",
            array( $user_id, $key, $value, $extra )
        );

        if ( $query ) {
            return true;
        }

        return false;
    }
	
    private function addTrackData( $array ) {

        if ( in_array('user_agent', $array) )
            $this->track['user_agent'] = $this->input->user_agent();

        if ( in_array('ip', $array) )
            $this->track['ip'] = $this->input->ip_address();
        
        if ( in_array('referer', $array) )
            $this->track['referer'] = $this->input->server('HTTP_REFERER');
    }

}