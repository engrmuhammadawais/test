<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	public $websettings;
	 public function __construct(){

        // Call the Model constructor
	   parent::__construct();
	   $this->SqlModel->setTitle();
		$this->websettings = $this->SqlModel->getSingleRecord('site_settings',array('id'=>1));
		
    }
	public function index()
	{
		$pageData = $this->SqlModel->getSingleRecord('pages',array('page_id'=>1));
		
		$nav = $this->SqlModel->getNav();
		
		$data = array(
		'websettings'=>$this->websettings,
		'page_title'	=> PROJECT_TITLE." | ".$pageData['page_name'],
		'page_meta_key'	=> $pageData['page_meta_key'],
		'page_meta_desc'	=> $pageData['page_meta_desc'],
		'active'=>1,
		'nav'=>$nav,
		'home' => $this->SqlModel->getSingleRecord('home_page',array('id'=>1))
		);
		$this->load->view('header',$data);
		$this->load->view('home');
		$this->load->view('footer');	
	}
	
	
	public function page($page_uri="")
	{
		
		$page_uri = str_replace(".html","",$page_uri);
		$pageData = $this->SqlModel->getSingleRecord('pages',array('page_uri'=>$page_uri));
		$preview = $this->input->get('preview');
		if(empty($pageData))
		{
			redirect(base_url());
		}	
		if($pageData['page_status']!="Published"&&$preview=="")
		{
			redirect(base_url());
		}
		$nav = $this->SqlModel->getNav();
		$data = array(
		'websettings'=>$this->websettings,
		'pageData' =>$pageData,
		'page_title'	=> PROJECT_TITLE." | ".$pageData['page_name'],
		'page_meta_key'	=> $pageData['page_meta_key'],
		'page_meta_desc'	=> $pageData['page_meta_desc'],
		'nav' =>$nav,
		'active'=>'page'.$pageData['page_id']
		);
		
		
		  $this->load->view('header',$data);
		  $this->load->view('webpage');
		  $this->load->view('footer');
	}
	
	public function faq()
	{
		$pageData = $this->SqlModel->getSingleRecord('pages',array('page_id'=>1));
		$faq = $this->SqlModel->getRecords('faq_question,faq_answer','faqs','faq_added','ASC',array('faq_status'=>'Enable'));
		$nav = $this->SqlModel->getNav();
		
		$data = array(
		'websettings'=>$this->websettings,
		'page_title'	=> PROJECT_TITLE." | Frequently Asked Questions",
		'page_meta_key'	=> '',
		'page_meta_desc'	=> '',
		'active'=>2,
		'nav'=>$nav,
		'faq' => $faq,
		'home' => $this->SqlModel->getSingleRecord('home_page',array('id'=>1))
		);
		$this->load->view('header',$data);
		$this->load->view('faq');
		$this->load->view('footer');	
	}
	
	public function booking()
	{
		$post_code = $this->input->get('post_code');
		$post_code = urldecode($post_code);
		if($this->SqlModel->countRecords('postcodes',array('post_code'=>$post_code))=="0")
		{
			redirect(base_url());
			exit;
		}
		
		$cities = $this->SqlModel->getRecords('City','cities','City','ASC');
		$nav = $this->SqlModel->getNav();
		$data = array(
		'websettings'=>$this->websettings,
		'nav' =>$nav,
		'page_title'	=> PROJECT_TITLE." | Booking",
		'page_meta_key'	=> '',
		'page_meta_desc'	=> '',
		'active'=>'10',
		'post_code'=>$post_code,
		'cities'=>$cities
		);
		
		
		  $this->load->view('header',$data);
		  $this->load->view('booking');
		  $this->load->view('footer');
	}
	
	
	public function contact()
	{
		$nav = $this->SqlModel->getNav();
		$data = array(
		'websettings'=>$this->websettings,
		'nav' =>$nav,
		'page_title'	=> PROJECT_TITLE." | Contact Us",
		'page_meta_key'	=> '',
		'page_meta_desc'	=> '',
		'active'=>4
		);
		
		
		  $this->load->view('header',$data);
		  $this->load->view('contact');
		  $this->load->view('footer');
	}
	
	public function sendemail(){
	
	
	$name = $this->websettings['website_title'];
	$to = $this->websettings['contact_form_email'];
	$subject = "[".PROJECT_TITLE."] Contact Us ".date('M-d-Y H:i a');
	$body = '<html>
	<head>
	<style>
	table{
	border: 1px solid #EAEAEA;
	margin-top:20px;
	border-collapse:collapse;	
	}
	table tr{
	border-bottom: 1px solid #EAEAEA;
	
	}
	
	table_span{
	clear:both;
	display:block;
	font-family: Arial, Helvetica, sans-serif;
	font-size:12px;
	color:#FF0000;
	margin-bottom:5px;
	}
	
	table td{
	padding:10px 5px;
	font-family: Arial, Helvetica, sans-serif;
	font-size:12px;
	color:#737373;
	}
	
	
	table h3{
	font-family: Arial, Helvetica, sans-serif;
	font-size: 18px;
	font-weight:normal;
	color:#666;
	margin:8px 0px;
	}
	</style>
	</head>
	<body>
	<table  width="100%">
       
       	 <tr>
        <td colspan="2"><h3>Contact INFORMATION</h3></td>
        </tr>
        <tr>
        <td><strong>Date/Time</strong></td><td>'.date('M d, Y h:i a').'</td>
        </tr>
        <tr>
        <td><strong>Name</strong></td><td>'.$this->input->post('name').'</td>
        </tr>
        
        
        <tr>
        <td><strong>Email</strong></td><td>'.$this->input->post('email').'</td>
        </tr>
      
        <tr>
        <td><strong>Message</strong></td><td>'.nl2br($this->input->post('message')).'</td>
        </tr>
		  <tr>
        <td><strong>IP Address</strong></td><td>'.$this->session->userdata('ip_address').'</td>
        </tr>
      
	    <tr>
        <td><strong>User Agent</strong></td><td>'.$this->session->userdata('user_agent').'</td>
        </tr>
      
       </table></body></html>';
	
	
	$config = array(
	'mailtype'=>'html',
	'useragent'=>$name
	);
	//$this->load->library('email',$config);
	//$this->email->from('no-reply@tidyupp.co.uk', $name);
	//$this->email->to($to);
	//$this->email->reply_to('no-reply@tidyupp.co.uk', $name);
	//$this->email->subject($subject);
	//$this->email->message($body);
	//$this->email->send();
	//$this->email->print_debugger();
	$this->load->model('EmailModel');
	@$this->EmailModel->sendEmail($name, $to, $subject, $body);
	$this->session->set_flashdata('status','success');
	@redirect(base_url('contact-us.html#bottom'),'location');
	}
	
}

