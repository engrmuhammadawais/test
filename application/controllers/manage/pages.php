<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends CI_Controller {

	public $tblName = 'pages';
	public $pKey = 'page_id';
	public $moduleName = "Web Pages";
	public $controller = "pages";
	 public function __construct(){

        // Call the Model constructor
	   parent::__construct();
	   $this->SqlModel->setTitle();
		if($this->session->userdata('admin_auth')!="1")
		{
		redirect(base_url().'manage/login','location');
		}
		$this->user_data = $this->SqlModel->getSingleRecord('admin_users' , array('id'=>$this->session->userdata('admin_id')));
		$this->load->helper('text');
		
    }
	
	//For listing the brands
	public function index($alert="",$sortby="page_name", $order="ASC", $pg_no="")
	{
		$data['page_title'] = PROJECT_TITLE." | ".$this->moduleName;
		$data['alert'] = $alert;
		$data['userdata'] = $this->user_data;
		$data['pagesActive'] = 1;
		$data['per_page'] = 10;
		//Pagination START
			$count_rows = $this->SqlModel->countRecords($this->tblName, array());
			$pconfig['base_url'] = base_url().'manage/'.$this->controller.'/index/page/'.$sortby."/".$order;
			$data['total_rows'] = $count_rows;
			$pconfig['total_rows'] =  $count_rows;
			$pconfig["uri_segment"] = 7;
			$pconfig['per_page'] = $data['per_page'];
			$pconfig['num_links'] = 1;
			$pconfig['prev_link'] = '<i class="entypo-left-open-mini"></i>';
			$pconfig['next_link'] = '<i class="entypo-right-open-mini"></i>';
			$pconfig['cur_tag_open'] = '<li  class="active"><a href="javascript:void(0)">';
			$pconfig['cur_tag_close'] = '</a></li>';
			$pconfig['full_tag_open'] = '<ul class="pagination pull-right">';
   			$pconfig['full_tag_close'] = '</ul>';
			$pconfig['num_tag_open'] = "<li>";
			$pconfig['num_tag_close']= "</li>";
			$pconfig['next_tag_open'] = "<li>";
			$pconfig['next_tag_close']= "</li>";
			$pconfig['prev_tag_open'] = "<li>";
			$pconfig['prev_tag_close']= "</li>";
			$pconfig['last_tag_open'] = "<li>";
			$pconfig['last_tag_close']= "</li>";
			$pconfig['first_tag_open'] = "<li>";
			$pconfig['first_tag_close']= "</li>";
			$page = ($this->uri->segment(7)) ? $this->uri->segment(7) : 0;
			if($pg_no!="")
			{
				$page = $pg_no;
			}
		
			$this->pagination->initialize($pconfig);
			$data['listing'] = $this->SqlModel->getRecords('*', $this->tblName, $sortby, $order, array(), $pconfig["per_page"], $page);
			$data['paginate'] = $this->pagination->create_links();	
		//Pagination END
			$data['sortby'] = $sortby;
			if($order=="ASC")
			{
				$order = "DESC";	
			}
			else if($order=="DESC")
			{
				$order = "ASC";	
			}
			$data['order'] = $order;
			$data['page_numb'] = $page;
		
		$this->load->view('admin/header',$data);
		$this->load->view('admin/navigation');
		$this->load->view('admin/webPages');
		$this->load->view('admin/footer');
	}
	
	//For adding/edting page
	public function control($alert="",$editID="",$editerror="")
	{
		$data['datePicker'] = 1;
		$data['pagesActive'] = 1;
		$data['alert'] = $alert;
		$data['editerror'] = $editerror;
		$type = ($editID=="") ? "Add" : "Edit";
		$data['page_title'] = PROJECT_TITLE." | ".$type." ".$this->moduleName;
		//CKEditor
		$this->load->library('ckeditor');
		$this->load->library('ckfinder');
		$this->ckeditor->basePath = base_url().'assets/ckeditor/';
		$this->ckeditor->config['removePlugins'] ='save, preview, newpage, forms';
		$this->ckeditor->config['height'] = '340px';            
			
		//Add Ckfinder to Ckeditor
		$this->ckfinder->SetupCKEditor($this->ckeditor,'../../../../assets/ckfinder/'); 
		
		//CKEdtior
		if($alert=="error" || $alert=="image_error")
		{
		$data['tbl_data'] = $this->session->userdata($this->controller.'_data');	
		}
		else if($alert=="edit")
		{
			if($editID=="")
			{
			redirect(base_url().'manage/'.$this->controller.'/control','location');	
			exit();	
			}
			$count = $this->SqlModel->countRecords($this->tblName, array($this->pKey=>$editID));
			if($count==0)
			{
			redirect(base_url().'manage/'.$this->controller,'location');
			}
			$data['tbl_data'] = $this->SqlModel->getSingleRecord($this->tblName, array($this->pKey=>$editID));
		}
		$data['userdata'] = $this->user_data;
		$this->load->view('admin/header',$data);
		$this->load->view('admin/navigation');
		$this->load->view('admin/addPage');
		$this->load->view('admin/footer');
	}
	
	
	
	
	//For add record 
	public function addRecord()
	{	
		if($this->input->post('page_name')=="")
		{
		redirect(base_url().'manage/'.$this->controller.'/index/error','location');	
		exit();
		}
		
		$data = array(
		'page_name' 	=> $this->input->post('page_name'),
		'page_uri'	=> $this->input->post('page_uri'),
		'page_caption'	=> $this->input->post('page_caption'),
		'page_text' 	=> $_REQUEST['page_text'],
		'page_meta_key' 	=> $this->input->post('page_meta_key'),
		'page_meta_desc' 	=> $this->input->post('page_meta_desc'),
		'page_extra_tags' 	=> $_REQUEST['page_extra_tags'],
		'page_status' 	=> $this->input->post('page_status'),
		'page_added' 	=> date('Y-m-d H:i:s'),
		'page_updated' 	=> date('Y-m-d H:i:s'),
		'page_year' 	=> date('Y'),
		'page_month' 	=> date('m'),
		'page_featured'	=> $this->input->post('page_featured'),
		'show_in_menu'	=> $this->input->post('show_in_menu'),
		'page_head'	=> $this->input->post('page_head'),
		'page_month_year' => date('F Y')
		);
		
		if($this->SqlModel->countRecords('pages',array('page_uri'=>$data['page_uri']))>0)
		{
			$data['page_uri'] = $this->input->post('page_uri').'-1';
		}
		
		if($this->SqlModel->countRecords('pages',array('page_uri'=>$data['page_uri']))>0)
		{
			$data['page_uri'] = uniqid().'-'.$this->input->post('page_uri');
		}
		
		if($this->input->post('pub_date')!="" && $this->input->post('pub_time')!="")
		{
			$pub_date = $this->input->post('pub_date'). " ".$this->input->post('pub_time');
			$pub_date = date('Y-m-d h:i:s',strtotime($pub_date));
			$data['page_pdate'] = $pub_date;	
		}
		
		if(isset($_FILES['uploadfile'])&&$_FILES['uploadfile']['tmp_name']!="")
		{
			$config['upload_path'] = './assets/frontend/images/pages/';
			$config['allowed_types'] = 'jpg|png|jpeg';
			$config['max_size']	= '102400';
			$config['max_width']  = '12000';
			$config['max_height']  = '12000';
			$config['remove_spaces'] = true;
			$this->load->library('upload', $config);
			$this->load->library('image_lib');
					
			if ( ! $this->upload->do_upload('uploadfile'))
			{
				$this->session->set_userdata($this->controller.'_data', $data);
				redirect(base_url().'manage/'.$this->controller.'/control/image_error','location');	
			}
			else{
					$filename_ephoto = $this->upload->data('uploadfile');
					$large = $this->imagethumb->image('./assets/frontend/images/pages/'.$filename_ephoto['file_name'],1920,0);
					$thumb = $this->imagethumb->image('./assets/frontend/images/pages/'.$filename_ephoto['file_name'],150,0);	
					$data['page_thumb_image'] = str_replace(FRONTEND_ASSETS."images/pages/","",$thumb);
					$data['page_image'] = str_replace(FRONTEND_ASSETS."images/pages/","",$large);
					unlink('./assets/frontend/images/pages/'.$filename_ephoto['file_name']);
			}
		}
		
		$q = $this->SqlModel->insertRecord($this->tblName, $data);
		$this->session->unset_userdata($this->controller.'_data');
		if($q!="")
		{
			redirect(base_url().'manage/'.$this->controller.'/index/success','location');		
		}
		else{
			redirect(base_url().'manage/'.$this->controller.'/index/error','location');		
		}	
	}
		
	//For edit record 
	public function editRecord($editID="")
	{
		
		if($editID=="")
		{
		redirect(base_url().'manage/'.$this->controller,'location');	
		exit();	
		}
		
		if($this->input->post('page_name')=="")
		{
		redirect(base_url().'manage/'.$this->controller.'/index/error','location');	
		exit();
		}
		$data = array(
		'page_name' 	=> $this->input->post('page_name'),
		'page_uri'	=> $this->input->post('page_uri'),
		'page_text' 	=> $_REQUEST['page_text'],
		'page_meta_key' 	=> $this->input->post('page_meta_key'),
		'page_meta_desc' 	=> $this->input->post('page_meta_desc'),
		'page_extra_tags' 	=> $_REQUEST['page_extra_tags'],
		'page_status' 	=> $this->input->post('page_status'),
		'page_updated' 	=> date('Y-m-d H:i:s'),
		'page_featured'	=> $this->input->post('page_featured'),
		'show_in_menu'	=> $this->input->post('show_in_menu'),
		'page_caption'	=> $this->input->post('page_caption'),
		'page_head'	=> $this->input->post('page_head'),
		);
		
		if($this->input->post('pub_date')!="" && $this->input->post('pub_time')!="")
		{
			$pub_date = $this->input->post('pub_date'). " ".$this->input->post('pub_time');
			$pub_date = date('Y-m-d h:i:s',strtotime($pub_date));
			$data['page_pdate'] = $pub_date;	
		}
		
		if($this->SqlModel->countRecords('pages',array('page_uri'=>$data['page_uri'],'page_id !='=>$editID))>0)
		{
			$data['page_uri'] = $this->input->post('page_uri').'-1';
		}
		
		if($this->SqlModel->countRecords('pages',array('page_uri'=>$data['page_uri'],'page_id !='=>$editID))>0)
		{
			$data['page_uri'] = uniqid().'-'.$this->input->post('page_uri');
		}
		if(isset($_FILES['uploadfile'])&&$_FILES['uploadfile']['tmp_name']!="")
		{
			$config['upload_path'] = './assets/frontend/images/pages/';
			$config['allowed_types'] = 'jpg|png|jpeg';
			$config['max_size']	= '102400';
			$config['max_width']  = '12000';
			$config['max_height']  = '12000';
			$config['remove_spaces'] = true;
			$this->load->library('upload', $config);
			$this->load->library('image_lib');
					
			if ( ! $this->upload->do_upload('uploadfile'))
			{
				$this->session->set_userdata($this->controller.'_data', $data);
				redirect(base_url().'manage/'.$this->controller.'/control/image_error','location');	
			}
			else{
					$filename_ephoto = $this->upload->data('uploadfile');
					$large = $this->imagethumb->image('./assets/frontend/images/pages/'.$filename_ephoto['file_name'],1920,0);
					$thumb = $this->imagethumb->image('./assets/frontend/images/pages/'.$filename_ephoto['file_name'],150,0);	
					$data['page_thumb_image'] = str_replace(FRONTEND_ASSETS."images/pages/","",$thumb);
					$data['page_image'] = str_replace(FRONTEND_ASSETS."images/pages/","",$large);
					unlink('./assets/frontend/images/pages/'.$filename_ephoto['file_name']);
			}
		}
		
		
		$q = $this->SqlModel->updateRecord($this->tblName, $data, array($this->pKey=>$editID));
		if($q==true)
		{
		redirect(base_url().'manage/'.$this->controller.'/index/editsuccess','location');		
		}
		else{
		redirect(base_url().'manage/'.$this->controller.'/index/error','location');		
		}	
	}
	
	//Delete Page
	public function delete($deleteID="")
	{
		$q = $this->SqlModel->deleteRecord($this->tblName , array($this->pKey=>$deleteID));
		if($q==true)
		{
		redirect(base_url().'manage/'.$this->controller.'/index/deletesuccess','location');		
		}
		else{
		redirect(base_url().'manage/'.$this->controller.'/index/deleteerror','location');		
		}
		
		
	}
	
	//For delete Pages
	public function deleteall()
	{
		$ids = $this->input->post('records');
		if(!empty($ids))
		{
			foreach($ids as $id)
			{
			$this->SqlModel->deleteRecord($this->tblName,  array($this->pKey=>$id));	
			}
		}
		redirect(base_url().'manage/'.$this->controller.'/index/deletesuccess','location');		
		
	}
	
	//For remove image of Pages
	public function removeimage($id="")
	{
		$q = $this->SqlModel->updateRecord($this->tblName , array('page_image'=>'','page_thumb_image'=>''), array($this->pKey=>$id));
	}

	
}

