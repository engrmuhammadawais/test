<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blogs extends CI_Controller {

	public $tblName = 'blogs';
	public $pKey = 'blog_id';
	public $moduleName = "Blogs";
	public $controller = "blogs";
	 public function __construct(){

        // Call the Model constructor
	   parent::__construct();
	   $this->SqlModel->setTitle();
		if($this->session->userdata('admin_auth')!="1")
		{
		redirect(base_url().'manage/login','location');
		}
		$this->user_data = $this->SqlModel->getSingleRecord('admin_users' , array('id'=>$this->session->userdata('admin_id')));
		$this->load->helper('text');
		
    }
	
	//For listing the Blog
	public function index($alert="",$sortby="blog_name", $order="ASC", $pg_no="",$cat_id="")
	{
		if($cat_id != ""){
			$where = array('cat_id'=>$cat_id);
		}else{ 
			$where = array();
		}
		$data['page_title'] = PROJECT_TITLE." | ".$this->moduleName;
		$data['alert'] = $alert;
		$data['userdata'] = $this->user_data;
		$data['blogsActive'] = 1;
		$data['blogsMainActive'] = 1;
		$data['per_page'] = 10;
		//Pagination START
		$count_rows =0;
			if($cat_id !=""){
				$cnt_sql = "SELECT COUNT(*) AS `numrows` FROM (`blogs`) WHERE blog_id IN (SELECT bc_blog_id FROM `blog_assigned_cat` WHERE bc_cat_id = $cat_id)";
				$cnt_sql_result = $this->SqlModel->runQuery($cnt_sql);
				$count_rows =$cnt_sql_result[0]['numrows'] ;
			}else{
				$count_rows = $this->SqlModel->countRecords($this->tblName, $where);
			}
			$pconfig['base_url'] = base_url().'manage/'.$this->controller.'/index/page/'.$sortby."/".$order;
			$data['total_rows'] = $count_rows;
			$pconfig['total_rows'] =  $count_rows;
			$pconfig["uri_segment"] = 7;
			$pconfig['per_page'] = $data['per_page'];
			$pconfig['num_links'] = 1;
			$pconfig['prev_link'] = '<i class="entypo-left-open-mini"></i>';
			$pconfig['next_link'] = '<i class="entypo-right-open-mini"></i>';
			$pconfig['cur_tag_open'] = '<li  class="active"><a href="javascript:void(0)">';
			$pconfig['cur_tag_close'] = '</a></li>';
			$pconfig['full_tag_open'] = '<ul class="pagination pull-right">';
   			$pconfig['full_tag_close'] = '</ul>';
			$pconfig['num_tag_open'] = "<li>";
			$pconfig['num_tag_close']= "</li>";
			$pconfig['next_tag_open'] = "<li>";
			$pconfig['next_tag_close']= "</li>";
			$pconfig['prev_tag_open'] = "<li>";
			$pconfig['prev_tag_close']= "</li>";
			$pconfig['last_tag_open'] = "<li>";
			$pconfig['last_tag_close']= "</li>";
			$pconfig['first_tag_open'] = "<li>";
			$pconfig['first_tag_close']= "</li>";
			$page = ($this->uri->segment(7)) ? $this->uri->segment(7) : 0;
			if($pg_no!="")
			{
				$page = $pg_no;
			}
			
		
			$this->pagination->initialize($pconfig);
			
			
			if($cat_id !=""){
				$sql = "SELECT * FROM (`blogs`) WHERE blog_id IN (SELECT bc_blog_id FROM `blog_assigned_cat` WHERE bc_cat_id = $cat_id)";
				$listing_data = $this->SqlModel->runQuery($sql);
				
			}else{
				$listing_data = $this->SqlModel->getRecords('*', $this->tblName, $sortby, $order, $where, $pconfig["per_page"], $page);
			}
			
			$data['listing'] = $listing_data;
			$data['paginate'] = $this->pagination->create_links();	
		//Pagination END
			$data['sortby'] = $sortby;
			if($order=="ASC")
			{
				$order = "DESC";	
			}
			else if($order=="DESC")
			{
				$order = "ASC";	
			}
			$data['order'] = $order;
			$data['blog_numb'] = $page;
			$data['cat_id'] = $cat_id;
			//get records
			$data['blog_categories'] = $this->SqlModel->getRecords('*', "blog_categories", "cat_id", "DESC", array());
		//Load Views
		$this->load->view('admin/header',$data);
		$this->load->view('admin/navigation');
		$this->load->view('admin/blogs');
		$this->load->view('admin/footer');
	}
	
	//For adding/edting blog
	public function control($alert="",$editID="",$editerror="")
	{
		$data['datePicker'] = 1;
		$data['blogsActive'] = 1;
		$data['blogsMainActive'] = 1;
		$data['alert'] = $alert;
		$data['editerror'] = $editerror;
		$type = ($editID=="") ? "Add" : "Edit";
		$data['page_title'] = PROJECT_TITLE." | ".$type." ".$this->moduleName;
		//CKEditor
		$this->load->library('ckeditor');
		$this->load->library('ckfinder');
		$this->ckeditor->basePath = base_url().'assets/ckeditor/';
		$this->ckeditor->config['removePlugins'] ='save, preview, newpage, forms,about';
		
		
		$this->ckeditor->config['height'] = '340px';
		//Add Ckfinder to Ckeditor
		$this->ckfinder->SetupCKEditor($this->ckeditor,'../../../../assets/ckfinder/'); 
		
		//CKEdtior
		//getting tags
		$data['tags'] = $this->SqlModel->getRecords('tag_id,tag_name', 'blog_tags', 'tag_name', 'ASC', array('tag_status'=>'Enable'));
		//getting tags
		//getting categories
		$data['cats'] = $this->SqlModel->getRecords('cat_id,cat_name', 'blog_categories', 'cat_name', 'ASC', array('cat_status'=>'Enable'));
		//getting categories
		
		if($alert=="error" || $alert=="image_error")
		{
		$data['tbl_data'] = $this->session->userdata($this->controller.'_data');	
		}
		else if($alert=="edit")
		{
			if($editID=="")
			{
			redirect(base_url().'manage/'.$this->controller.'/control','location');	
			exit();	
			}
			$count = $this->SqlModel->countRecords($this->tblName, array($this->pKey=>$editID));
			if($count==0)
			{
			redirect(base_url().'manage/'.$this->controller,'location');
			}
			$data['tbl_data'] = $this->SqlModel->getSingleRecord($this->tblName, array($this->pKey=>$editID));
			$bt = $this->SqlModel->runQuery("SELECT GROUP_CONCAT(bt_tag_id) AS csv FROM blog_assigned_tags WHERE bt_blog_id=".$editID." GROUP BY bt_blog_id",1);
			$data['tbl_data']['blog_tags'] =  (isset($bt['csv'])) ?  $bt['csv'] : '';
			$bc = $this->SqlModel->runQuery("SELECT GROUP_CONCAT(bc_cat_id) AS csv FROM blog_assigned_cat WHERE bc_blog_id=".$editID." GROUP BY bc_blog_id",1);
			$data['tbl_data']['blog_cats'] =  (isset($bc['csv'])) ?  $bc['csv'] : '';
		}
		$data['userdata'] = $this->user_data;
		$this->load->view('admin/header',$data);
		$this->load->view('admin/navigation');
		$this->load->view('admin/addBlog');
		$this->load->view('admin/footer');
	}
	
	
	
	
	//For add record
	public function addRecord()
	{	
		if($this->input->post('blog_name')=="")
		{
		redirect(base_url().'manage/'.$this->controller.'/index/error','location');	
		exit();
		}
		
		$data = array(
		'blog_name' 	=> $this->input->post('blog_name'),
		'blog_author' 	=> $this->input->post('blog_author'),
		'blog_uri'	=> $this->input->post('blog_uri'),
		'blog_text' 	=> $_REQUEST['blog_text'],
		'blog_meta_key' 	=> $this->input->post('blog_meta_key'),
		'blog_meta_desc' 	=> $this->input->post('blog_meta_desc'),
		'blog_extra_tags' 	=> $_REQUEST['blog_extra_tags'],
		'blog_status' 	=> $this->input->post('blog_status'),
		'blog_comment_status' 	=> $this->input->post('blog_commnet_status'),
		'blog_added' 	=> date('Y-m-d H:i:s'),
		'blog_updated' 	=> date('Y-m-d H:i:s'),
		'blog_year' 	=> date('Y'),
		'blog_month' 	=> date('m'),
		'blog_featured'	=> $this->input->post('blog_featured'),
		'blog_month_year' => date('F Y'),
		);
		
		if($this->SqlModel->countRecords('blogs',array('blog_uri'=>$data['blog_uri']))>0)
		{
			$data['blog_uri'] = $this->input->post('blog_uri').'-1';
		}
		
		if($this->SqlModel->countRecords('blogs',array('blog_uri'=>$data['blog_uri']))>0)
		{
			$data['blog_uri'] = uniqid().'-'.$this->input->post('blog_uri');
		}
		
		
		if(isset($_FILES['uploadfile']))
		{
			$config['upload_path'] = './assets/frontend/images/blog/';
			$config['allowed_types'] = 'jpg|png|jpeg';
			$config['max_size']	= '102400';
			$config['max_width']  = '12000';
			$config['max_height']  = '12000';
			$config['remove_spaces'] = true;
			$this->load->library('upload', $config);
			$this->load->library('image_lib');
					
			if ( ! $this->upload->do_upload('uploadfile'))
			{
					
			}
			else{
					$filename_ephoto = $this->upload->data('uploadfile');
					$thumb = $this->imagethumb->image('./assets/frontend/images/blog/'.$filename_ephoto['file_name'],180,0);
					$large = $this->imagethumb->image('./assets/frontend/images/blog/'.$filename_ephoto['file_name'],800,0);		
					$data['blog_thumb'] = str_replace(FRONTEND_ASSETS."images/blog/","",$thumb);
					$data['blog_image'] = str_replace(FRONTEND_ASSETS."images/blog/","",$large);
					
					@unlink('./assets/frontend/images/blog/'.$filename_ephoto['file_name']);
					
			}
		}
		$q = $this->SqlModel->insertRecord($this->tblName, $data);
		$this->session->unset_userdata($this->controller.'_data');
		if($q!="")
		{
			$blog_cats = $this->input->post('blog_category');
			if(!empty($blog_cats))
			{
				foreach($blog_cats as $bc)
				{
					$this->SqlModel->insertRecord('blog_assigned_cat',array('bc_blog_id'=>$q,'bc_cat_id'=>$bc));	
				}
			}
			$blog_tags = $this->input->post('blog_tags');
			if(!empty($blog_tags))
			{
				foreach($blog_tags as $bt)
				{
					$this->SqlModel->insertRecord('blog_assigned_tags',array('bt_blog_id'=>$q,'bt_tag_id'=>$bt));	
				}
			}
			redirect(base_url().'manage/'.$this->controller.'/index/success','location');		
		}
		else{
			redirect(base_url().'manage/'.$this->controller.'/index/error','location');		
		}	
	}
		
	//For update record 
	public function editRecord($editID="")
	{
		
		if($editID=="")
		{
		redirect(base_url().'manage/'.$this->controller,'location');	
		exit();	
		}
		
		if($this->input->post('blog_name')=="")
		{
		redirect(base_url().'manage/'.$this->controller.'/index/error','location');	
		exit();
		}
		$data = array(
		'blog_name' 	=> $this->input->post('blog_name'),
		'blog_author' 	=> $this->input->post('blog_author'),
		'blog_uri'	=> $this->input->post('blog_uri'),
		'blog_text' 	=> $_REQUEST['blog_text'],
		'blog_meta_key' 	=> $this->input->post('blog_meta_key'),
		'blog_meta_desc' 	=> $this->input->post('blog_meta_desc'),
		'blog_extra_tags' 	=> $_REQUEST['blog_extra_tags'],
		'blog_status' 	=> $this->input->post('blog_status'),
		'blog_comment_status' 	=> $this->input->post('blog_commnet_status'),
		'blog_updated' 	=> date('Y-m-d H:i:s'),
		'blog_featured'	=> $this->input->post('blog_featured'),
		);
		
		if($this->SqlModel->countRecords('blogs',array('blog_uri'=>$data['blog_uri'],'blog_id !='=>$editID))>0)
		{
			$data['blog_uri'] = $this->input->post('blog_uri').'-1';
		}
		
		if($this->SqlModel->countRecords('blogs',array('blog_uri'=>$data['blog_uri'],'blog_id !='=>$editID))>0)
		{
			$data['blog_uri'] = uniqid().'-'.$this->input->post('blog_uri');
		}
		
		if($_FILES['uploadfile']['tmp_name']!="")
		{
			$config['upload_path'] = './assets/frontend/images/blog/';
			$config['allowed_types'] = 'jpg|png|jpeg';
			$config['max_size']	= '102400';
			$config['max_width']  = '12000';
			$config['max_height']  = '12000';
			$config['remove_spaces'] = true;
			$this->load->library('upload', $config);
			$this->load->library('image_lib');
					
			if ( ! $this->upload->do_upload('uploadfile'))
			{
					
			}
			else{
					$filename_ephoto = $this->upload->data('uploadfile');
					$thumb = $this->imagethumb->image('./assets/frontend/images/blog/'.$filename_ephoto['file_name'],180,0);
					$large = $this->imagethumb->image('./assets/frontend/images/blog/'.$filename_ephoto['file_name'],800,0);		
					$data['blog_thumb'] = str_replace(FRONTEND_ASSETS."images/blog/","",$thumb);
					$data['blog_image'] = str_replace(FRONTEND_ASSETS."images/blog/","",$large);
					
					@unlink('./assets/frontend/images/blog/'.$filename_ephoto['file_name']);
					
			}
		}
		$q = $this->SqlModel->updateRecord($this->tblName, $data, array($this->pKey=>$editID));
		if($q==true)
		{
			$this->SqlModel->deleteRecord('blog_assigned_cat',array('bc_blog_id'=>$editID));
			$this->SqlModel->deleteRecord('blog_assigned_tags',array('bt_blog_id'=>$editID));
			
			$blog_cats = $this->input->post('blog_category');
			if(!empty($blog_cats))
			{
				foreach($blog_cats as $bc)
				{
					$this->SqlModel->insertRecord('blog_assigned_cat',array('bc_blog_id'=>$editID,'bc_cat_id'=>$bc));	
				}
			}
			$blog_tags = $this->input->post('blog_tags');
			if(!empty($blog_tags))
			{
				foreach($blog_tags as $bt)
				{
					$this->SqlModel->insertRecord('blog_assigned_tags',array('bt_blog_id'=>$editID,'bt_tag_id'=>$bt));	
				}
			}
		redirect(base_url().'manage/'.$this->controller.'/index/editsuccess','location');		
		}
		else{
		redirect(base_url().'manage/'.$this->controller.'/index/error','location');		
		}	
	}
	//Delete blog
	public function delete($deleteID="")
	{
		$q = $this->SqlModel->deleteRecord($this->tblName , array($this->pKey=>$deleteID));
		if($q==true)
		{
		redirect(base_url().'manage/'.$this->controller.'/index/deletesuccess','location');		
		}
		else{
		redirect(base_url().'manage/'.$this->controller.'/index/deleteerror','location');		
		}
		
		
	}
	
	//For delete selected / all blog
	public function deleteall()
	{
		$ids = $this->input->post('records');
		if(!empty($ids))
		{
			foreach($ids as $id)
			{
			$this->SqlModel->deleteRecord($this->tblName,  array($this->pKey=>$id));	
			}
		}
		redirect(base_url().'manage/'.$this->controller.'/index/deletesuccess','location');		
		
	}
	//For delete all comments of blog
	public function deleteallomment($blog_id)
	{		
		if(!empty($blog_id))
		{
			$this->SqlModel->deleteRecord('blog_comments',  array('blog_id'=>$blog_id));
		}else{
			redirect(base_url().'manage/'.$this->controller.'/','location');		
		}
		redirect(base_url().'manage/'.$this->controller.'/comments/'.$blog_id.'/deletesuccess','location');		
		
	}
	//Comment listing of blog
	public function comments($blog_id,$alert = ''){
		//$blog_uri = str_replace(".html","",$blog_id);
		$blogData = $this->SqlModel->getSingleRecord('blogs',array('blog_id'=>$blog_id,'blog_status'=>'Published'));
		$comments = $this->SqlModel->getRecords('*', 'blog_comments', 'id', 'ASC', array('status != '=>'Deleted'));
		$data['page_title'] = PROJECT_TITLE." | ".$this->moduleName;
		$data['alert'] = $alert;
		$data['userdata'] = $this->user_data;
		$data['blogsActive'] = 1;
		$data['blogsMainActive'] = 1;
		$data['per_page'] = 10;
		$data['userdata'] = $this->user_data;
		$data['blog_id'] = $blog_id;
		
		$data['listing'] = $comments;		
		$this->load->view('admin/header',$data);
		$this->load->view('admin/navigation');
		$this->load->view('admin/blogsComents');
		$this->load->view('admin/footer');
	}
	
	//Delete Particular comment	
	public function deleteComment($deleteID="")
	{
		$blogData = $this->SqlModel->getSingleRecord('blog_comments',array('id'=>$deleteID));
		$q = $this->SqlModel->deleteRecord("blog_comments" , array('id'=>$deleteID));
		if($q==true)
		{
		redirect(base_url().'manage/'.$this->controller.'/comments/'.$blogData['blog_id'].'/deletesuccess','location');		
		}
		else{
		redirect(base_url().'manage/'.$this->controller.'/comments/'.$blogData['blog_id'].'/deleteerror','location');		
		}		
	}
	
	//Comment status update published to unpublished / unpublished to published
	public function postComment($commentID="")
	{
		$blogData = $this->SqlModel->getSingleRecord('blog_comments',array('id'=>$commentID));
		$status = ($blogData == 'Published') ? 'Unpublished':'Published';
		$rstatus = ($blogData == 'Published') ? 'unpostsuccess':'postsuccess';
		$q = $this->SqlModel->updateRecord('blog_comments', array('status'=>$status), array('id'=>$commentID));
		if($q==true)
		{
		redirect(base_url().'manage/'.$this->controller.'/comments/'.$blogData['blog_id'].'/'.$rstatus,'location');		
		}
		else{
		redirect(base_url().'manage/'.$this->controller.'/comments/'.$blogData['blog_id'].'/deleteerror','location');		
		}		
	}
	
	

	
}

