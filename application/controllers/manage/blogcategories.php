<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blogcategories extends CI_Controller {

	public $tblName = 'blog_categories';
	public $pKey = 'cat_id';
	public $moduleName = "Blog Categories";
	public $controller = "blogcategories";
	 public function __construct(){

        // Call the Model constructor
	   parent::__construct();
	   $this->SqlModel->setTitle();
		if($this->session->userdata('admin_auth')!="1")
		{
		redirect(base_url().'manage/login','location');
		}
		$this->user_data = $this->SqlModel->getSingleRecord('admin_users' , array('id'=>$this->session->userdata('admin_id')));
		$this->load->helper('text');
		
    }
	
	//For listing the blog categories
	public function index($alert="",$sortby="cat_name", $order="ASC", $pg_no="")
	{
		$data['page_title'] = PROJECT_TITLE." | ".$this->moduleName;
		$data['alert'] = $alert;
		$data['userdata'] = $this->user_data;
		$data['blogsActive'] = 1;
$data['blogsCatActive'] = 1;
		$data['per_page'] = 10;
		//Pagination START
			$count_rows = $this->SqlModel->countRecords($this->tblName, array());
			$pconfig['base_url'] = base_url().'manage/'.$this->controller.'/index/page/'.$sortby."/".$order;
			$data['total_rows'] = $count_rows;
			$pconfig['total_rows'] =  $count_rows;
			$pconfig["uri_segment"] = 7;
			$pconfig['per_page'] = $data['per_page'];
			$pconfig['num_links'] = 1;
			$pconfig['prev_link'] = '<i class="entypo-left-open-mini"></i>';
			$pconfig['next_link'] = '<i class="entypo-right-open-mini"></i>';
			$pconfig['cur_tag_open'] = '<li  class="active"><a href="javascript:void(0)">';
			$pconfig['cur_tag_close'] = '</a></li>';
			$pconfig['full_tag_open'] = '<ul class="pagination pull-right">';
   			$pconfig['full_tag_close'] = '</ul>';
			$pconfig['num_tag_open'] = "<li>";
			$pconfig['num_tag_close']= "</li>";
			$pconfig['next_tag_open'] = "<li>";
			$pconfig['next_tag_close']= "</li>";
			$pconfig['prev_tag_open'] = "<li>";
			$pconfig['prev_tag_close']= "</li>";
			$pconfig['last_tag_open'] = "<li>";
			$pconfig['last_tag_close']= "</li>";
			$pconfig['first_tag_open'] = "<li>";
			$pconfig['first_tag_close']= "</li>";
			$page = ($this->uri->segment(7)) ? $this->uri->segment(7) : 0;
			if($pg_no!="")
			{
				$page = $pg_no;
			}
		
			$this->pagination->initialize($pconfig);
			$data['listing'] = $this->SqlModel->getRecords('*', $this->tblName, $sortby, $order, array(), $pconfig["per_page"], $page);
			$data['paginate'] = $this->pagination->create_links();	
		//Pagination END
			$data['sortby'] = $sortby;
			if($order=="ASC")
			{
				$order = "DESC";	
			}
			else if($order=="DESC")
			{
				$order = "ASC";	
			}
			$data['order'] = $order;
			$data['page_numb'] = $page;
		
		$this->load->view('admin/header',$data);
		$this->load->view('admin/navigation');
		$this->load->view('admin/blogCategories');
		$this->load->view('admin/footer');
	}
	
	//For adding/edting blog categories
	public function control($alert="",$editID="",$editerror="")
	{
		
		$data['blogsActive'] = 1;
$data['blogsCatActive'] = 1;
		$data['alert'] = $alert;
		$data['editerror'] = $editerror;
		$type = ($editID=="") ? "Add" : "Edit";
		$data['page_title'] = PROJECT_TITLE." | ".$type." ".$this->moduleName;
		if($alert=="error" || $alert=="image_error")
		{
		$data['tbl_data'] = $this->session->userdata($this->controller.'_data');	
		}
		else if($alert=="edit")
		{
			if($editID=="")
			{
			redirect(base_url().'manage/'.$this->controller.'/control','location');	
			exit();	
			}
			$count = $this->SqlModel->countRecords($this->tblName, array($this->pKey=>$editID));
			if($count==0)
			{
			redirect(base_url().'manage/'.$this->controller,'location');
			}
			
			$data['tbl_data'] = $this->SqlModel->getSingleRecord($this->tblName, array($this->pKey=>$editID));
		}
		$data['userdata'] = $this->user_data;
		$this->load->view('admin/header',$data);
		$this->load->view('admin/navigation');
		$this->load->view('admin/addBlogCat');
		$this->load->view('admin/footer');
	}
	
	
	
	
	//For add blog categories record
	public function addRecord()
	{	
		if($this->input->post('cat_name')=="")
		{
		redirect(base_url().'manage/'.$this->controller.'/index/error','location');	
		exit();
		}
		
		$data = array(
		'cat_name' 	=> $this->input->post('cat_name'),
		'cat_status'	=> $this->input->post('cat_status'),
		'cat_added' 	=> date('Y-m-d H:i:s'),
		'cat_updated' 	=> date('Y-m-d H:i:s')
		);
		
		$config = array(
		'field' => 'cat_uri',
		'title' => 'cat_name',
		'table' => 'blog_categories',
		'id' => 'cat_id',
		);
		$this->load->library('slug', $config);
		$udata = array('cat_name' => $data['cat_name']);
		$data['cat_uri'] = $this->slug->create_uri($udata);
		$count = $this->SqlModel->countRecords($this->tblName, array('cat_name'=> $this->input->post('cat_name')));
		if($count>0)
		{
		$this->session->set_userdata($this->controller.'_data', $data);
		redirect(base_url().'manage/'.$this->controller.'/control/error','location');	
		exit();	
		}
		$q = $this->SqlModel->insertRecord($this->tblName, $data);
		$this->session->unset_userdata($this->controller.'_data');
		if($q!="")
		{
			redirect(base_url().'manage/'.$this->controller.'/index/success','location');		
		}
		else{
			redirect(base_url().'manage/'.$this->controller.'/index/error','location');		
		}	
	}
		
	//For update Blog Categories
	public function editRecord($editID="")
	{
		if($editID=="")
		{
		redirect(base_url().'manage/'.$this->controller,'location');	
		exit();	
		}
		
		if($this->input->post('cat_name')=="")
		{
		redirect(base_url().'manage/'.$this->controller.'/index/error','location');	
		exit();
		}
		$data = array(
		'cat_name' 	=> $this->input->post('cat_name'),
		'cat_status'	=> $this->input->post('cat_status'),
		'cat_updated' 	=> date('Y-m-d H:i:s')
		);
		
		$config = array(
		'field' => 'cat_uri',
		'title' => 'cat_name',
		'table' => 'blog_categories',
		'id' => 'cat_id',
		);
		$this->load->library('slug', $config);
		$udata = array('cat_name' => $data['cat_name'],'cat_id'=>$editID);
		$data['cat_uri'] = $this->slug->create_uri($udata);
		$count = $this->SqlModel->countRecords($this->tblName, array('cat_name'=> $this->input->post('cat_name'),'cat_id !='=>$editID));
		if($count>0)
		{
		$this->session->set_userdata($this->controller.'_data', $data);
		redirect(base_url().'manage/'.$this->controller.'/control/edit/'.$editID.'/editerror','location');	
		exit();	
		}
		
		
		
		$q = $this->SqlModel->updateRecord($this->tblName, $data, array($this->pKey=>$editID));
		if($q==true)
		{
		redirect(base_url().'manage/'.$this->controller.'/index/editsuccess','location');		
		}
		else{
		redirect(base_url().'manage/'.$this->controller.'/index/error','location');		
		}	
	}
	
	//For delete BLOG Categories
	public function delete($deleteID="")
	{
		$q = $this->SqlModel->deleteRecord($this->tblName , array($this->pKey=>$deleteID));
		if($q==true)
		{
		redirect(base_url().'manage/'.$this->controller.'/index/deletesuccess','location');		
		}
		else{
		redirect(base_url().'manage/'.$this->controller.'/index/deleteerror','location');		
		}
		
		
	}
	
	//For delete selected blog categories
	public function deleteall()
	{
		$ids = $this->input->post('records');
		if(!empty($ids))
		{
			foreach($ids as $id)
			{
			$this->SqlModel->deleteRecord($this->tblName,  array($this->pKey=>$id));	
			}
		}
		redirect(base_url().'manage/'.$this->controller.'/index/deletesuccess','location');		
		
	}
	// add blog category via ajax
	public function addRecordAJAX()
	{	
		if($this->input->post('cat_name')=="")
		{
		echo json_encode(array('status'=>'false','message'=>'Category name is required'));
		exit();
		}
		
		$data = array(
		'cat_name' 	=> $this->input->post('cat_name'),
		'cat_status'	=> 'Enable',
		'cat_added' 	=> date('Y-m-d H:i:s'),
		'cat_updated' 	=> date('Y-m-d H:i:s')
		);
		
		$config = array(
		'field' => 'cat_uri',
		'title' => 'cat_name',
		'table' => 'blog_categories',
		'id' => 'cat_id',
		);
		$this->load->library('slug', $config);
		$udata = array('cat_name' => $data['cat_name']);
		$data['cat_uri'] = $this->slug->create_uri($udata);
		$count = $this->SqlModel->countRecords($this->tblName, array('cat_name'=> $this->input->post('cat_name')));
		if($count>0)
		{
		echo json_encode(array('status'=>'false','message'=>'Category name is alreay exist.'));	
		exit();	
		}
		$q = $this->SqlModel->insertRecord($this->tblName, $data);
		
		if($q!="")
		{
			echo json_encode(array('status'=>'true','cat_id'=>$q,'cat_name'=>$data['cat_name']));		
		}
		else{
			echo json_encode(array('status'=>'false','message'=>'Error occurred during transmission, please try again'));	
		}	
	}
		

	
}

