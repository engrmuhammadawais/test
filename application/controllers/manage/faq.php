<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Faq extends CI_Controller {

	public $tblName = 'faqs';
	public $pKey = 'faq_id';
	public $moduleName = "FAQs";
	public $controller = "faq";
	 public function __construct(){

        // Call the Model constructor
	   parent::__construct();
	   $this->SqlModel->setTitle();
		if($this->session->userdata('admin_auth')!="1")
		{
		redirect(base_url().'manage/login','location');
		}
		$this->user_data = $this->SqlModel->getSingleRecord('admin_users' , array('id'=>$this->session->userdata('admin_id')));
		$this->load->helper('text');
		
    }
	
	//For listing the FAQ
	public function index($alert="",$sortby="faq_added", $order="DESC", $pg_no="")
	{
		$data['page_title'] = PROJECT_TITLE." | ".$this->moduleName;
		$data['alert'] = $alert;
		$data['userdata'] = $this->user_data;
		$data['faqActive'] = 1;
		$data['per_page'] = 10;
		//Pagination START
			$count_rows = $this->SqlModel->countRecords($this->tblName, array());
			$pconfig['base_url'] = base_url().'manage/'.$this->controller.'/index/page/'.$sortby."/".$order;
			$data['total_rows'] = $count_rows;
			$pconfig['total_rows'] =  $count_rows;
			$pconfig["uri_segment"] = 7;
			$pconfig['per_page'] = $data['per_page'];
			$pconfig['num_links'] = 1;
			$pconfig['prev_link'] = '<i class="entypo-left-open-mini"></i>';
			$pconfig['next_link'] = '<i class="entypo-right-open-mini"></i>';
			$pconfig['cur_tag_open'] = '<li  class="active"><a href="javascript:void(0)">';
			$pconfig['cur_tag_close'] = '</a></li>';
			$pconfig['full_tag_open'] = '<ul class="pagination pull-right">';
   			$pconfig['full_tag_close'] = '</ul>';
			$pconfig['num_tag_open'] = "<li>";
			$pconfig['num_tag_close']= "</li>";
			$pconfig['next_tag_open'] = "<li>";
			$pconfig['next_tag_close']= "</li>";
			$pconfig['prev_tag_open'] = "<li>";
			$pconfig['prev_tag_close']= "</li>";
			$pconfig['last_tag_open'] = "<li>";
			$pconfig['last_tag_close']= "</li>";
			$pconfig['first_tag_open'] = "<li>";
			$pconfig['first_tag_close']= "</li>";
			$page = ($this->uri->segment(7)) ? $this->uri->segment(7) : 0;
			if($pg_no!="")
			{
				$page = $pg_no;
			}
		
			$this->pagination->initialize($pconfig);
			//Load Data			
			$data['slider'] = $this->SqlModel->getRecords('*', $this->tblName, $sortby, $order, array(), $pconfig["per_page"], $page);
			$data['paginate'] = $this->pagination->create_links();	
			//Pagination END
			$data['sortby'] = $sortby;
			if($order=="ASC")
			{
				$order = "DESC";	
			}
			else if($order=="DESC")
			{
				$order = "ASC";	
			}
			$data['order'] = $order;
			$data['page_numb'] = $page;
		
		$this->load->view('admin/header',$data);
		$this->load->view('admin/navigation');
		$this->load->view('admin/faqs');
		$this->load->view('admin/footer');
	}
	
	//For adding/edting FAQ
	public function control($alert="",$editID="",$editerror="")
	{
		
		$data['faqActive'] = 1;//Alert for Set Active in menu
		$data['alert'] = $alert;//Alert for showing message by alert Status
		$data['editerror'] = $editerror;//Message Status
		$type = ($editID=="") ? "Add" : "Edit";//Set Type
		$data['page_title'] = PROJECT_TITLE." | ".$type." ".rtrim($this->moduleName,'s'); //Page Title
		if($alert=="error" || $alert=="image_error")
		{
		$data['tbl_data'] = $this->session->userdata($this->controller.'_data');		
		}
		else if($alert=="edit")
		{
			if($editID=="")
			{
			redirect(base_url().'manage/'.$this->controller.'/control','location');	
			exit();	
			}
			$count = $this->SqlModel->countRecords($this->tblName, array('faq_id'=>$editID));
			if($count==0)
			{
			redirect(base_url().'manage/'.$this->controller,'location');
			}
			
			$data['tbl_data'] = $this->SqlModel->getSingleRecord($this->tblName, array($this->pKey=>$editID));
		}
		$data['userdata'] = $this->user_data;
		$this->load->view('admin/header',$data);
		$this->load->view('admin/navigation');
		$this->load->view('admin/addFAQ');
		$this->load->view('admin/footer');
	}
	
	//For add record FAQ
	public function addRecord()
	{	
		$data = array(
		'faq_question' 	=> $this->input->post('faq_question'),
		'faq_answer' 	=> $this->input->post('faq_answer'),
		'faq_status' => $this->input->post('faq_status'),
		'faq_added' 	=> date('Y-m-d H:i:s'),
		'faq_updated' 	=> date('Y-m-d H:i:s')
		);
		
	
		$q = $this->SqlModel->insertRecord($this->tblName, $data);
		$this->session->unset_userdata($this->controller.'_data');
		if($q!="")
		{
			redirect(base_url().'manage/'.$this->controller.'/index/success','location');		
		}
		else{
			redirect(base_url().'manage/'.$this->controller.'/index/error','location');		
		}	
	}
		
	//For edit record
	public function editRecord($editID="")
	{
		if($editID=="")
		{
		redirect(base_url().'manage/'.$this->controller,'location');	
		exit();	
		}
		
		
		$data = array(
		'faq_question' 	=> $this->input->post('faq_question'),
		'faq_answer' 	=> $this->input->post('faq_answer'),
		'faq_status' => $this->input->post('faq_status'),
		'faq_updated' 	=> date('Y-m-d H:i:s')
		);
		
		
		
		$q = $this->SqlModel->updateRecord($this->tblName, $data, array($this->pKey=>$editID));
		if($q==true)
		{
		redirect(base_url().'manage/'.$this->controller.'/index/editsuccess','location');		
		}
		else{
		redirect(base_url().'manage/'.$this->controller.'/index/error','location');		
		}	
	}
	
	//For delete FAQ
	public function delete($deleteID="")
	{
		$q = $this->SqlModel->deleteRecord($this->tblName , array($this->pKey=>$deleteID));
		if($q==true)
		{
		redirect(base_url().'manage/'.$this->controller.'/index/deletesuccess','location');		
		}
		else{
		redirect(base_url().'manage/'.$this->controller.'/index/deleteerror','location');		
		}
		
		
	}
	
	//For delete selected / all
	public function deleteall()
	{
		$ids = $this->input->post('records');
		if(!empty($ids))
		{
			foreach($ids as $id)
			{
			$this->SqlModel->deleteRecord($this->tblName,  array($this->pKey=>$id));	
			}
		}
		redirect(base_url().'manage/'.$this->controller.'/index/deletesuccess','location');		
		
	}
	
	

	
}

