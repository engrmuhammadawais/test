<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Activity extends CI_Controller {

	public $tblName = 'activities';
	public $pKey = 'id';
	public $moduleName = "Activities";
	public $controller = "activity";
	public $listingView = "activityListing"; 
	public $addeditView = "addEditActivity"; 
	public $per_page = "10"; 	
	public function __construct(){

        // Call the Model constructor
		parent::__construct();
		$this->SqlModel->setTitle();
		if($this->session->userdata('admin_auth')!="1")
		{
		redirect(base_url().'manage/login','location');
		}
		$this->user_data = $this->SqlModel->getSingleRecord('admin_users' , array('id'=>$this->session->userdata('admin_id')));
		$this->load->helper('text');
    }
	
	//For listing the activity
	public function index($sortby="id", $order="DESC", $status="-",$keywords="-", $pg_no="")
	{
		$data['page_title'] = PROJECT_TITLE." | ".$this->moduleName;
		$data['userdata'] = $this->user_data;
		$data['activityActive'] = true;
			$this->load->helper('inflector');
		
			$data['alert'] = $this->session->flashdata('alert');

			// Set filter here
			$where =  array(				
			);

			// Set select query here
			$select = $this->tblName.'.*, admin_users.user_name';

			$keywords = urldecode($keywords);

			if($status!="-")
			{
				$where[ 'admin_users.user_name' ] = $status;
			}
			
			$search = ($keywords!="-") ? array('cols'=>'user_name,log_key','value'=>urldecode($keywords)) : array();	
			
			//Pagination START
			$base_url 			= base_url().'manage/'.$this->controller.'/index/'.$sortby."/".$order."/".$status."/".$keywords;
			$total_rows 		= $data['total_rows'] = $this->SqlModel->countRecords($this->tblName. ' INNER JOIN admin_users ON admin_users.id = '.$this->tblName.'.user_id',$where,$search);
			$per_page 			= $data['per_page'] = $this->per_page;
			$uri_segment 		= 8;
			//call the pagination function for initialization
			$pconfig 			= $this->pconfig($base_url,$total_rows,$per_page,$uri_segment);
			$this->pagination->initialize($pconfig);
			$data['paginate'] 	= $this->pagination->create_links();	
			$offset 			= ($this->uri->segment($uri_segment)) ? $this->uri->segment($uri_segment) : 0;
			//Pagination END
			//get records
			$data['rows'] 		= $this->SqlModel->getRecords($select, $this->tblName. ' INNER JOIN admin_users ON admin_users.id = '.$this->tblName.'.user_id'	, $sortby, $order, $where, $per_page, $offset);
			
			$data['sortby'] 	=   $sortby;
			$data['order'] 		= 	($order=="ASC") ? "DESC" : "ASC";
			$data['page_numb'] 	= 	$offset;
			$data['status']		=	$status;
			$data['keywords']	=	$keywords;
		//load views
		$this->load->view('admin/header',$data);
		$this->load->view('admin/navigation');
		$this->load->view('admin/'.$this->listingView);
		$this->load->view('admin/footer');
	}
	
	//For Detail Activity
	public function control($editID="")
	{
		$data['activityActive'] = true;
		$this->load->helper('inflector');
		$data['editID'] = $editID;
		$data['alert'] = $this->session->flashdata('alert');
		$data['type'] = "View";
		
		$data['page_title'] = PROJECT_TITLE." | ".$this->moduleName;
		$data['tbl_data'] = array();
		//check is there any error 
		if($data['alert']=="error")
		{
			$data['tbl_data'] = $this->session->userdata($this->controller.'_data');	
		}
		else if($editID!="")
		{
		//check id 
			$where = array(
				$this->pKey=>$editID
			);
			//Get Data
			$data['tbl_data'] = $this->SqlModel->runQuery("SELECT ka.id,ka.user_id,kc.`user_name`,kc.`full_name`,kc.`email`,kc.user_agent,kc.ip,kc.last_login,log_key,log_value,log_others,datetime FROM activities ka INNER JOIN admin_users kc ON kc.`id` = ka.`user_id` WHERE ka.id = " . $editID, true);
			//check Data 
			if(empty($data['tbl_data']))
			{
				redirect(base_url('manage/'.$this->controller));	
			}

		}
		//User Data
		$data['userdata'] = $this->user_data;
		//Load and assign data to views
		$this->load->view('admin/header',$data);
		$this->load->view('admin/navigation');
		$this->load->view('admin/'.$this->addeditView);
		$this->load->view('admin/footer');
	}	
	//pagination function
	function pconfig($base_url,$total_rows,$per_page,$uri_segment)
	{	
			$pconfig['base_url'] = $base_url;
			$pconfig['total_rows'] = $total_rows;
			$pconfig['per_page'] = $per_page;
			$pconfig["uri_segment"] = $uri_segment;
			$pconfig['num_links'] = 1;
			$pconfig['prev_link'] = '<i class="entypo-left-open-mini"></i>';
			$pconfig['next_link'] = '<i class="entypo-right-open-mini"></i>';
			$pconfig['cur_tag_open'] =  '<li  class="active"><a href="javascript:void(0)">';
			$pconfig['cur_tag_close'] = '</a></li>';
			$pconfig['full_tag_open'] ='<ul class="pagination pull-right">';
   			$pconfig['full_tag_close'] = '</ul>';
			$pconfig['num_tag_open'] = "<li>";
			$pconfig['num_tag_close']= "</li>";
			$pconfig['next_tag_open'] = "<li>";
			$pconfig['next_tag_close']= "</li>";
			$pconfig['prev_tag_open'] = "<li>";
			$pconfig['prev_tag_close']= "</li>";
			$pconfig['last_tag_open'] = "<li>";
			$pconfig['last_tag_close']= "</li>";
			$pconfig['first_tag_open'] = "<li>";
			$pconfig['first_tag_close']= "</li>";
			return $pconfig;	
		
	}
	
	
}

