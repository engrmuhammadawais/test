<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Controller {

	public $tblName = 'admin_users';

	 public function __construct(){

        // Call the Model constructor
	   parent::__construct();
	   $this->SqlModel->setTitle();
		if($this->session->userdata('admin_auth')!="1"||$this->session->userdata('admin_role')!="Super Admin")
		{
		redirect(base_url().'manage/login','location');
		}
		$this->user_data = $this->SqlModel->getSingleRecord('admin_users' , array('id'=>$this->session->userdata('admin_id')));
		$this->load->helper('text');
    }
	
	//For listing the users
	public function index($alert="",$sortby="full_name", $order="ASC", $pg_no="")
	{
		$data['alert'] = $alert;
		$data['usersActive'] = 1;
		$data['userdata'] = $this->user_data;
		$data['page_title'] = PROJECT_TITLE." | Users";
		//Pagination START
			$count_rows = $this->SqlModel->countRecords($this->tblName, array());
			$pconfig['base_url'] = base_url().'manage/users/index/page/'.$sortby."/".$order;
			$pconfig['total_rows'] =  $count_rows;
			$pconfig["uri_segment"] = 7;
			$data['total_rows'] = $count_rows;
			$data['per_page'] = 10;
			$pconfig['per_page'] = $data['per_page'];
			$pconfig['num_links'] = 1;
			$pconfig['prev_link'] = '<i class="entypo-left-open-mini"></i>';
			$pconfig['next_link'] = '<i class="entypo-right-open-mini"></i>';
			$pconfig['cur_tag_open'] = '<li  class="active"><a href="javascript:void(0)">';
			$pconfig['cur_tag_close'] = '</a></li>';
			$pconfig['full_tag_open'] = '<ul class="pagination pull-right">';
   			$pconfig['full_tag_close'] = '</ul>';
			$pconfig['num_tag_open'] = "<li>";
			$pconfig['num_tag_close']= "</li>";
			$pconfig['next_tag_open'] = "<li>";
			$pconfig['next_tag_close']= "</li>";
			$pconfig['prev_tag_open'] = "<li>";
			$pconfig['prev_tag_close']= "</li>";
			$pconfig['last_tag_open'] = "<li>";
			$pconfig['last_tag_close']= "</li>";
			$pconfig['first_tag_open'] = "<li>";
			$pconfig['first_tag_close']= "</li>";
			$page = ($this->uri->segment(7)) ? $this->uri->segment(7) : 0;
			if($pg_no!="")
			{
				$page = $pg_no;
			}
		
			$this->pagination->initialize($pconfig);
			$data['admins'] = $this->SqlModel->getRecords('*', $this->tblName, $sortby, $order, array(), $pconfig["per_page"], $page);
			$data['paginate'] = $this->pagination->create_links();	
		//Pagination END
			$data['sortby'] = $sortby;
			if($order=="ASC")
			{
				$order = "DESC";	
			}
			else if($order=="DESC")
			{
				$order = "ASC";	
			}
			$data['order'] = $order;
			$data['page_numb'] = $page;
		
		$this->load->view('admin/header',$data);
		$this->load->view('admin/navigation');
		$this->load->view('admin/userListing');
		$this->load->view('admin/footer');
	}
	
	//For adding/edting users
	public function control($alert="",$editID="",$editerror="")
	{
		$data['usersActive'] = 1;
		$data['alert'] = $alert;
		$data['editerror'] = $editerror;
		$type = ($editID=="") ? "Add" : "Edit";
		$data['page_title'] = PROJECT_TITLE." | ".$type." Users";
		if($alert=="error")
		{
		$data['tbl_data'] = $this->session->userdata('admin_data');	
		}
		else if($alert=="edit")
		{
			if($editID=="")
			{
			redirect(base_url().'manage/users/control','location');	
			exit();	
			}
			$count = $this->SqlModel->countRecords($this->tblName, array('id'=>$editID));
			if($count==0)
			{
			redirect(base_url().'manage/users','location');
			}
			$data['tbl_data'] = $this->SqlModel->getSingleRecord($this->tblName, array('id'=>$editID));
		}
	
		$data['userdata'] = $this->user_data;
		$this->load->view('admin/header',$data);
		$this->load->view('admin/navigation');
		$this->load->view('admin/addUser');
		$this->load->view('admin/footer');
	}
	
	
	//For add record
	public function addRecord()
	{
		if($this->input->post('full_name')=="" || $this->input->post('email')=="" || $this->input->post('pwd')=="" || $this->input->post('user_name')=="")
		{
		redirect(base_url().'manage/users/index/error','location');	
		exit();
		}
		
		$data = array(
		'full_name' => $this->input->post('full_name'),
		'email' 	=> $this->input->post('email'),
		'user_name'	=> $this->input->post('user_name'),
		'pwd' => md5($this->input->post('pwd')),
		'user_role'	=> $this->input->post('user_role'),
		'status'	=> $this->input->post('status'),
		'date_created' => date('Y-m-d H:i:s')
		);
		
		$cquery = "SELECT count(*) as cnt from ".$this->tblName." WHERE user_name = '".$this->input->post('user_name')."' OR email = '".$this->input->post('email')."' ";
		$cData = $this->SqlModel->runQuery($cquery,1);
		$count = $cData['cnt'];
		
		if($count>0)
		{
		$this->session->set_userdata('admin_data', $data);
		redirect(base_url().'manage/users/control/error','location');	
		exit();	
		}
		
		$q = $this->SqlModel->insertRecord($this->tblName, $data);
		
		$this->session->unset_userdata('admin_data');
		if($q!="")
		{
		redirect(base_url().'manage/users/index/success','location');		
		}
		else{
		redirect(base_url().'manage/users/index/error','location');		
		}	
	}
	
	
	//For edit Record
	public function editRecord($editID="")
	{
		if($editID=="")
		{
		redirect(base_url().'manage/users','location');	
		exit();	
		}
		
		if($this->input->post('full_name')=="" || $this->input->post('email')=="" || $this->input->post('pwd')=="" || $this->input->post('user_name')=="")
		{
		redirect(base_url().'manage/users/index/error','location');	
		exit();
		}
		
		$data = array(
		'full_name' => $this->input->post('full_name'),
		'email' 	=> $this->input->post('email'),
		'user_name'	=> $this->input->post('user_name'),
		'user_role'	=> $this->input->post('user_role'),
		'status'	=> $this->input->post('status'),
		'last_modified' => date('Y-m-d H:i:s')
		);
		
		$current_pwd = $this->SqlModel->getSingleField('pwd',$this->tblName,array('id'=>$editID));
		if($current_pwd!=$this->input->post('pwd'))
		{
		$data['pwd'] = 	md5($this->input->post('pwd'));
		}
		$cquery = "SELECT count(*) as cnt from ".$this->tblName." WHERE (user_name = '".$this->input->post('user_name')."' OR email = '".$this->input->post('email')."' ) AND id !='".$editID."'";
		$cData = $this->SqlModel->runQuery($cquery,1);
		$count = $cData['cnt'];
		if($count>0)
		{
		$this->session->set_userdata('admin_data', $data);
		redirect(base_url().'manage/users/control/edit/'.$editID.'/editerror','location');	
		exit();	
		}
		
		$q = $this->SqlModel->updateRecord($this->tblName, $data, array('id'=>$editID));
		if($q==true)
		{
		redirect(base_url().'manage/users/index/editsuccess','location');		
		}
		else{
		redirect(base_url().'manage/users/index/error','location');		
		}	
	}
	
	//For delete Record
	public function delete($deleteID="")
	{
		$q = $this->SqlModel->deleteRecord($this->tblName , array('id'=>$deleteID));
		if($q==true)
		{
		redirect(base_url().'manage/users/index/deletesuccess','location');		
		}
		else{
		redirect(base_url().'manage/users/index/deleteerror','location');		
		}
		
		
	}
	
	//For delete selected user
	public function deleteall()
	{
		$ids = $this->input->post('records');
		if(!empty($ids))
		{
			foreach($ids as $id)
			{
			$this->SqlModel->deleteRecord($this->tblName ,array('id'=>$id));	
			}
		}
		redirect(base_url().'manage/users/index/deletesuccess','location');		
	}
	
	
	
	
	
	
	
}

