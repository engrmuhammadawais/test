<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	 public function __construct(){

        // Call the Model constructor
	   parent::__construct();
	   $this->SqlModel->setTitle();
		if($this->session->userdata('admin_auth')!="1")
		{
		redirect(base_url().'manage/login','location');
		}
		
		$this->load->model('EmailModel');
		$this->user_data = $this->SqlModel->getSingleRecord('admin_users' , array('id'=>$this->session->userdata('admin_id')));
    }
	//Dashoard
	//Parameter @alert use for message
	public function index($alert="")
	{
		$data['dashBoardActive'] = 1;
		$data['totalUsers'] = $this->SqlModel->countRecords('admin_users');
		$data['totalFAQ'] = $this->SqlModel->countRecords('faqs');
		$data['totalPC'] = $this->SqlModel->countRecords('postcodes');
		$data['totalBookings'] = $this->SqlModel->countRecords('bookings');
		$data['totalBlogs'] = $this->SqlModel->countRecords('blogs');
		$data['totalPages'] = $this->SqlModel->countRecords('pages');
		$data['totalNews'] = 0;
		$data['totalBackgrounds'] = 0;
		$data['page_title'] = PROJECT_TITLE." | Dashboard";
		$data['alert'] = $alert;
		$data['userdata'] = $this->user_data;
		$this->load->view('admin/header',$data);
		$this->load->view('admin/navigation');
		$this->load->view('admin/dashboard');
		$this->load->view('admin/footer');
	}
	//Edit Home Page
	public function edit()
	{
		$data['homeActive'] = 1;
		$data['page_title'] = PROJECT_TITLE." | Edit Home";
		$data['alert'] = $this->session->flashdata('alert');
		$data['userdata'] = $this->user_data;
		$data['home'] = $this->SqlModel->getSingleRecord('home_page',array('id'=>1));
		$this->load->view('admin/header',$data);
		$this->load->view('admin/navigation');
		$this->load->view('admin/editHome');
		$this->load->view('admin/footer');
		
	}
	//Save Home Page info
	public function savehome()
	{
		if($this->input->post('homesubmit')=="Submit")
		{
			$data = array(
			  'banner_head' => $this->input->post('banner_head'),
			  'banner_subhead' => $this->input->post('banner_subhead'),
			  'banner_p1' => $this->input->post('banner_p1'),
			  'banner_p2' => $this->input->post('banner_p2'),
			  'banner_p3' => $this->input->post('banner_p3'),
			  'banner_p4' => $this->input->post('banner_p4'),
			  'banner_p5' => $this->input->post('banner_p5'),
			  'banner_p6' => $this->input->post('banner_p6'),
			  'banner_p7' => $this->input->post('banner_p7'),
			  'green_top_head' => $this->input->post('green_top_head'),
			  'green_sub_head1' => $this->input->post('green_sub_head1'),
			  'green_sub_text1' => $this->input->post('green_sub_text1'),
			  'green_sub_head2' => $this->input->post('green_sub_head2'),
			  'green_sub_text2' => $this->input->post('green_sub_text2'),
			  'green_sub_head3' => $this->input->post('green_sub_head3'),
			  'green_sub_text3' => $this->input->post('green_sub_text3'),
			  'tidy_head' => $this->input->post('tidy_head'),
			  'tidy_sub_head' => $this->input->post('tidy_sub_head'),
			  'com_head' => $this->input->post('com_head'),
			  'com_text' => $this->input->post('com_text'),
			  'com_p1' => $this->input->post('com_p1'),
			  'com_p2' => $this->input->post('com_p2'),
			  'com_p3' => $this->input->post('com_p3'),
			  'com_p4' => $this->input->post('com_p4'),
			  'com_p5' => $this->input->post('com_p5'),
			  'com_p6' => $this->input->post('com_p6'),
			  'com_p7' => $this->input->post('com_p7'),
			);
			$q = $this->SqlModel->updateRecord('home_page',$data,array('id'=>1));
			if($q==true)
			{
				$this->session->set_flashdata('alert','success');
			}
			else{
				$this->session->set_flashdata('alert','error');	
			}
		}
		else{
			$this->session->set_flashdata('alert','error');
		}
		redirect(base_url('manage/home/edit'));
	}
	
	//Admin Account Settings
	public function settings($alert="")
	{
		if($this->session->userdata('admin_auth')!="1")
		{
		redirect(base_url().'manage/login','location');
		}
		$data['page_title'] = PROJECT_TITLE." | Account Settings";
		$data['alert'] = $alert;
		$data['userdata'] = $this->user_data;
		$this->load->view('admin/header',$data);
		$this->load->view('admin/navigation');
		$this->load->view('admin/adminSettings');
		$this->load->view('admin/footer');
	}
	
	
	//Save Admin Account Settings
	public function savesettings()
	{
		if($this->input->post('admin_name')=="" || $this->input->post('admin_email')=="")
		{
		redirect(base_url().'manage/home/settings/error', 'location');
		exit();
		}
		
		$this->SqlModel->updateRecord('admin_users' , array('full_name'=>$this->input->post('admin_name'), 'email'=>$this->input->post('admin_email')) , array('id'=>$this->user_data['id']));
		
		
		if($this->input->post('admin_current_pwd')!="")
		{
			if(md5($this->input->post('admin_current_pwd'))!=$this->user_data['pwd'])
			{
			redirect(base_url().'manage/home/settings/perror', 'location');	
			exit();
			}
			else{
				$this->SqlModel->updateRecord('admin_users' , array('pwd'=>md5($this->input->post('admin_new_pwd'))) , array('id'=>$this->user_data['id']));
			}
		}
		
		redirect(base_url().'manage/home/settings/success', 'location');
	}
	
	//Admin Logout
	public function logout(){
		$data = $this->user_data;		
		//Load Activity Model
		$this->load->model('activitymodel');
		//Set Activity
		$this->activitymodel->track( array('user_agent','ip') )->addActivity( $data['id'], 'logout' );
		$this->session->sess_destroy();
		redirect(base_url().'manage','location');
	}
	
	
	public function table($alert="")
	{
		$data['page_title'] = PROJECT_TITLE." | Dashboard";
		$data['alert'] = $alert;
		$data['userdata'] = $this->user_data;
		$this->load->view('admin/header',$data);
		$this->load->view('admin/navigation');
		$this->load->view('admin/mainTable');
		$this->load->view('admin/footer');
	}
	
	public function form($alert="")
	{
		$data['page_title'] = PROJECT_TITLE." | Dashboard";
		$data['alert'] = $alert;
		$data['userdata'] = $this->user_data;
		$this->load->view('admin/header',$data);
		$this->load->view('admin/navigation');
		$this->load->view('admin/form');
		$this->load->view('admin/footer');
	}
	
	
	public function websettings($alert="")
	{
		$data['wsettingActive'] = 1;
		$data['page_title'] = PROJECT_TITLE." | Website Settings";
		//CKEditor
		$this->load->library('ckeditor');
		$this->ckeditor->basePath = base_url().'assets/ckeditor/';
		$this->ckeditor->config['removePlugins'] ='save, preview, newpage, forms';
		$this->ckeditor->config['height'] = '240px'; 
	//	$this->ckeditor->config['extraPlugins'] = 'mediaembed';           
		$this->ckeditor->config['toolbar'] = array(
						array( 'Source', '-', 'MediaEmbed','Bold', 'Italic', 'Underline', '-','Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo','-','NumberedList','BulletedList' )
		);
			
		//Add Ckfinder to Ckeditor
		$data['alert'] = $alert;
		$data['userdata'] = $this->user_data;
		$data['web'] = $this->SqlModel->getSingleRecord('site_settings',array('id'=>1));
		$this->load->view('admin/header',$data);
		$this->load->view('admin/navigation');
		$this->load->view('admin/webSettings');
		$this->load->view('admin/footer');
	}
	
	public function savewebsettings()
	{
		
		$data = array(
		'address'	=>	$this->input->post('address'),
		'fax'		=>	$this->input->post('fax'),
		'website_title' => $this->input->post('website_title'),
		'website_url' => $this->input->post('website_url'),
		'phone'		=>	$this->input->post('phone'),
		'm_phone'		=>	$this->input->post('m_phone'),
		'b_phone'		=>	$this->input->post('b_phone'),
		'm_email'		=>	$this->input->post('m_email'),
		'b_email'		=>	$this->input->post('b_email'),
		'analytics'		=>	$this->input->post('analytics'),
		'map'		=>	$_REQUEST['map'],
		'contact_text'		=>	(isset($_REQUEST['contact_text']))? $_REQUEST['contact_text'] : '',
		'lat_long'	=>	$this->input->post('lat_long'),
		'contact_form_email'	=>	$this->input->post('contact_form_email'),
		'email'		=>	$this->input->post('email'),
		'twitter'		=>	$this->input->post('twitter'),
		'facebook'		=>	$this->input->post('facebook'),
		'google'		=>	$this->input->post('google'),
		'linkedin'		=>	$this->input->post('linkedin'),
		'pinterest'		=>	$this->input->post('pinterest'),
		'flickr'		=>	$this->input->post('flickr'),
		'youtube'		=>	$this->input->post('youtube'),
		'vimeo'		=>	$this->input->post('vimeo'),
		'instagram'		=>	$this->input->post('instagram'),
		'wimp'		=>	$this->input->post('wimp'),
		'itunes'		=>	$this->input->post('itunes'),
		'spotify'		=>	$this->input->post('spotify')
		);
		
		if(isset($_FILES['uploadfile']))
		{
			$config['upload_path'] = './assets/frontend/images/logo/';
			$config['allowed_types'] = 'jpg|png|jpeg';
			$config['max_size']	= '102400';
			$config['max_width']  = '12000';
			$config['max_height']  = '12000';
			$config['remove_spaces'] = true;
			$this->load->library('upload', $config);
			$this->load->library('image_lib');
					
			if ($this->upload->do_upload('uploadfile'))
			{
				    
				    $filename_ephoto = $this->upload->data('uploadfile');
					$data['logo'] = $filename_ephoto['file_name'];
			}
			
		}
		
		
		$q = $this->SqlModel->updateRecord('site_settings' , $data , array('id'=>1));
		if($q==true)
		{
			redirect(base_url().'manage/home/websettings/success', 'location');
		}
		else{
			redirect(base_url().'manage/home/websettings/error', 'location');
		}
	}
	
	
	
}

