<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blog extends CI_Controller {
	public $websettings;
	 public function __construct(){

        // Call the Model constructor
	   parent::__construct();
	   $this->SqlModel->setTitle();
		$this->websettings = $this->SqlModel->getSingleRecord('site_settings',array('id'=>1));
		
    }
	
	
	public function index()
	{
		$in_query = "";
		$param = "";
		$blog_cat = $this->input->get('cat');
		if($blog_cat!="")
		{
			$cnt_cat = $this->SqlModel->runQuery("SELECT GROUP_CONCAT(bc_blog_id) as cnt FROM blog_assigned_cat WHERE bc_cat_id=".$blog_cat,1);
			$in_query = "  b.blog_id IN (".$cnt_cat['cnt'].") AND ";
			$param = "?cat=".$blog_cat;
		}
		$blog_tag = $this->input->get('tag');
		if($blog_tag!="")
		{
			$cnt_tag = $this->SqlModel->runQuery("SELECT GROUP_CONCAT(bt_blog_id) as cnt FROM blog_assigned_tags WHERE bt_tag_id=".$blog_tag,1);
			$in_query = "  b.blog_id IN (".$cnt_tag['cnt'].")  AND ";
			$param = "?tag=".$blog_tag;
		}
		
		
		
		
		$page = $this->input->get('page');
		$cats_query = "SELECT 
  cat_id,
  cat_name,
  (SELECT 
    COUNT(*) 
  FROM
    blog_assigned_cat bc 
    INNER JOIN blogs b 
      ON bc.bc_blog_id = b.blog_id 
  WHERE bc_cat_id = bcat.cat_id) AS cat_total 
FROM
  blog_categories bcat 
WHERE cat_status = 'Enable' 
ORDER BY cat_name ASC ";
		$cats = $this->SqlModel->runQuery($cats_query);
		
		//Tags
		$tags_query ="SELECT 
  tag_id,
  tag_name,
  (SELECT 
    COUNT(*) 
  FROM
    blog_assigned_tags bt 
    INNER JOIN blogs b 
      ON bt.bt_blog_id = b.blog_id 
  WHERE bt_tag_id = btag.tag_id) AS tag_total 
FROM
  blog_tags btag 
WHERE tag_status = 'Enable' AND (SELECT 
    COUNT(*) 
  FROM
    blog_assigned_tags bt 
    INNER JOIN blogs b 
      ON bt.bt_blog_id = b.blog_id 
  WHERE bt_tag_id = btag.tag_id) >0 
ORDER BY tag_name ASC ";
	$tags = $this->SqlModel->runQuery($tags_query);
		
		
		$cnt_query = "SELECT 
  COUNT(*) AS cnt
FROM
  blogs b 
WHERE b.`blog_status` = 'Published'  
  AND  ".$in_query."
  (SELECT 
    COUNT(*) 
  FROM
    blog_assigned_cat bc 
  WHERE  bc.`bc_blog_id` = b.blog_id) > 0 ";
 
  $cnt = $this->SqlModel->runQuery($cnt_query,1);
		//$blogs =  $this->SqlModel->getRecords('*','blogs','blog_added','DESC',array('blog_status'=>'Published'));
		//Pagination Start
			$count_rows = $cnt['cnt'];
			
			$pconfig['base_url'] = base_url().'blog/index';
			$data['total_rows'] = $count_rows;
			$pconfig['total_rows'] =  $count_rows;
			$pconfig["uri_segment"] = 3;
			$pconfig['per_page'] = 2;
			 
			$pconfig['num_links'] = 1;
			$pconfig['next_link'] = 'Next';
			$pconfig['prev_link'] = 'Previous';
			$pconfig['cur_tag_open'] = '<span>';
			$pconfig['cur_tag_close'] = '</span>';
			$pconfig['full_tag_open'] = ' <div class="blog-pagination">';
   			$pconfig['full_tag_close'] = ' </div>';
			
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			$this->pagination->initialize($pconfig);
			
			$blog_qyery = "SELECT 
  * 
FROM
  blogs b 
WHERE b.`blog_status` = 'Published' 
  AND ".$in_query."
  (SELECT 
    COUNT(*) 
  FROM
    blog_assigned_cat bc 
  WHERE  bc.`bc_blog_id` = b.blog_id) > 0 
ORDER BY b.blog_added DESC LIMIT ".$page.",".$pconfig["per_page"];
$blogs = $this->SqlModel->runQuery($blog_qyery);
			
			
			if(count($blogs)>0)
			{
				foreach($blogs as $k=>$b)
				{
			
					$qt = "SELECT t.tag_id,t.tag_name FROM blog_tags t INNER JOIN blog_assigned_tags bt ON bt.`bt_tag_id`=t.`tag_id` WHERE t.`tag_status`='Enable' AND bt_blog_id='".$b['blog_id']."' ORDER BY t.`tag_name` ASC";
					$blogs[$k]['tags'] = $this->SqlModel->runQuery($qt);
					$qc = "SELECT c.cat_id,c.cat_name FROM blog_categories c INNER JOIN blog_assigned_cat bc ON bc.`bc_cat_id`=c.`cat_id` WHERE c.`cat_status`='Enable' AND bc_blog_id='".$b['blog_id']."' ORDER BY c.`cat_name` ASC";
					$blogs[$k]['cats'] = $this->SqlModel->runQuery($qc);
				}
			}
			$paginate = $this->pagination->create_links();	
			if($paginate!="")
			{
				$xml = new DOMDocument();
				// Load the url's contents into the DOM
				$xml->loadHTML($paginate);
				// Empty array to hold all links to return
				$links = array();
				//Loop through each <a> tag in the dom and add it to the link array
				foreach($xml->getElementsByTagName('a') as $link) {
				 $link->setAttribute('href', $link->getAttribute('href').$param);       
				 }
				$paginate = $xml->saveHTML();
			}
		
		
		$nav = $this->SqlModel->getNav();
		$data = array(
		'websettings'=>$this->websettings,
		'page_title'	=> PROJECT_TITLE." | Blog",
		'page_meta_key'	=> '',
		'page_meta_desc'	=> '',
		'nav' =>$nav,
		'cats' =>$cats,
		'blogs'=>$blogs,
		'tags'=>$tags,
		'tags'=>$tags,
		'paginate'=>$paginate,
		'active' =>3
		);
	
		
	  $this->load->view('header',$data);
	  $this->load->view('blog');
	  $this->load->view('footer');
	}
	
	public function details($blog_uri="")
	{
		$blog_uri = str_replace(".html","",$blog_uri);
		$blogData = $this->SqlModel->getSingleRecord('blogs',array('blog_uri'=>$blog_uri,'blog_status'=>'Published'));
		if(empty($blogData))
		{
			redirect(base_url('blog'));
			exit;	
		}
		$qt = "SELECT t.tag_id,t.tag_name FROM blog_tags t INNER JOIN blog_assigned_tags bt ON bt.`bt_tag_id`=t.`tag_id` WHERE t.`tag_status`='Enable' AND bt_blog_id='".$blogData['blog_id']."' ORDER BY t.`tag_name` ASC";
		$blogData['tags'] = $this->SqlModel->runQuery($qt);
		$qc = "SELECT c.cat_id,c.cat_name FROM blog_categories c INNER JOIN blog_assigned_cat bc ON bc.`bc_cat_id`=c.`cat_id` WHERE c.`cat_status`='Enable' AND bc_blog_id='".$blogData['blog_id']."' ORDER BY c.`cat_name` ASC";
		$blogData['cats'] = $this->SqlModel->runQuery($qc);
		#############################################################################################
		$cats_query = "SELECT 
  cat_id,
  cat_name,
  (SELECT 
    COUNT(*) 
  FROM
    blog_assigned_cat bc 
    INNER JOIN blogs b 
      ON bc.bc_blog_id = b.blog_id 
  WHERE bc_cat_id = bcat.cat_id) AS cat_total 
FROM
  blog_categories bcat 
WHERE cat_status = 'Enable' 
ORDER BY cat_name ASC ";
		$cats = $this->SqlModel->runQuery($cats_query);
		
		//Tags
		$tags_query ="SELECT 
  tag_id,
  tag_name,
  (SELECT 
    COUNT(*) 
  FROM
    blog_assigned_tags bt 
    INNER JOIN blogs b 
      ON bt.bt_blog_id = b.blog_id 
  WHERE bt_tag_id = btag.tag_id) AS tag_total 
FROM
  blog_tags btag 
WHERE tag_status = 'Enable' AND (SELECT 
    COUNT(*) 
  FROM
    blog_assigned_tags bt 
    INNER JOIN blogs b 
      ON bt.bt_blog_id = b.blog_id 
  WHERE bt_tag_id = btag.tag_id) >0 
ORDER BY tag_name ASC ";
	$tags = $this->SqlModel->runQuery($tags_query);
		#############################################################################################
		
		# get blog comments
		$comments= $this->SqlModel->getRecords('*','blog_comments','id','DESC',array('status'=>'Published'),"5");
		
		$nav = $this->SqlModel->getNav();
		$data = array(
		'websettings'=>$this->websettings,
		'page_title'	=> PROJECT_TITLE." | ".$blogData['blog_name'],
		'page_meta_key'	=> $blogData['blog_meta_key'],
		'page_meta_desc'	=> $blogData['blog_meta_desc'],
		'nav' =>$nav,
		'cats' =>$cats,
		'b'=>$blogData,
		'tags'=>$tags,
		'comments' =>$comments,
		'active'=>3
		
		);
		
		
		  $this->load->view('header',$data);
		  $this->load->view('blogDetails');
		  $this->load->view('footer');
	}
	
	public function addcomment($blog_id= ""){
	$blog_uri = str_replace(".html","",$blog_id);
	$blogData = $this->SqlModel->getSingleRecord('blogs',array('blog_id'=>$blog_id,'blog_status'=>'Published'));
	
	$name = $this->websettings['website_title'];
	$to = $this->websettings['contact_form_email'];
	$subject = "[".PROJECT_TITLE."] Contact Us ".date('M-d-Y H:i a');
	$body = '<html>
	<head>
	<style>
	table{
	border: 1px solid #EAEAEA;
	margin-top:20px;
	border-collapse:collapse;	
	}
	table tr{
	border-bottom: 1px solid #EAEAEA;
	
	}
	
	table_span{
	clear:both;
	display:block;
	font-family: Arial, Helvetica, sans-serif;
	font-size:12px;
	color:#FF0000;
	margin-bottom:5px;
	}
	
	table td{
	padding:10px 5px;
	font-family: Arial, Helvetica, sans-serif;
	font-size:12px;
	color:#737373;
	}
	
	
	table h3{
	font-family: Arial, Helvetica, sans-serif;
	font-size: 18px;
	font-weight:normal;
	color:#666;
	margin:8px 0px;
	}
	</style>
	</head>
	<body>
	<table  width="100%">
       
       	 <tr>
        <td colspan="2"><h3>Comment INFORMATION</h3></td>
        </tr>
        <tr>
        <td><strong>Date/Time</strong></td><td>'.date('M d, Y h:i a').'</td>
        </tr>
        <tr>
        <td><strong>Name</strong></td><td>'.$this->input->post('name').'</td>
        </tr>
        
        
        <tr>
        <td><strong>Email</strong></td><td>'.$this->input->post('email').'</td>
        </tr>
      
        <tr>
        <td><strong>Message</strong></td><td>'.nl2br($this->input->post('message')).'</td>
        </tr>
		  <tr>
        <td><strong>IP Address</strong></td><td>'.$this->session->userdata('ip_address').'</td>
        </tr>
      
	    <tr>
        <td><strong>User Agent</strong></td><td>'.$this->session->userdata('user_agent').'</td>
        </tr>
      
       </table></body></html>';
	
	
	$config = array(
	'mailtype'=>'html',
	'useragent'=>$name
	);
	
	$blog_data = array(
		'blog_id' => $blogData['blog_id'],
		'name' => $this->input->post('name'),
		'email' => $this->input->post('email'),
		'comment' => $this->input->post('message'),
	);
	
	$this->SqlModel->insertRecord('blog_comments',$blog_data);
	
	//$this->load->library('email',$config);
	//$this->email->from('no-reply@tidyupp.co.uk', $name);
	//$this->email->to($to);
	//$this->email->reply_to('no-reply@tidyupp.co.uk', $name);
	//$this->email->subject($subject);
	//$this->email->message($body);
	//$this->email->send();
	//$this->email->print_debugger();
	$this->load->model('EmailModel');
	@$this->EmailModel->sendEmail($name, $to, $subject, $body);
	$this->session->set_flashdata('status','success');
		@redirect(base_url('blog/'.$blogData['blog_uri'].'.html/#bottom'),'location');
	}
}

