(function($) {
	"use strict";
	
	//Booking Page Variables
	var req = null;
	var reqcheck = null;
	var stepcount = 1;
	var clean_type = "Weekly Clean";
	var how_long = 2.5;
	var clean_supplies = "No";
	var rooms_hours = 0;
	var bath_hours = 0;
	var total_time = 2.5;
	var total = 2.5;
	var fridge = "No";
	var oven = "No";
	var cabinets = "No";
	var windows = "No";
	var walls = "No";
	var sub_total = 25;
	var grand_total = 25;
	//var extras = 0;
	var extras_time = 0;
	
	
	 
	
	
	function calculate()
	{
		$("#booking_alert").addClass('dnone');
		extras_time = 0;
		if(fridge=="Yes")
		{
			extras_time = parseFloat(extras_time) + 0.5;
		}
		if(oven=="Yes")
		{
			extras_time = parseFloat(extras_time) + 0.5;
		}
		if(cabinets=="Yes")
		{
			extras_time = parseFloat(extras_time) + 0.5;
		}
		if(windows=="Yes")
		{
			extras_time = parseFloat(extras_time) + 0.5;
		}
		if(walls=="Yes")
		{
			extras_time = parseFloat(extras_time) + 0.5;
		}
		
		total_time = 2.5 + parseFloat(rooms_hours) + parseFloat(bath_hours)  + parseFloat(extras_time);
		$("#estimated_hours").html(total_time);
		if(total_time>parseFloat(how_long))
		{
			$("#booking_alert").removeClass('dnone');
			//alert("Show box");	
		}
		
		if(how_long<2.5)
		{
			how_long = 2.5;
			$(".optimg").each(function(){
			 $(this).removeClass('optactive').attr('src','images/'+this.id+'.png');
				
			});
		}
		//alert("How Long: "+how_long+" Rooms: "+rooms_hours+ " Bath: "+bath_hours);
		$("#howlong").val(how_long);
		$("#howlong_text").html(how_long+' hours').addClass('seltextWhite');
		$("#how_long_text").html(how_long+' hours');	
		$("#howlong_cont").addClass('dropselected');
		$("#dpicker_container, #howtime_cont").removeClass('dnone');
		$("#howtext").addClass('dnone');
		
		//Calcs
		sub_total = parseFloat(how_long) * 10;
		grand_total = sub_total;
		if(clean_supplies=="Yes")
		{
			grand_total = parseFloat(grand_total)+parseFloat(5);
		}
		$("#sml_hours").html(how_long);
		$("#sml_total").html(sub_total);
		$("#grand_total").html(grand_total);
	}
	
	$(".optimg").hover(
	  function() {
		var imgID = this.id;
		if($(this).hasClass('optactive')==1)
		{
			//$(this).removeClass('optactive');
			//$(this).attr('src','images/'+this.id+'.png');
		}
		else{
			
			$(this).attr('src',front_assets+'images/'+this.id+'_hover.png');	
		}
	  }, function() {
			if($(this).hasClass('optactive')==1)
			{
				//$(this).removeClass('optactive');
				//$(this).attr('src','images/'+this.id+'.png');
			}
			else{
				
				$(this).attr('src',front_assets+'images/'+this.id+'.png');	
			}
	  }
	);
	
	 
	
	$(".optimg").on('click',function(){
		var imgID = this.id;
		if($(this).hasClass('optactive')==1)
		{
			$(this).removeClass('optactive');
			$(this).attr('src',front_assets+'images/'+this.id+'.png');
		}
		else{
			$(this).addClass('optactive');
			$(this).attr('src',front_assets+'images/'+this.id+'_active.png');	
		}
		
		if(this.id=="opt1")
		{
			fridge = (fridge=="No") ? "Yes" : "No";	
			if(fridge=="Yes")
			{
				how_long = parseFloat(how_long) + parseFloat(0.5);
			}
			else{
				how_long = parseFloat(how_long)-parseFloat(0.5);
			}
		}
		else if(this.id=="opt2")
		{
			oven = (oven=="No") ? "Yes" : "No";
			if(oven=="Yes")
			{
				how_long = parseFloat(how_long) + parseFloat(0.5);
				
			}
			else{
				how_long = parseFloat(how_long)-parseFloat(0.5);
			}
		}
		else if(this.id=="opt3")
		{
			cabinets = (cabinets=="No") ? "Yes" : "No";	
			if(cabinets=="Yes")
			{
				how_long = parseFloat(how_long) + parseFloat(0.5);
				
			}
			else{
				how_long = parseFloat(how_long)-parseFloat(0.5);
			}
		}
		else if(this.id=="opt4")
		{
			windows = (windows=="No") ? "Yes" : "No";
			if(windows=="Yes")
			{
				how_long = parseFloat(how_long) + parseFloat(0.5);
				
			}
			else{
				how_long = parseFloat(how_long)-parseFloat(0.5);
			}	
		}
		else if(this.id=="opt5")
		{
			walls = (walls=="No") ? "Yes" : "No";
			if(walls=="Yes")
			{
				how_long = parseFloat(how_long) + parseFloat(0.5);
				
			}
			else{
				how_long = parseFloat(how_long)-parseFloat(0.5);
			}	
		}
		calculate();
	});
	
	$(".typegraybg").on('click',function(){
		$(".typegraybg").removeClass("typegreenbg");
		$(this).addClass("typegreenbg");
		clean_type = $(this).html();
		$("#clean_type_text").html(clean_type);		
	});
	$(".cleangraybg").on('click',function(){
		if(clean_supplies=="No")
		{
			clean_supplies = "Yes";
			$("#clean_supp_rate").html('&pound;5');
		}
		else{
			clean_supplies = "No";
			$("#clean_supp_rate").html('&pound;0');
		}
		$(".cleangraybg").toggleClass("cleangreenbg");
		calculate();
		
	});
	$(".shide").on('change',function(){
		var sID = "#"+this.id;
		var textID = sID+"_text";
		var contID = sID+"_cont"; 
		
		if(this.value>0)
		{
			if(this.id=="howlong")
			{
				how_long = this.value;
				$("#how_long_text").html(how_long+' hours');
				$("#howtext").addClass('dnone');	
				$("#dpicker_container,#howtime_cont").removeClass('dnone');
			}
			
			
			
			$(contID).addClass('dropselected');
			$(textID).addClass('seltextWhite');		
		}
		else{
			if(this.id=="howlong")
			{
				how_long = 2.5;
				$("#how_long_text").html('Choose duration');
				//$("#show_date,#showformatteddate").html('Pick a date');
				//$("#dpicker_container,#howtime_cont").removeClass('typegreenbgdp');
				//$("#dp1").val('');
				$("#howtext").removeClass('dnone');	
				//$("#dpicker_container,#howtime_cont").addClass('dnone');	
			}
			$(contID).removeClass('dropselected');
			$(textID).removeClass('seltextWhite');		
				
		}
		
		if(this.id=="rooms")
		{
			how_long = parseFloat(how_long)-parseFloat(rooms_hours);
			//alert("How Long: "+how_long+" Rooms: "+rooms_hours+ " Bath: "+bath_hours);
			rooms_hours = this.value;
			how_long = parseFloat(how_long)+parseFloat(rooms_hours);
			//alert("How Long: "+how_long+" Rooms: "+rooms_hours+ " Bath: "+bath_hours);
		}
		
		if(this.id=="bathrooms")
		{
			how_long = parseFloat(how_long) - parseFloat(bath_hours);
			bath_hours = this.value;
			how_long = parseFloat(how_long) + parseFloat(bath_hours);
		}
		
		if(this.id=="howtime")
		{
			$("#show_time").html($("#howtime :selected").text());
		}
		 calculate();
		var value = $(sID +" :selected").text();
		$(textID).html(value);
		
			
	});
	
	/*Booking Steps START */
	$("#gostep2").on('click',function(){
		if(checkstep1()==false)
		{
			return false;
		}		
		stepcount = 2;
		$(".stageDiv").removeClass('currentStage');
		$("#stage2").addClass('currentStage');
		$("#booktext").html('Your Details');
		$(".bookconbt,.showstep1,.bsteps,#booking_alert").addClass('dnone');
		$("#gostep3,.showstep2").removeClass('dnone');
		$(window).scrollTop(0); 
		
	});
	
	$("#gostep3").on('click',function(){
		if(checkstep2()==false)
		{
			$(window).scrollTop(100); 
			return false;	
		}
		stepcount = 3;
		$(".stageDiv").removeClass('currentStage');
		$("#stage3").addClass('currentStage');
		$("#booktext").html('Pay &amp; Confirm');
		$(".bookconbt,.showstep2,.bsteps").addClass('dnone');
		$("#gostep4,.showstep3").removeClass('dnone');
		//Showing the data in summary section
		$("#s_full_name").html($("#full_name").val());
		$("#s_email_address").html($("#email_address").val());
		$("#s_phone_numb").html($("#phone_numb").val());
		$("#s_house_numb").html($("#house_numb").val());
		$("#s_street").html($("#street").val());
		$("#s_pcode").html($("#pcode").val());
		$("#s_city").html($("#city").val());		
		
		$(window).scrollTop(0); 
		
	});
	
	$("#gostep4").on('click',function(){
		if(checkstep3()==false)
		{
			$(window).scrollTop(100); 
			return false;	
		}
		stepcount = 4;
		var bookData = {
			'rooms' 		: 	$("#rooms option:selected").text(),
			'bathrooms' 	: 	$("#bathrooms option:selected").text(),
			'cleantype' 	: 	clean_type,
			'fridge'		:	fridge,
			'oven'			:	oven,
			'cabinets'		:	cabinets,
			'windows'		:	windows,
			'walls'			:	walls,
			'how_long'		:	how_long,
			'grand_total'	:	grand_total,
			'estimated_time':	total_time,
			'booking_date'			:	$("#dp1").val(),
			'howtime' 		: 	$("#howtime option:selected").text(),
			'clean_supplies':	clean_supplies,
			'clean_comments':	$("#clean_comments").val(),
			'email_address' :	$("#email_address").val(),
			'full_name' 	:	$("#full_name").val(),
			'phone_numb' 	:	$("#phone_numb").val(),
			'add_comments' 	:	$("#add_comments").val(),
			'house_numb' 	:	$("#house_numb").val(),
			'street' 		:	$("#street").val(),
			'pcode' 		:	$("#pcode").val(),
			'city' 			:	$("#city").val(),
			'howtoenter' 	:	$("#howtoenter").val(),
			'cc_name'		:	$("#cc_name").val(),
			'cc_numb'		:	$("#cc_numb").val(),
			'cc_cvv'		:	$("#cc_cvv").val(),
			'cc_month'		:	$("#cc_month").val(),
			'cc_year'		:	$("#cc_year").val(),	
		};
		
		if (req != null) req.abort();
		req = $.ajax({
			url: base_url+'booking/save',
			type: "POST",
			data:{"bookData":bookData} ,
			dataType: "JSON",
			success: function(data){
				if(data.status=="true")
				{
					$(window).scrollTop(0);
					$(".stageDiv").removeClass('currentStage');
					$("#stage4").addClass('currentStage');
					$("#booktext").html('Booking Complete');
					$("#mainbook_div").addClass('dnone');
					$("#thankyou_div").removeClass('dnone');
					
				}
				else{
					
					$("#bvmsg").html(data.message);
				}
			}
		});
		
	});
	
	
	$("#postcode").keypress(function( event ) {
		
		if ( event.which == 13 ) {
			$("#check").click();
		}
	
	});
	$('#postcode').keyup(function () { 
    this.value = this.value.replace(/[^A-Za-z0-9\.\_\- ]/g,'');
	});
	
	$("#check").on('click',function(){
		$("#bsorry").addClass('dnone');
		if($("#postcode").val().trim()=="")
		{
			return false;	
		}
		if (reqcheck != null) reqcheck.abort();
		reqcheck = $.ajax({
			url: base_url+'booking/check',
			type: "POST",
			data:{"post_code":$("#postcode").val()} ,
			dataType: "JSON",
			success: function(data){
				if(data.status=="true")
				{
					window.location = base_url+'booking.html?post_code='+data.post_code;
					
				}
				else{
					
					$("#bsorry").removeClass('dnone');
				}
			}
		});	
		
	});
	
	$(".stageDiv").on('click',function(){
		if(stepcount==4)
		{
			return false;	
		}
		else if(stepcount==3)
		{
			if(this.id=="stage1")
			{
				stepcount = 1;
				$(".stageDiv").removeClass('currentStage');
				$("#stage1").addClass('currentStage');
				$("#booktext").html('Booking Options');
				$(".bsteps,.bookconbt").addClass('dnone');
				$(".showstep1,#gostep2").removeClass('dnone');
				
			}
			else if(this.id=="stage2")
			{
				$("#gostep2").click();
			}
		}
		else if(stepcount==2)
		{
			if(this.id=="stage1")
			{
				stepcount = 1;
				$(".stageDiv").removeClass('currentStage');
				$("#stage1").addClass('currentStage');
				$("#booktext").html('Booking Options');
				$(".bsteps,.bookconbt").addClass('dnone');
				$(".showstep1,#gostep2").removeClass('dnone');
			}
		}
		
	});
	var cdate = new Date();
	/*Booking Steps END */
	function checkstep3()
	{
		$("#bvmsg").html('');
		$(".rcolor").removeClass('rcolor');
		if($("#cc_name").val().trim()=="")
		{
			$("#bvmsg").html("Please enter cardholder's name");
			$("#cc_name").addClass('rcolor').focus();
			return false;
		}	
		else if($("#cc_numb").val().trim()=="")
		{
			$("#bvmsg").html("Please enter credit card number");
			$("#cc_numb").addClass('rcolor').focus();
			return false;
		}
		else if($("#cc_cvv").val().trim()=="")
		{
			$("#bvmsg").html("Please enter CVC/CVV");
			$("#cc_cvv").addClass('rcolor').focus();
			return false;
		}
		else if($("#cc_month").val()=="0")
		{
			$("#bvmsg").html("Invalid expiry month");
			$("#cc_month").addClass('rcolor').focus();
			return false;
		}
		else if($("#cc_year").val()=="0")
		{
			$("#bvmsg").html("Invalid expiry year");
			$("#cc_year").addClass('rcolor').focus();
			return false;
		}
		
	}
	
	function checkstep2()
	{
		$("#bvmsg").html('');
		$(".rcolor").removeClass('rcolor');
		var x=$("#email_address").val();
		var atpos=x.indexOf("@");
		var dotpos=x.lastIndexOf(".");
		if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length)
		{
		  $("#bvmsg").html('Please enter valid email address');
		  $("#email_address").addClass('rcolor').focus();
		  return false;
		}
		else if($("#full_name").val().trim()=="")
		{
			$("#bvmsg").html('Please enter your name');
			$("#full_name").addClass('rcolor').focus();
			return false;
		}
		else if($("#phone_numb").val().trim()=="")
		{
			$("#bvmsg").html('Please enter your phone number');
			$("#phone_numb").addClass('rcolor').focus();
			return false;
		}
		else if($("#phone_numb").val().trim()=="")
		{
			$("#bvmsg").html('Please enter your phone number');
			$("#phone_numb").addClass('rcolor').focus();
			return false;
		}
		else if($("#house_numb").val().trim()=="")
		{
			$("#bvmsg").html('Please enter your house number');
			$("#house_numb").addClass('rcolor').focus();
			return false;
		}
		else if($("#street").val().trim()=="")
		{
			$("#bvmsg").html('Please enter your street address');
			$("#street").addClass('rcolor').focus();
			return false;
		}
		else if($("#city").val()=="0")
		{
			$("#bvmsg").html('Please select your city');
			$("#city").addClass('rcolor').focus();
			return false;
		}
		else if($("#howtoenter").val()=="")
		{
			$("#howtoenter").addClass('rcolor').focus();
			return false;
		}
		
		
		return true;
	}
	
	function checkstep1()
	{
		$("#bvmsg").html('');
		if($("#howlong").val()=="0")
		{
			$("#bvmsg").html('Please choose duration');
			return false;
		}
		else if($("#dp1").val()=="")
		{
			$("#bvmsg").html('Please pick a date');
			return false;
		}
		else if($("#howtime").val()=="0")
		{
			$("#bvmsg").html('Please pick a time');
			return false;
		}
		
		else if(total_time>parseFloat(how_long)&&$("#clean_comments").val()=="")
		{
			$("#clean_comments").addClass('rcolor').focus();
			return false;	
		}
		return true;
	}
	
	
	
	
	$('.alphanumeric').keyup(function () { 
    this.value = this.value.replace(/[^0-9]/g,'');
	});
	 

	$('.settings-wrapper .show-settings i').click(function() {
		$(this).parent().parent().find('.settings-block').slideToggle();
	});
	$('.settings-block a').click(function(e) {
		e.preventDefault();
		var $t = $(this);
		if ($t.attr('data-color') != undefined) {
			$('#site-color').attr('href', './css/color-' + $t.attr('data-color') + '.css');
		}
		if ($(this).attr('data-pattern') != undefined) {
			$('.pattern-bg').css('background-image', 'url(images/patterns/' + $(this).attr('data-pattern') +'.png)');
		}
	});

	$('#layerslider').layerSlider({
		skinsPath : './css/layerslider/skins/',
		skin : 'borderlesslight',
		twoWaySlideshow : true,
		firstLayer: 1,
		responsive: true,
	});
	/* carousels */
/*
	jQuery('#testimonials-2').jcarousel({
		scroll: 1,
		visible: 1,
		wrap: 'both'
    });
	jQuery('#single-post-releated-posts').jcarousel({
		scroll: 1,
		visible: 3,
		wrap: 'both'
    });
	jQuery('#latest-projects-items', '.latest-projects').jcarousel({
		scroll: 1,
		visible: 4,
		wrap: 'both'
    });
	jQuery('#latest-projects-items', '.latest-projects-2').jcarousel({
		scroll: 1,
		visible: 5,
		wrap: 'both'
    });
    jQuery('#testimonials-1').jcarousel({
		scroll: 1,
		visible: 2,
		wrap: 'both'
    });
    jQuery('#footer-latest-tweets').jcarousel({
		scroll: 1,
		visible: 1,
		wrap: 'both'
    });
    jQuery('#latest-blog-posts').jcarousel({
		scroll: 1,
		visible: 1,
		wrap: 'both'
    });
	jQuery('#latest-blog-posts', '.latest-blog-posts-2').jcarousel({
		scroll: 1,
		visible: 2,
		wrap: 'both'
    });
	jQuery('#respective-partners', '.respective-partners').jcarousel({
		scroll: 1,
		visible: 5,
		wrap: 'both'
    });
	
	var bw = $('body').width();
		if (bw < 480) {
			jQuery('#latest-projects-items', '.latest-projects').jcarousel({
				scroll: 1,
				visible: 1,
				wrap: 'both'
			});
			jQuery('#latest-projects-items', '.latest-projects-2').jcarousel({
				scroll: 1,
				visible: 1,
				wrap: 'both'
			});
			jQuery('#latest-projects-items', '.latest-projects-3').jcarousel({
				scroll: 1,
				visible: 1,
				wrap: 'both'
			});
		} else if ((bw <= 767) && (bw >= 480)) {
			jQuery('#latest-projects-items', '.latest-projects').jcarousel({
				scroll: 1,
				visible: 1,
				wrap: 'both'
			});
			jQuery('#latest-projects-items', '.latest-projects-2').jcarousel({
				scroll: 1,
				visible: 1,
				wrap: 'both'
			});
			jQuery('#latest-projects-items', '.latest-projects-3').jcarousel({
				scroll: 1,
				visible: 1,
				wrap: 'both'
			});
		} else if ((bw > 767) && (bw <= 979)) {
			jQuery('#latest-projects-items', '.latest-projects').jcarousel({
				scroll: 1,
				visible: 3,
				wrap: 'both'
			});
			jQuery('#latest-projects-items', '.latest-projects-2').jcarousel({
				scroll: 1,
				visible: 4,
				wrap: 'both'
			});
			jQuery('#latest-projects-items', '.latest-projects-3').jcarousel({
				scroll: 1,
				visible: 4,
				wrap: 'both'
			});
		} else if (bw > 979) {
			jQuery('#latest-projects-items', '.latest-projects').jcarousel({
				scroll: 1,
				visible: 4,
				wrap: 'both'
			});
			jQuery('#latest-projects-items', '.latest-projects-2').jcarousel({
				scroll: 1,
				visible: 5,
				wrap: 'both'
			});
			jQuery('#latest-projects-items', '.latest-projects-3').jcarousel({
				scroll: 1,
				visible: 5,
				wrap: 'both'
			});
		}
		*/
	/* magnis-product-item */
	
	$('.magnis-product-item').each(function() {
		var $this = $(this),
			$fig = $this.find('figure');
		$fig.hover(function() {
			$(this).find('.magnis-product-item-hover').stop(true, true).fadeIn().parent().addClass('hovered');
		}, function() {
			$(this).find('.magnis-product-item-hover').stop(true, true).fadeOut().parent().removeClass('hovered');
		});
		$('.magnis-product-item-hover a', $this).wrapAll('<div class="magnis-product-item-a-wrapper"></div>');
		$('.icon-search', $(this)).parent().colorbox();
	});
	$('i.icon-minus', '.magnis-shopping-cart').click(function() {
		$(this).toggleClass('icon-shopping-cart');
		$('.magnis-shopping-cart-entry', '.magnis-shopping-cart').slideToggle();
	}).css('left', $('.magnis-shopping-cart h2').width() + 20);
	$('.magnis-product-item-single-desc .price .product-qty a, .magnis-shopping-cart-details-table td .product-qty a').click(function(e) {
		e.preventDefault();
		var $i = $(this).find('i'),
			t = $(this).parent().find('input').val();
		if ($i.hasClass('icon-plus')) {
			t++;
			$(this).parent().find('input').val(t);
		} else {
			if (t != 1) {
				t--;
			}
			$(this).parent().find('input').val(t);
		}
	});
	$('.customers-reviews-list-item:last-child').css('margin-bottom','0px');
	$('form.magnis-product-item-review p input, form.magnis-product-item-review p textarea').bind('focus', function() {
		$(this).addClass('focused');
	});
	$('form.magnis-product-item-review p input, form.magnis-product-item-review p textarea').bind('blur', function() {
		$(this).removeClass('focused');
	});
	$('.magnis-shopping-cart-details-table').wrap('<div class="magnis-shopping-cart-details-table-wrapper"></div>');
	$('.magnis-shopping-cart-details-table tbody tr').hover(
		function() {
			$(this).find('> td').css('background-color','#eee');
		}, function() {
			$(this).find('> td').css('background-color','#fff');
	});
	if ($('body').width() < 768) {
		$('.magnis-shopping-cart-details-table-wrapper').css('overflow','scroll');
	} else {
		$('.magnis-shopping-cart-details-table-wrapper').css('overflow','visible');
	}
	$('.magnis-checkout-block #create-account').click(function() {
		if ($(this).is(':checked')) {
			$('.magnis-checkout-block .create-account').slideDown();
		} else {
			$('.magnis-checkout-block .create-account').slideUp();
		}
	});
	if ($('.magnis-checkout-block #create-account').is(':checked')) {
			$('.magnis-checkout-block .create-account').show();
	}
	$('.magnis-checkout-block #same-address').click(function() {
		if ($(this).is(':checked')) {
			$('.magnis-checkout-block .create-account').slideUp();
		} else {
			$('.magnis-checkout-block .create-account').slideDown();
		}
	});
	if ($('.magnis-checkout-block #same-address').is(':checked')) {
			$('.magnis-checkout-block .create-account').hide();
	}
	$('.magin-payment-option p input[type=radio]').each(function() {
		if ($(this).is(':checked')) {
			$(this).parent().parent().parent().find('p.desc').show();
		}
	}).on('click', function() {
		$('.magin-payment-option p.desc').slideUp();
		$('.magin-payment-option p label').css('color','#777');
		if ($(this).is(':checked')) {
			$(this).parent().css('color', '#333').parent().parent().find('p.desc').slideDown();
		}
	});
	
	/* 404 search */
	
	$('.magnis-404 #magnis-404-search input').bind('focus', function() {
		$('.magnis-404 #magnis-404-search').addClass('focused');
	});
	$('.magnis-404 #magnis-404-search input').bind('blur', function() {
		$('.magnis-404 #magnis-404-search').removeClass('focused');
	});
	
	/* main-content-soon */
	
	$('.main-content-soon').wrapInner('<div class="main-content-soon-color"></div>');

	/* home-3-features-item */
	
	$('.home-3-features-item, .feature').hover(function() {
		$(this).find('i').addClass('hover');
	},function () {
		$(this).find('i').removeClass('hover');
	});
	
	/* magnis-pagination */
	
	$('.magnis-pagination').find('a').addClass('button-border');
	$('.widget-content-tags').find('a').addClass('button-border button-small');
	
	$('.single-project-details:last-child').css('margin-bottom','20px');
	
	/* nav bars */

	$('nav.site-desktop-menu > ul > li').hover(function() {
		$(this).find('> ul').stop(true, true).slideDown(200);
	}, function() {
		$(this).find('> ul').stop(true, true).slideUp(200);
	});
	
	$('nav.site-mobile-menu > i').click(function() { $(this).parent().find('> ul').slideToggle(); } );
	$('nav.site-mobile-menu > ul li').each(function() {
		if ($(this).find('> ul').length != 0) {
			$('<i class="icon-plus"></i>').prependTo($(this));
		}
	});
	$('nav.site-mobile-menu > ul li i').click(function() {
		$(this).toggleClass('icon-minus').parent().find('> ul').slideToggle();
	});
	$('nav.site-mobile-menu > ul li a, nav.site-desktop-menu > ul li a').hover(function() {
		$(this).parent().addClass('hovered');
	}, function() {
		$(this).parent().removeClass('hovered');
	});
	
	/* a.button */

	$('.button, .quick_newsletter button.btn, .add-comment-form p button').hover(function() {
		$('<span></span>').appendTo($(this));
	},function() {
		$(this).find('span').remove();
	});
	
	$('.feature:last-child').css('margin-bottom','0px');
	
	/* testimonials-1 */
	/*
	$('.jcarousel-prev-horizontal, .jcarousel-next-horizontal', '.testimonials-1').wrapAll('<div class="carousel-navi"></div>');
	$('<i class="icon-angle-left"></i>').appendTo($('.jcarousel-prev-horizontal', '.testimonials-1'));
	$('<i class="icon-angle-right"></i>').appendTo($('.jcarousel-next-horizontal', '.testimonials-1'));
	$('.carousel-navi', '.testimonials-1').css('left', $('h2', '.testimonials-1').width() + 20 + 'px');
	$('<i class="icon-quote-left"></i>').prependTo('.testimonials-1 .jcarousel-skin-tango .jcarousel-item-horizontal p');
	
	
	
	$('.jcarousel-prev-horizontal, .jcarousel-next-horizontal', '.testimonials-2').wrapAll('<div class="carousel-navi"></div>');
	$('<i class="icon-angle-left"></i>').appendTo($('.jcarousel-prev-horizontal', '.testimonials-2'));
	$('<i class="icon-angle-right"></i>').appendTo($('.jcarousel-next-horizontal', '.testimonials-2'));
	
	
	$('<i class="icon-tag"></i>').prependTo('.latest-projects-wrapper .jcarousel-skin-tango .jcarousel-item-horizontal p.project-tags');
	$('.latest-projects-wrapper .jcarousel-skin-tango .jcarousel-item-horizontal, .project-item').hover(function() {
		$(this).find('.project-details').stop(true, true).fadeIn().find('.project-title').stop(true, true).animate({'top':20}).end().find('.project-tags').stop(true, true).animate({'bottom': 20});
	}, function() {
		$(this).find('.project-details').stop(true, true).fadeOut().find('.project-title').stop(true, true).animate({'top':-20}).end().find('.project-tags').stop(true, true).animate({'bottom': -20});
	});
	
	
	$('<i class="icon-twitter"></i>').prependTo($('.footer-latest-tweets .jcarousel-skin-tango .jcarousel-item-horizontal p.username'));
	$('.jcarousel-prev-horizontal, .jcarousel-next-horizontal', '.footer-latest-tweets').wrapAll('<div class="carousel-navi"></div>');
	$('.carousel-navi', '.footer-latest-tweets').css('left', $('h2', '.footer-latest-tweets').width() + 20 + 'px');
	$('<i class="icon-angle-left"></i>').appendTo($('.jcarousel-prev-horizontal', '.footer-latest-tweets'));
	$('<i class="icon-angle-right"></i>').appendTo($('.jcarousel-next-horizontal', '.footer-latest-tweets'));
	
	
	$('<i class="icon-double-angle-right"></i>').prependTo($('.latest-blog-posts .jcarousel-skin-tango .jcarousel-item-horizontal .latest-blog-post-details small span.read'));
	$('<i class="icon-tag"></i>').prependTo($('.latest-blog-posts .jcarousel-skin-tango .jcarousel-item-horizontal .latest-blog-post-details small span.tags'));
	$('.jcarousel-prev-horizontal, .jcarousel-next-horizontal', '.latest-blog-posts').wrapAll('<div class="carousel-navi"></div>');
	$('.carousel-navi', '.latest-blog-posts').css('left', $('h2', '.latest-blog-posts').width() + 20 + 'px');
	$('<i class="icon-angle-left"></i>').appendTo($('.jcarousel-prev-horizontal', '.latest-blog-posts'));
	$('<i class="icon-angle-right"></i>').appendTo($('.jcarousel-next-horizontal', '.latest-blog-posts'));
	
	
	$('.jcarousel-prev-horizontal, .jcarousel-next-horizontal', '.respective-partners').wrapAll('<div class="carousel-navi"></div>');
	$('<i class="icon-angle-left"></i>').appendTo($('.jcarousel-prev-horizontal', '.respective-partners'));
	$('<i class="icon-angle-right"></i>').appendTo($('.jcarousel-next-horizontal', '.respective-partners'));
	$('.carousel-navi', '.respective-partners').css('margin-left', '-32px');
	$('.respective-partners .jcarousel-skin-tango .jcarousel-item-horizontal').find('img').fadeTo(0, 0.4).hover(function() {
		$(this).fadeTo(0, 1);
	}, function() {
		$(this).fadeTo(0, 0.4);
	});
	
	$('.jcarousel-prev-horizontal, .jcarousel-next-horizontal', '.single-post-releated-posts').wrapAll('<div class="carousel-navi"></div>');
	$('<i class="icon-angle-left"></i>').appendTo($('.jcarousel-prev-horizontal', '.single-post-releated-posts'));
	$('<i class="icon-angle-right"></i>').appendTo($('.jcarousel-next-horizontal', '.single-post-releated-posts'));
	$('.carousel-navi', '.single-post-releated-posts').css('left', $('h2', '.single-post-releated-posts').width() + 20 + 'px');
	*/
	/* to the top */

	$(window).scroll(function () {
		if ($(this).scrollTop() > 40) {
			$('#to-the-top').fadeIn();
		} else {
			$('#to-the-top').fadeOut();
		}
	});
	$('#to-the-top').click(function() { $('body,html').animate({scrollTop: 0}, 1000); });
	
	/* latest-blog-posts-2 */
	
	$('.latest-blog-posts-2 .jcarousel-skin-tango .jcarousel-item-horizontal').css('width', ($('.latest-blog-posts-2').width()/2 - 10) + 'px');
	$('.carousel-navi', '.latest-blog-posts-2').css('left', ($('.latest-blog-posts-2').width()/2 - 27) + 'px');

	/* magnis-toggle */
	
	$('section', '.magnis-toggle').click(function() {
		if (!$(this).hasClass('active')) {
			$('section', '.magnis-toggle').removeClass('active').find('p').slideUp();
			$(this).addClass('active').find('p').slideDown();
		} else {
			$(this).removeClass('active').find('p').slideUp();
		}
	});
	
	/* magnis skills */
	
	$('section', '.magnis-skills').each(function() {
		var $this = $(this);
		var val = $this.find('.skill-value').attr('data-skill-value');
		$('<div class="skill-value-chart"></div>').css('width', val + '%').appendTo($this.find('.skill-value'));
		$('<p>' + val + '%</p>').appendTo($this.find('.skill-value').find('.skill-value-chart'));
	});
	
	/* magnis-tabs */
	
	$('section', '.magnis-tabs').eq(0).show();
	$('header p', '.magnis-tabs').each(function() {
		$('<span></span>').appendTo($(this));
	}).eq(0).addClass('active').end().click(function() {
		$('header p', '.magnis-tabs').removeClass('active');
		$(this).addClass('active');
		$('section', '.magnis-tabs').hide().eq($(this).index()).slideDown();
	});
	
	/* price tables */
	
	$('.pr-table-3').find('thead tr th:first-child, tbody tr td:first-child').addClass('options');
	$('.pr-table-3').wrap('<div class="pr-table-3-wrapper"></div>');
	$('.pr-table-3 tbody tr').hover(
		function() {
			$(this).find('> td:not(.options)').css('background-color','#eee');
			$(this).find('> td.options').css('background-color','#444');
		}, function() {
			$(this).find('> td:not(.options)').css('background-color','#fff');
			$(this).find('> td.options').css('background-color','#333');
	});
	if ($('body').width() < 768) {
		$('.pr-table-3-wrapper').css('overflow','scroll');
	} else {
		$('.pr-table-3-wrapper').css('overflow','visible');
	}
	
	/* sidebar categories */
	
	$('<i class="icon-caret-right"></i>').prependTo('.sidebar-categories ul li');
	
	/* sidebar posts */
	
	$('.sidebar-posts-item').each(function() {
		var $this = $(this),
			h = $this.find('figure').height(),
			img = new Image();

		img.src = $this.find('figure img').attr('src');
		var w = Math.round(img.width * (h/img.height));
		$this.find('figure img').attr('width', w).css('margin-left', '-' + w/2 + 'px');
	});
	
	/* projects */
	
	$('.latest-projects .latest-projects-wrapper .jcarousel-skin-tango .jcarousel-item-horizontal').each(function() {
		$(this).height($(this).width());
	});
	$('.latest-projects-wrapper li', '.latest-projects').each(function() {
		var $this = $(this),
			w = $this.width(),
			$img = $this.find('> img').eq(0),
			h = $img.height(),
			temp = new Image(),
			index = $(this).index(),
			imgW;
			$img.show();
			
		temp.src = $img.attr('src');
		imgW = Math.round(temp.width * (h/temp.height));
		$img.attr('width', imgW).css('margin-left', '-' + ($img.width() - w)/2 + 'px');
		$('.icon-search', $(this)).parent().colorbox();
	});
	
	/* sidebar search */
	
	$('.sidebar-search').find('input[type=text]').bind('focus', function() {
		$(this).parent().addClass('sidebar-search-active');
	});
	$('.sidebar-search').find('input[type=text]').bind('blur', function() {
		$(this).parent().removeClass('sidebar-search-active');
	});
	
	/* header search */
	
	$('.header-search').find('input[type=text]').bind('focus', function() {
		$(this).parent().parent().addClass('header-search-focused');
	});
	$('.header-search').find('input[type=text]').bind('blur', function() {
		$(this).parent().parent().removeClass('header-search-focused');
	});
	
	$('.magnis-shipping, .magnis-promo').find('input[type=text], input[type=email], textarea, select').bind('focus', function() {
		$(this).addClass('focused');
	});
	$('.magnis-shipping, .magnis-promo').find('input[type=text], input[type=email], textarea, select').bind('blur', function() {
		$(this).removeClass('focused');
	});
	
	$('.magnis-checkout-block').find('input[type=text], input[type=email], input[type=password], textarea, select').bind('focus', function() {
		$(this).addClass('focused');
	});
	$('.magnis-checkout-block').find('input[type=text], input[type=email], input[type=password], textarea, select').bind('blur', function() {
		$(this).removeClass('focused');
	});
	
	makeResize();

	if ($('body').width() > 768)
		$('.latest-projects-intro').height($('.latest-projects-wrapper .jcarousel-skin-tango .jcarousel-item-horizontal').width());
	$('.latest-projects-wrapper .jcarousel-skin-tango .jcarousel-clip-horizontal').height($('.latest-projects-wrapper .jcarousel-skin-tango .jcarousel-item-horizontal').width());
	$('.latest-projects-wrapper .jcarousel-skin-tango .jcarousel-prev-horizontal, .latest-projects-wrapper .jcarousel-skin-tango .jcarousel-next-horizontal').css('top',($('.latest-projects-wrapper .jcarousel-skin-tango .jcarousel-item-horizontal').width() - 32)/2)
	$(window).resize(makeResize);

	jQuery(document).ready(function() {
		$('.jcarousel-prev-horizontal, .jcarousel-next-horizontal', '.single-project-slides').css('top', ($('.single-project-slides .jcarousel-skin-tango .jcarousel-item').height() - 32) / 2 + 'px');
	});
	
})(jQuery);

function makeResize() {

	/* project-item */
	
	var multipler = 1/3;
	var projectItemsWrapperWidth = $('#portfolio-list').width();
	if ($('body').width() < 768) {
		multipler = 1;
	} else {
		if ($('#portfolio-list').hasClass('portfolio-3-columns')) {
			multipler = 1/3;
		}
		if ($('#portfolio-list').hasClass('portfolio-4-columns')) {
			multipler = 1/4;
		}
		if ($('#portfolio-list').hasClass('portfolio-5-columns')) {
			multipler = 1/5;
		}
	}
	$('.project-item').css('height', projectItemsWrapperWidth * multipler + 'px');
	$('.project-item').css('width', projectItemsWrapperWidth * multipler + 'px');
	
	/* latest-projects */
	
	$('.project-item').each(function() {
		var $img = $(this).find('img').eq(0),
			h = $(this).height(),
			imgW,
			temp = new Image();
		temp.src = $img.attr('src');
		imgW = Math.round(temp.width * (h/temp.height));
		$img.attr('height', $(this).width());
		$img.attr('width', imgW).css('margin-left', '-' + ($img.attr('width') - $(this).width())/2 + 'px');
		$('.icon-search', $(this)).parent().colorbox();
	});

	$('.latest-projects-wrapper .jcarousel-skin-tango .jcarousel-item-horizontal').each(function() {
		$(this).height($(this).width());
	});
	$('.latest-projects-wrapper li').each(function() {
		var $this = $(this),
			w = $this.width(),
			$img = $this.find('> img').eq(0),
			h = $img.height(),
			temp = new Image(),
			index = $(this).index(),
			imgW;
			$img.show(),
		temp.src = $img.attr('src');
		imgW = Math.round(temp.width * (h/temp.height));
		$img.attr('width', imgW).css('margin-left', '-' + ($img.width() - w)/2 + 'px');
		$('.icon-search', $(this)).parent().colorbox();
	});
	
	if ($('body').width() < 768) {
		$('.magnis-shopping-cart-details-table-wrapper').css('overflow','scroll');
	} else {
		$('.magnis-shopping-cart-details-table-wrapper').css('overflow','visible');
	}
	if ($('body').width() < 768) {
		$('.pr-table-3-wrapper').css('overflow','scroll');
	} else {
		$('.pr-table-3-wrapper').css('overflow','visible');
	}
	$('.latest-blog-posts-2 .jcarousel-skin-tango .jcarousel-item-horizontal').css('width', ($('.latest-blog-posts-2').width()/2 - 10) + 'px');
	$('.carousel-navi', '.latest-blog-posts-2').css('left', ($('.latest-blog-posts-2').width()/2 - 27) + 'px');
	$('.latest-projects-wrapper .jcarousel-skin-tango .jcarousel-clip-horizontal').height($('.latest-projects-wrapper .jcarousel-skin-tango .jcarousel-item-horizontal').width());
	var bw = $('body').width();
		if (bw < 480) {
			
		} else if ((bw <= 767) && (bw >= 480)) {
			
			$('.latest-projects-intro').height($('.latest-projects-wrapper .jcarousel-skin-tango .jcarousel-item-horizontal').width() - 40);
		} else if (bw > 979) {
			
			$('.latest-projects-intro').height($('.latest-projects-wrapper .jcarousel-skin-tango .jcarousel-item-horizontal').width() - 40);
		}
	$('.latest-projects-wrapper .jcarousel-skin-tango .jcarousel-prev-horizontal, .latest-projects-wrapper .jcarousel-skin-tango .jcarousel-next-horizontal').css('top',($('.latest-projects-wrapper .jcarousel-skin-tango .jcarousel-item-horizontal').width() - 32)/2)
	$('.carousel-navi', '.footer-latest-tweets').css('left', $('h2', '.footer-latest-tweets').width() + 20 + 'px');
	$('.carousel-navi', '.testimonials-1').css('left', $('h2', '.testimonials-1').width() + 20 + 'px');
		$('.magnis-product-item').each(function() {
			if (($('body').width() > 768) && ($('body').width() < 979)) {
					$(this).find('a.button-color').html('<i class="icon-shopping-cart"></i>');
					$(this).find('a.button-dark').html('<i class="icon-align-justify"></i>');
			} else {
					$(this).find('a.button-color').html('<i class="icon-shopping-cart"></i> To The Cart');
					$(this).find('a.button-dark').html('<i class="icon-align-justify"></i> Details');
			}
		});
}


(function( $ ){

	$.fn.magnisSort = function( options ) {
		
		var $this = $(this),
			$filters = $this.find('.sorting-filters a'),
			$toSort = $this.find('.sort-item'),
			showAll = options['showAll'];
			
		if (showAll) {
			$toSort.show();
		} else {
			$toSort.eq(0).show();
		}
		
		$filters.click(function(e) {
			e.preventDefault();
			$filters.removeClass('active');
			$(this).addClass('active');
			var value = $(this).attr('data-filter'),
				qty = $toSort.length;
			$toSort.hide();
			for (var $i = 0; $i < qty; $i++) {
				if ($toSort.eq($i).hasClass(value)) {
					$toSort.eq($i).show();
				} else if (value == '*') {
					$toSort.show();
				}
			}
		});
	};	
$("#contact_form").validate();
})( jQuery );